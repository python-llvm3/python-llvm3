#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
# 
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from LLVMCore cimport LLVMTypeRef, LLVMPassManagerRef, LLVMValueRef, string
from LLVMTarget cimport LLVMTargetDataRef

cdef extern from "llvm-c/TargetMachine.h":

	cdef cppclass LLVMTargetMachine "llvm::TargetMachine":
		pass
	ctypedef LLVMTargetMachine* LLVMTargetMachineRef

	cdef cppclass LLVMTarget "llvm::Target":
		pass
	ctypedef LLVMTarget* LLVMTargetRef

	enum LLVMCodeGenOptLevel:
		LLVMCodeGenLevelNone,
		LLVMCodeGenLevelLess,
		LLVMCodeGenLevelDefault,
		LLVMCodeGenLevelAggressive

	enum LLVMRelocMode:
		LLVMRelocDefault,
		LLVMRelocStatic,
		LLVMRelocPIC,
		LLVMRelocDynamicNoPic

	enum LLVMCoreModel:
		LLVMCodeModelDefault,
		LLVMCodeModelJITDefault,
		LLVMCodeModelSmall,
		LLVMCodeModelKernel,
		LLVMCodeModelMedium,
		LLVMCodeModelLarge

	enum LLVMCodeGenFileType:
		LLVMAssemblyFile,
		LLVMObjectFile

	LLVMTargetRef LLVMGetFirstTarget()
	LLVMTargetRef LLVMGetNextTarget(LLVMTargetRef)

	#===-- Target ------------------------------------------------------------===
	char* LLVMGetTargetName(LLVMTargetRef)
	char* LLVMGetTargetDescription(LLVMTargetRef)
	bint LLVMTargetHasJIT(LLVMTargetRef)
	bint LLVMTargetHasTargetMachine(LLVMTargetRef)
	bint LLVMTargetHasAsmBackend(LLVMTargetRef)

	#===-- Target Machine ----------------------------------------------------===
	LLVMTargetMachineRef LLVMCreateTargetMachine(LLVMTargetRef, char* Triple, char* CPU, char* Features, LLVMCodeGenOptLevel, LLVMRelocMode, LLVMCoreModel)
	void LLVMDisposeTargetMachine(LLVMTargetMachineRef)
	LLVMTargetRef LLVMGetTargetMachineTarget(LLVMTargetMachineRef)
	char* LLVMGetTargetMachineTriple(LLVMTargetMachineRef) # result needs to be disposed with LLVMDisposeMessage.
	char *LLVMGetTargetMachineCPU(LLVMTargetMachineRef T) # result needs to be disposed with LLVMDisposeMessage.
	char *LLVMGetTargetMachineFeatureString(LLVMTargetMachineRef) # result needs to be disposed with LLVMDisposeMessage.
	LLVMTargetDataRef LLVMGetTargetMachineData(LLVMTargetMachineRef)

	bint LLVMTargetMachineEmitToFile(LLVMTargetMachineRef, LLVMModuleRef, char* Filename, LLVMCodeGenFileType, char **ErrorMessage)
	# ^ Use LLVMDisposeMessage to dispose ErrorMessage

	LLVMTarget* unwrap_Target "::llvm::unwrap" (LLVMTargetRef)
	LLVMTargetRef wrap_Target "::llvm::wrap" (LLVMTarget*)

cdef extern from "llvm/Support/TargetRegistry.h" namespace "llvm::TargetRegistry":
	LLVMTarget* LLVMLookupTarget "llvm::TargetRegistry::lookupTarget"(string, string&)
