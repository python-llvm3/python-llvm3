#pragma once

#include <llvm/Config/llvm-config.h>

#ifndef LLVM_DEFAULT_TARGET_TRIPLE
# define LLVM_DEFAULT_TARGET_TRIPLE LLVM_HOSTTRIPLE
#endif


#define PYLLVM_xstr(s) PYLLVM_str(s)
#define PYLLVM_str(s) #s

#define LLVM_NATIVE_ARCH_STR PYLLVM_xstr(LLVM_NATIVE_ARCH)

#ifndef LLVM_HAS_ATOMICS
# define LLVM_HAS_ATOMICS 0
#endif

#ifndef LLVM_ENABLE_THREADS
# define LLVM_ENABLE_THREADS 0
#endif

#ifndef LLVM_VERSION_MAJOR
# define LLVM_VERSION_MAJOR 0
#endif

#ifndef LLVM_VERSION_MINOR
# define LLVM_VERSION_MINOR 0
#endif
