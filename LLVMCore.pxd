#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

ctypedef bint bool
from libc.stdint cimport uintptr_t, uint64_t, uint32_t, uint16_t, uint8_t, int64_t, int32_t, int16_t, int8_t

cdef extern from "string" namespace "std":
	cdef cppclass string:
		string()
		string(char*)
		char* c_str()

cdef extern from "llvm/ADT/StringRef.h" namespace "llvm":
	cdef cppclass StringRef:
		StringRef()
		StringRef(char*)
		StringRef(char*, size_t)
		char* data()
		bool empty()
		size_t size()
		bool equals(StringRef)
		bool equals_lower(StringRef)
		int compare(StringRef)
		int compare_lower(StringRef)
		unsigned edit_distance(StringRef, bool, unsigned)
		bool getAsInteger(unsigned, long long &)
		bool getAsInteger(unsigned, unsigned long long &)

cdef extern from "llvm/IR/Value.h" namespace "llvm":
	cdef cppclass Value:
		pass

cdef extern from "llvm/IR/LLVMContext.h" namespace "llvm":
	cdef cppclass LLVMContext:
		pass

cdef extern from "llvm/IR/Constants.h" namespace "llvm":
	cdef cppclass Type:
		pass
	cdef cppclass ArrayRef[T]:
		ArrayRef ()
		ArrayRef (T *, unsigned long)
		ArrayRef (T &)

	cdef cppclass ConstantDataSequential(Value):
		bool isString()
		bool isCString()
		unsigned getNumElements()
		unsigned getElementByteSize()
		unsigned getElementAsInteger(unsigned)
		float getElementAsFloat(unsigned)
		double getElementAsDouble(unsigned)
		StringRef getAsString()
		StringRef getAsCString()
		Value *getElementAsConstant(unsigned)

	cdef Value *ConstantDataArray_get "llvm::ConstantDataArray::get" (LLVMContext&, ArrayRef[uint8_t])
	cdef Value *ConstantDataArray_get "llvm::ConstantDataArray::get" (LLVMContext&, ArrayRef[uint16_t])
	cdef Value *ConstantDataArray_get "llvm::ConstantDataArray::get" (LLVMContext&, ArrayRef[uint32_t])
	cdef Value *ConstantDataArray_get "llvm::ConstantDataArray::get" (LLVMContext&, ArrayRef[uint64_t])
	cdef Value *ConstantDataArray_get "llvm::ConstantDataArray::get" (LLVMContext&, ArrayRef[float])
	cdef Value *ConstantDataArray_get "llvm::ConstantDataArray::get" (LLVMContext&, ArrayRef[double])

	ctypedef enum roundingMode "llvm::APFloat::roundingMode":
		rmNearestTiesToEven
		rmTowardPositive
		rmTowardNegative
		rmTowardZero
		rmNearestTiesToAway

	cdef struct fltSemantics:
		pass

	cdef cppclass APFloat:
		APFloat(double)
		APFloat(float)
		APFloat(APFloat)
		bool isNegative()
		bool isNormal()
		bool isFinite()
		bool isZero()
		bool isDenormal()
		bool isInfinity()
		bool isNaN()
		bool isSignaling()
		bool isNonZero()
		bool isFiniteNonZero()
		bool isPosZero()
		bool isNegZero()
		bool isSmallest()
		bool isLargest()
		double convertToDouble()
		float convertToFloat()
		unsigned convertToHexString(char *, int, bool, roundingMode)
		fltSemantics& getSemantics()

	cdef fltSemantics IEEEhalf "llvm::APFloat::IEEEhalf"
	cdef fltSemantics IEEEsingle "llvm::APFloat::IEEEsingle"
	cdef fltSemantics IEEEdouble "llvm::APFloat::IEEEdouble"
	cdef fltSemantics IEEEquad "llvm::APFloat::IEEEquad"
	cdef fltSemantics x87DoubleExtended "llvm::APFloat::x87DoubleExtended"
	cdef fltSemantics PPCDoubleDouble "llvm::APFloat::PPCDoubleDouble"

	cdef cppclass ConstantFP(Value):
		bool isZero()
		bool isNegative()
		bool isNaN()
		bool isExactlyValue(double)
		APFloat &getValueAPF()


cdef inline bytes stringref2bytes(StringRef ref):
	cdef size_t size = ref.size()
	return ref.data()[:size]

cdef inline str stringref2str(StringRef ref):
	cdef size_t size = ref.size()
	return ref.data()[:size].decode('UTF-8')

cdef extern from "llvm/IR/Metadata.h" namespace "llvm":
	cdef cppclass MDNode(Value):
		pass

	cdef cppclass NamedMDNode(Value):
		StringRef getName()
		void addOperand(MDNode*)

cdef extern from "llvm/IR/Module.h" namespace "llvm":

	cdef cppclass Module:
		NamedMDNode* getNamedMetadata(char*)
		NamedMDNode* getOrInsertNamedMetadata(char*)
		void eraseNamedMetadata(NamedMDNode*)

cdef extern from "llvm-c/Core.h":

	ctypedef char* const_char_ptr "const char*"
	ctypedef bint LLVMBool

	cdef struct LLVMOpaqueContext:
		pass
	ctypedef LLVMOpaqueContext* LLVMContextRef

	ctypedef struct LLVMOpaqueModule:
		pass
	ctypedef LLVMOpaqueModule* LLVMModuleRef

	ctypedef struct LLVMOpaqueType:
		pass
	ctypedef LLVMOpaqueType* LLVMTypeRef

	ctypedef struct LLVMOpaqueValue:
		pass
	ctypedef LLVMOpaqueValue* LLVMValueRef

	ctypedef struct LLVMOpaqueBasicBlock:
		pass
	ctypedef LLVMOpaqueBasicBlock* LLVMBasicBlockRef

	ctypedef struct LLVMOpaqueBuilder:
		pass
	ctypedef LLVMOpaqueBuilder* LLVMBuilderRef

	ctypedef struct LLVMOpaqueModuleProvider:
		pass
	ctypedef LLVMOpaqueModuleProvider* LLVMModuleProviderRef

	ctypedef struct LLVMOpaqueMemoryBuffer:
		pass
	ctypedef LLVMOpaqueMemoryBuffer* LLVMMemoryBufferRef

	ctypedef struct LLVMOpaquePassManager:
		pass
	ctypedef LLVMOpaquePassManager* LLVMPassManagerRef

	ctypedef struct LLVMOpaquePassRegistry:
		pass
	ctypedef LLVMOpaquePassRegistry* LLVMPassRegistryRef

	ctypedef struct LLVMOpaqueUse:
		pass
	ctypedef LLVMOpaqueUse* LLVMUseRef

	ctypedef enum LLVMAttribute:
		LLVMZExtAttribute
		LLVMSExtAttribute
		LLVMNoReturnAttribute
		LLVMInRegAttribute
		LLVMStructRetAttribute
		LLVMNoUnwindAttribute
		LLVMNoAliasAttribute
		LLVMByValAttribute
		LLVMNestAttribute
		LLVMReadNoneAttribute
		LLVMReadOnlyAttribute
		LLVMNoInlineAttribute
		LLVMAlwaysInlineAttribute
		LLVMOptimizeForSizeAttribute
		LLVMStackProtectAttribute
		LLVMStackProtectReqAttribute
		LLVMAlignment
		LLVMNoCaptureAttribute
		LLVMNoRedZoneAttribute
		LLVMNoImplicitFloatAttribute
		LLVMNakedAttribute
		LLVMInlineHintAttribute
		LLVMStackAlignment
		LLVMReturnsTwice
		LLVMUWTable
		LLVMNonLazyBind

	ctypedef enum LLVMOpcode:
		# Terminator Instructions
		LLVMRet
		LLVMBr
		LLVMSwitch
		LLVMIndirectBr
		LLVMInvoke
		LLVMUnreachable

		# Standard Binary Operators
		LLVMAdd
		LLVMFAdd
		LLVMSub
		LLVMFSub
		LLVMMul
		LLVMFMul
		LLVMUDiv
		LLVMSDiv
		LLVMFDiv
		LLVMURem
		LLVMSRem
		LLVMFRem
		# Logical Operators
		LLVMShl
		LLVMLShr
		LLVMAShr
		LLVMAnd
		LLVMOr
		LLVMXor

		# Memory Operators
		LLVMAlloca
		LLVMLoad
		LLVMStore
		LLVMGetElementPtr

		# Cast Operators
		LLVMTrunc
		LLVMZExt
		LLVMSExt
		LLVMFPToUI
		LLVMFPToSI
		LLVMUIToFP
		LLVMSIToFP
		LLVMFPTrunc
		LLVMFPExt
		LLVMPtrToInt
		LLVMIntToPtr
		LLVMBitCast
		LLVMAddrSpaceCast
		# Other Operators
		LLVMICmp
		LLVMFCmp
		LLVMPHI
		LLVMCall
		LLVMSelect
		LLVMUserOp1
		LLVMUserOp2
		LLVMVAArg
		LLVMExtractElement
		LLVMInsertElement
		LLVMShuffleVector
		LLVMExtractValue
		LLVMInsertValue
		# Atomic operators
		LLVMFence
		LLVMAtomicCmpXchg
		LLVMAtomicRMW
		# Exception Handling Operators
		LLVMResume
		LLVMLandingPad
		LLVMUnwind

	ctypedef enum LLVMTypeKind:
		LLVMVoidTypeKind
		LLVMHalfTypeKind
		LLVMFloatTypeKind
		LLVMDoubleTypeKind
		LLVMX86_FP80TypeKind
		LLVMFP128TypeKind
		LLVMPPC_FP128TypeKind
		LLVMLabelTypeKind
		LLVMIntegerTypeKind
		LLVMFunctionTypeKind
		LLVMStructTypeKind
		LLVMArrayTypeKind
		LLVMPointerTypeKind
		LLVMVectorTypeKind
		LLVMMetadataTypeKind
		LLVMX86_MMXTypeKind

	ctypedef enum LLVMLinkage:
		LLVMExternalLinkage
		LLVMAvailableExternallyLinkage
		LLVMLinkOnceAnyLinkage
		LLVMLinkOnceODRLinkage
		LLVMWeakAnyLinkage
		LLVMWeakODRLinkage
		LLVMAppendingLinkage
		LLVMInternalLinkage
		LLVMPrivateLinkage
		LLVMExternalWeakLinkage
		LLVMCommonLinkage
		LLVMLinkerPrivateLinkage
		LLVMLinkerPrivateWeakLinkage

	ctypedef enum LLVMVisibility:
		LLVMDefaultVisibility
		LLVMHiddenVisibility
		LLVMProtectedVisibility

	ctypedef enum LLVMDLLStorageClass:
		LLVMDefaultStorageClass
		LLVMDLLImportStorageClass
		LLVMDLLExportStorageClass

	ctypedef enum LLVMCallConv:
		LLVMCCallConv
		LLVMFastCallConv
		LLVMColdCallConv
		LLVMWebKitJSCallConv
		LLVMAnyRegCallConv
		LLVMX86StdcallCallConv
		LLVMX86FastcallCallConv

	ctypedef enum LLVMIntPredicate:
		LLVMIntEQ
		LLVMIntNE
		LLVMIntUGT
		LLVMIntUGE
		LLVMIntULT
		LLVMIntULE
		LLVMIntSGT
		LLVMIntSGE
		LLVMIntSLT
		LLVMIntSLE

	ctypedef enum LLVMRealPredicate:
		LLVMRealPredicateFalse
		LLVMRealOEQ
		LLVMRealOGT
		LLVMRealOGE
		LLVMRealOLT
		LLVMRealOLE
		LLVMRealONE
		LLVMRealORD
		LLVMRealUNO
		LLVMRealUEQ
		LLVMRealUGT
		LLVMRealUGE
		LLVMRealULT
		LLVMRealULE
		LLVMRealUNE
		LLVMRealPredicateTrue

	ctypedef enum LLVMLandingPadClauseTy:
		LLVMLandingPadCatch
		LLVMLandingPadFilter

	ctypedef enum LLVMThreadLocalMode:
		LLVMNotThreadLocal
		LLVMGeneralDynamicTLSModel
		LLVMLocalDynamicTLSModel
		LLVMInitialExecTLSModel
		LLVMLocalExecTLSModel



	ctypedef enum LLVMAtomicOrdering:
		LLVMAtomicOrderingNotAtomic
		LLVMAtomicOrderingUnordered
		LLVMAtomicOrderingMonotonic
		LLVMAtomicOrderingAcquire
		LLVMAtomicOrderingRelease
		LLVMAtomicOrderingAcquireRelease
		LLVMAtomicOrderingSequentiallyConsistent

	ctypedef enum LLVMAtomicRMWBinOp:
		LLVMAtomicRMWBinOpXchg
		LLVMAtomicRMWBinOpAdd
		LLVMAtomicRMWBinOpSub
		LLVMAtomicRMWBinOpAnd
		LLVMAtomicRMWBinOpNand
		LLVMAtomicRMWBinOpOr
		LLVMAtomicRMWBinOpXor
		LLVMAtomicRMWBinOpMax
		LLVMAtomicRMWBinOpMin
		LLVMAtomicRMWBinOpUMax
		LLVMAtomicRMWBinOpUMin

	void LLVMInitializeCore(LLVMPassRegistryRef)

	# = Error handling =
	char* LLVMCreateMessage(char*)
	void LLVMDisposeMessage(char*)
	ctypedef void (*LLVMFatalErrorHandler)(char *Reason)
	void LLVMInstallFatalErrorHandler(LLVMFatalErrorHandler Handler)
	void LLVMResetFatalErrorHandler()

	# = Contexts =
	LLVMContextRef LLVMContextCreate()
	LLVMContextRef LLVMGetGlobalContext()
	void LLVMContextDispose(LLVMContextRef)

	unsigned LLVMGetMDKindIDInContext(LLVMContextRef, char*, unsigned)
	unsigned LLVMGetMDKindID(char*, unsigned)

	# = Modules =

	# Create and destroy modules.
	LLVMModuleRef LLVMModuleCreateWithName(char *)
	LLVMModuleRef LLVMModuleCreateWithNameInContext(char *, LLVMContextRef)

	void LLVMDisposeModule(LLVMModuleRef)

	# Data layout.
	const_char_ptr LLVMGetDataLayout(LLVMModuleRef)
	void LLVMSetDataLayout(LLVMModuleRef, char*)

	# Target triple
	const_char_ptr LLVMGetTarget(LLVMModuleRef)
	void LLVMSetTarget(LLVMModuleRef, char*)

	void LLVMDumpModule(LLVMModuleRef)
	LLVMBool LLVMPrintModuleToFile(LLVMModuleRef, char*, char**)
	char* LLVMPrintModuleToString(LLVMModuleRef)

	void LLVMSetModuleInlineAsm(LLVMModuleRef, char*)

	LLVMContextRef LLVMGetModuleContext(LLVMModuleRef)

	# = Types =

	LLVMTypeKind LLVMGetTypeKind(LLVMTypeRef)
	LLVMBool LLVMTypeIsSized(LLVMTypeRef)

	LLVMContextRef LLVMGetTypeContext(LLVMTypeRef)
	void LLVMDumpType(LLVMTypeRef)
	char* LLVMPrintTypeToString(LLVMTypeRef)


	# Operations on integer types
	LLVMTypeRef LLVMInt1TypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMInt8TypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMInt16TypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMInt32TypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMInt64TypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMIntTypeInContext(LLVMContextRef, unsigned)

	LLVMTypeRef LLVMInt1Type()
	LLVMTypeRef LLVMInt8Type()
	LLVMTypeRef LLVMInt16Type()
	LLVMTypeRef LLVMInt32Type()
	LLVMTypeRef LLVMInt64Type()
	LLVMTypeRef LLVMIntType(unsigned)
	unsigned LLVMGetIntTypeWidth(LLVMTypeRef IntegerTy)

	# Operations on real types
	LLVMTypeRef LLVMHalfTypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMFloatTypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMDoubleTypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMX86FP80TypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMFP128TypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMPPCFP128TypeInContext(LLVMContextRef)

	LLVMTypeRef LLVMHalfType()
	LLVMTypeRef LLVMFloatType()
	LLVMTypeRef LLVMDoubleType()
	LLVMTypeRef LLVMX86FP80Type()
	LLVMTypeRef LLVMFP128Type()
	LLVMTypeRef LLVMPPCFP128Type()

	# Operations on function types
	LLVMTypeRef LLVMFunctionType(LLVMTypeRef, LLVMTypeRef*, unsigned, LLVMBool)
	LLVMBool LLVMIsFunctionVarArg(LLVMTypeRef)
	LLVMTypeRef LLVMGetReturnType(LLVMTypeRef)
	unsigned LLVMCountParamTypes(LLVMTypeRef)
	void LLVMGetParamTypes(LLVMTypeRef, LLVMTypeRef*)

	# Operations on struct types
	LLVMTypeRef LLVMStructTypeInContext(LLVMContextRef, LLVMTypeRef*, unsigned, LLVMBool)
	LLVMTypeRef LLVMStructType(LLVMTypeRef*, unsigned, LLVMBool)
	LLVMTypeRef LLVMStructCreateNamed(LLVMContextRef, char*)
	const_char_ptr LLVMGetStructName(LLVMTypeRef)
	void LLVMStructSetBody(LLVMTypeRef, LLVMTypeRef*, unsigned, LLVMBool)

	unsigned LLVMCountStructElementTypes(LLVMTypeRef)
	void LLVMGetStructElementTypes(LLVMTypeRef, LLVMTypeRef*)
	LLVMBool LLVMIsPackedStruct(LLVMTypeRef)
	LLVMBool LLVMIsOpaqueStruct(LLVMTypeRef)

	LLVMTypeRef LLVMGetTypeByName(LLVMModuleRef, char*)

	# Operations on array, pointer, and vector types (sequence types)
	LLVMTypeRef LLVMArrayType(LLVMTypeRef, unsigned)
	LLVMTypeRef LLVMPointerType(LLVMTypeRef, unsigned)
	LLVMTypeRef LLVMVectorType(LLVMTypeRef, unsigned)

	LLVMTypeRef LLVMGetElementType(LLVMTypeRef)
	unsigned LLVMGetArrayLength(LLVMTypeRef ArrayTy)
	unsigned LLVMGetPointerAddressSpace(LLVMTypeRef PointerTy)
	unsigned LLVMGetVectorSize(LLVMTypeRef VectorTy)

	# Operations on other types
	LLVMTypeRef LLVMVoidTypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMLabelTypeInContext(LLVMContextRef)
	LLVMTypeRef LLVMX86MMXTypeInContext(LLVMContextRef)

	LLVMTypeRef LLVMVoidType()
	LLVMTypeRef LLVMLabelType()
	LLVMTypeRef LLVMX86MMXType()

	# = Values =

	# Operations on all values
	LLVMTypeRef LLVMTypeOf(LLVMValueRef)
	char* LLVMGetValueName(LLVMValueRef)
	void LLVMSetValueName(LLVMValueRef, char*)
	void LLVMDumpValue(LLVMValueRef)
	char* LLVMPrintValueToString(LLVMValueRef)
	void LLVMReplaceAllUsesWith(LLVMValueRef, LLVMValueRef)
	bint LLVMHasMetadata(LLVMValueRef)
	LLVMValueRef LLVMGetMetadata(LLVMValueRef, unsigned)
	void LLVMSetMetadata(LLVMValueRef, unsigned, LLVMValueRef)

	LLVMValueRef LLVMIsAArgument(LLVMValueRef)
	LLVMValueRef LLVMIsABasicBlock(LLVMValueRef)
	LLVMValueRef LLVMIsAInlineAsm(LLVMValueRef)
	LLVMValueRef LLVMIsAMDNode(LLVMValueRef)
	LLVMValueRef LLVMIsAMDString(LLVMValueRef)
	LLVMValueRef LLVMIsAUser(LLVMValueRef)
	LLVMValueRef LLVMIsAConstant(LLVMValueRef)
	LLVMValueRef LLVMIsABlockAddress(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantAggregateZero(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantArray(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantDataSequential(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantDataArray(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantExpr(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantFP(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantInt(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantPointerNull(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantStruct(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantDataVector(LLVMValueRef)
	LLVMValueRef LLVMIsAConstantVector(LLVMValueRef)
	LLVMValueRef LLVMIsAGlobalValue(LLVMValueRef)
	LLVMValueRef LLVMIsAFunction(LLVMValueRef)
	LLVMValueRef LLVMIsAGlobalAlias(LLVMValueRef)
	LLVMValueRef LLVMIsAGlobalVariable(LLVMValueRef)
	LLVMValueRef LLVMIsAUndefValue(LLVMValueRef)
	LLVMValueRef LLVMIsAInstruction(LLVMValueRef)
	LLVMValueRef LLVMIsABinaryOperator(LLVMValueRef)
	LLVMValueRef LLVMIsACallInst(LLVMValueRef)
	LLVMValueRef LLVMIsAIntrinsicInst(LLVMValueRef)
	LLVMValueRef LLVMIsADbgInfoIntrinsic(LLVMValueRef)
	LLVMValueRef LLVMIsADbgDeclareInst(LLVMValueRef)
	LLVMValueRef LLVMIsAMemIntrinsic(LLVMValueRef)
	LLVMValueRef LLVMIsAMemCpyInst(LLVMValueRef)
	LLVMValueRef LLVMIsAMemMoveInst(LLVMValueRef)
	LLVMValueRef LLVMIsAMemSetInst(LLVMValueRef)
	LLVMValueRef LLVMIsACmpInst(LLVMValueRef)
	LLVMValueRef LLVMIsAFCmpInst(LLVMValueRef)
	LLVMValueRef LLVMIsAICmpInst(LLVMValueRef)
	LLVMValueRef LLVMIsAExtractElementInst(LLVMValueRef)
	LLVMValueRef LLVMIsAGetElementPtrInst(LLVMValueRef)
	LLVMValueRef LLVMIsAInsertElementInst(LLVMValueRef)
	LLVMValueRef LLVMIsAInsertValueInst(LLVMValueRef)
	LLVMValueRef LLVMIsALandingPadInst(LLVMValueRef)
	LLVMValueRef LLVMIsAPHINode(LLVMValueRef)
	LLVMValueRef LLVMIsASelectInst(LLVMValueRef)
	LLVMValueRef LLVMIsAShuffleVectorInst(LLVMValueRef)
	LLVMValueRef LLVMIsAStoreInst(LLVMValueRef)
	LLVMValueRef LLVMIsATerminatorInst(LLVMValueRef)
	LLVMValueRef LLVMIsABranchInst(LLVMValueRef)
	LLVMValueRef LLVMIsAIndirectBrInst(LLVMValueRef)
	LLVMValueRef LLVMIsAInvokeInst(LLVMValueRef)
	LLVMValueRef LLVMIsAReturnInst(LLVMValueRef)
	LLVMValueRef LLVMIsASwitchInst(LLVMValueRef)
	LLVMValueRef LLVMIsAUnreachableInst(LLVMValueRef)
	LLVMValueRef LLVMIsAResumeInst(LLVMValueRef)
	LLVMValueRef LLVMIsAUnaryInstruction(LLVMValueRef)
	LLVMValueRef LLVMIsAAllocaInst(LLVMValueRef)
	LLVMValueRef LLVMIsACastInst(LLVMValueRef)
	LLVMValueRef LLVMIsABitCastInst(LLVMValueRef)
	LLVMValueRef LLVMIsAAddrSpaceCastInst(LLVMValueRef)
	LLVMValueRef LLVMIsAFPExtInst(LLVMValueRef)
	LLVMValueRef LLVMIsAFPToSIInst(LLVMValueRef)
	LLVMValueRef LLVMIsAFPToUIInst(LLVMValueRef)
	LLVMValueRef LLVMIsAFPTruncInst(LLVMValueRef)
	LLVMValueRef LLVMIsAIntToPtrInst(LLVMValueRef)
	LLVMValueRef LLVMIsAPtrToIntInst(LLVMValueRef)
	LLVMValueRef LLVMIsASExtInst(LLVMValueRef)
	LLVMValueRef LLVMIsASIToFPInst(LLVMValueRef)
	LLVMValueRef LLVMIsATruncInst(LLVMValueRef)
	LLVMValueRef LLVMIsAUIToFPInst(LLVMValueRef)
	LLVMValueRef LLVMIsAZExtInst(LLVMValueRef)
	LLVMValueRef LLVMIsAExtractValueInst(LLVMValueRef)
	LLVMValueRef LLVMIsALoadInst(LLVMValueRef)
	LLVMValueRef LLVMIsAVAArgInst(LLVMValueRef)
	#LLVMValueRef LLVMIsAAtomicRMWInst(LLVMValueRef)
	#LLVMValueRef LLVMIsAFenceInst(LLVMValueRef)

	# Operations on Uses
	LLVMUseRef LLVMGetFirstUse(LLVMValueRef)
	LLVMUseRef LLVMGetNextUse(LLVMUseRef)
	LLVMValueRef LLVMGetUser(LLVMUseRef)
	LLVMValueRef LLVMGetUsedValue(LLVMUseRef)

	# Operations on Users
	LLVMValueRef LLVMGetOperand(LLVMValueRef, unsigned)
	void LLVMSetOperand(LLVMValueRef, unsigned, LLVMValueRef)
	int LLVMGetNumOperands(LLVMValueRef)

	# Operations on constants of any type
	LLVMValueRef LLVMConstNull(LLVMTypeRef)
	LLVMValueRef LLVMConstAllOnes(LLVMTypeRef)
	LLVMValueRef LLVMGetUndef(LLVMTypeRef)
	LLVMBool LLVMIsConstant(LLVMValueRef)
	LLVMBool LLVMIsNull(LLVMValueRef)
	LLVMBool LLVMIsUndef(LLVMValueRef)
	LLVMValueRef LLVMConstPointerNull(LLVMTypeRef)

	# Operations on metadata
	LLVMValueRef LLVMMDStringInContext(LLVMContextRef, char*, unsigned)
	LLVMValueRef LLVMMDString(char*, unsigned)
	LLVMValueRef LLVMMDNodeInContext(LLVMContextRef, LLVMValueRef*, unsigned)
	LLVMValueRef LLVMMDNode(LLVMValueRef*, unsigned)
	const_char_ptr LLVMGetMDString(LLVMValueRef, unsigned*)
	unsigned LLVMGetNamedMetadataNumOperands(LLVMModuleRef, char*)
	void LLVMGetNamedMetadataOperands(LLVMModuleRef, char*, LLVMValueRef*)

	# Operations on scalar constants
	LLVMValueRef LLVMConstInt(LLVMTypeRef, unsigned long long, LLVMBool)
	LLVMValueRef LLVMConstIntOfArbitraryPrecision(LLVMTypeRef, unsigned, uint64_t*)
	LLVMValueRef LLVMConstIntOfString(LLVMTypeRef, char*, uint8_t)
	LLVMValueRef LLVMConstIntOfStringAndSize(LLVMTypeRef, char*, unsigned, uint8_t)
	LLVMValueRef LLVMConstReal(LLVMTypeRef, double)

	LLVMValueRef LLVMConstRealOfString(LLVMTypeRef, char*)
	LLVMValueRef LLVMConstRealOfStringAndSize(LLVMTypeRef, char*, unsigned)
	unsigned long long LLVMConstIntGetZExtValue(LLVMValueRef)
	long long LLVMConstIntGetSExtValue(LLVMValueRef)

	# Operations on composite constants
	LLVMValueRef LLVMConstStringInContext(LLVMContextRef, char*, unsigned, LLVMBool)
	LLVMValueRef LLVMConstStructInContext(LLVMContextRef, LLVMValueRef *, unsigned, LLVMBool)

	LLVMValueRef LLVMConstString(char*, unsigned, LLVMBool)
	LLVMValueRef LLVMConstArray(LLVMTypeRef, LLVMValueRef*, unsigned)
	LLVMValueRef LLVMConstStruct(LLVMValueRef*, unsigned, LLVMBool)
	LLVMValueRef LLVMConstNamedStruct(LLVMTypeRef, LLVMValueRef*, unsigned)
	LLVMValueRef LLVMConstVector(LLVMValueRef*, unsigned)

	# Constant expressions
	LLVMOpcode LLVMGetConstOpcode(LLVMValueRef)
	LLVMValueRef LLVMAlignOf(LLVMTypeRef)
	LLVMValueRef LLVMSizeOf(LLVMTypeRef)
	LLVMValueRef LLVMConstNeg(LLVMValueRef)
	LLVMValueRef LLVMConstNSWNeg(LLVMValueRef)
	LLVMValueRef LLVMConstNUWNeg(LLVMValueRef)
	LLVMValueRef LLVMConstFNeg(LLVMValueRef)
	LLVMValueRef LLVMConstNot(LLVMValueRef)
	LLVMValueRef LLVMConstAdd(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstNSWAdd(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstNUWAdd(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstFAdd(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstSub(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstNSWSub(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstNUWSub(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstFSub(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstMul(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstNSWMul(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstNUWMul(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstFMul(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstUDiv(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstSDiv(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstExactSDiv(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstFDiv(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstURem(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstSRem(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstFRem(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstAnd(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstOr(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstXor(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstICmp(LLVMIntPredicate, LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstFCmp(LLVMRealPredicate, LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstShl(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstLShr(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstAShr(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstGEP(LLVMValueRef, LLVMValueRef*, unsigned)
	LLVMValueRef LLVMConstInBoundsGEP(LLVMValueRef, LLVMValueRef*, unsigned)
	LLVMValueRef LLVMConstTrunc(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstSExt(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstZExt(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstFPTrunc(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstFPExt(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstUIToFP(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstSIToFP(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstFPToUI(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstFPToSI(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstPtrToInt(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstIntToPtr(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstBitCast(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstAddrSpaceCast(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstZExtOrBitCast(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstSExtOrBitCast(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstTruncOrBitCast(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstPointerCast(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstIntCast(LLVMValueRef, LLVMTypeRef, LLVMBool)
	LLVMValueRef LLVMConstFPCast(LLVMValueRef, LLVMTypeRef)
	LLVMValueRef LLVMConstSelect(LLVMValueRef, LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstExtractElement(LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstInsertElement(LLVMValueRef, LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstShuffleVector(LLVMValueRef, LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMConstExtractValue(LLVMValueRef, unsigned*, unsigned)
	LLVMValueRef LLVMConstInsertValue(LLVMValueRef, LLVMValueRef, unsigned*, unsigned)
	LLVMValueRef LLVMConstInlineAsm(LLVMTypeRef, char*, char*, LLVMBool, LLVMBool)
	LLVMValueRef LLVMBlockAddress(LLVMValueRef, LLVMBasicBlockRef)

	# Operations on global variables, functions, and aliases (globals)
	LLVMModuleRef LLVMGetGlobalParent(LLVMValueRef)
	LLVMBool LLVMIsDeclaration(LLVMValueRef)
	LLVMLinkage LLVMGetLinkage(LLVMValueRef)
	void LLVMSetLinkage(LLVMValueRef, LLVMLinkage)
	char* LLVMGetSection(LLVMValueRef)
	void LLVMSetSection(LLVMValueRef, char*)
	LLVMVisibility LLVMGetVisibility(LLVMValueRef)
	void LLVMSetVisibility(LLVMValueRef, LLVMVisibility)
	LLVMDLLStorageClass LLVMGetDLLStorageClass(LLVMValueRef)
	void LLVMSetDLLStorageClass(LLVMValueRef, LLVMDLLStorageClass)
	LLVMBool LLVMHasUnnamedAddr(LLVMValueRef)
	void LLVMSetUnnamedAddr(LLVMValueRef, LLVMBool)
	unsigned LLVMGetAlignment(LLVMValueRef)
	void LLVMSetAlignment(LLVMValueRef, unsigned)

	# Operations on global variables
	LLVMValueRef LLVMAddGlobal(LLVMModuleRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMAddGlobalInAddressSpace(LLVMModuleRef, LLVMTypeRef, char*, unsigned)
	LLVMValueRef LLVMGetNamedGlobal(LLVMModuleRef, char*)
	LLVMValueRef LLVMGetFirstGlobal(LLVMModuleRef)
	LLVMValueRef LLVMGetLastGlobal(LLVMModuleRef)
	LLVMValueRef LLVMGetNextGlobal(LLVMValueRef)
	LLVMValueRef LLVMGetPreviousGlobal(LLVMValueRef)
	void LLVMDeleteGlobal(LLVMValueRef)
	LLVMValueRef LLVMGetInitializer(LLVMValueRef)
	void LLVMSetInitializer(LLVMValueRef, LLVMValueRef)
	LLVMBool LLVMIsThreadLocal(LLVMValueRef)
	void LLVMSetThreadLocal(LLVMValueRef, LLVMBool)
	LLVMBool LLVMIsGlobalConstant(LLVMValueRef)
	void LLVMSetGlobalConstant(LLVMValueRef, LLVMBool)

	# Operations on aliases
	LLVMValueRef LLVMAddAlias(LLVMModuleRef, LLVMTypeRef, LLVMValueRef, char*)

	# Operations on functions
	LLVMValueRef LLVMAddFunction(LLVMModuleRef, char*, LLVMTypeRef)
	LLVMValueRef LLVMGetNamedFunction(LLVMModuleRef, char*)
	LLVMValueRef LLVMGetFirstFunction(LLVMModuleRef)
	LLVMValueRef LLVMGetLastFunction(LLVMModuleRef)
	LLVMValueRef LLVMGetNextFunction(LLVMValueRef)
	LLVMValueRef LLVMGetPreviousFunction(LLVMValueRef)
	void LLVMDeleteFunction(LLVMValueRef)
	unsigned LLVMGetIntrinsicID(LLVMValueRef)
	unsigned LLVMGetFunctionCallConv(LLVMValueRef)
	void LLVMSetFunctionCallConv(LLVMValueRef, unsigned)
	char* LLVMGetGC(LLVMValueRef)
	void LLVMSetGC(LLVMValueRef, char*)
	void LLVMAddFunctionAttr(LLVMValueRef, LLVMAttribute)
	LLVMAttribute LLVMGetFunctionAttr(LLVMValueRef)
	void LLVMRemoveFunctionAttr(LLVMValueRef, LLVMAttribute)

	# Operations on parameters
	unsigned LLVMCountParams(LLVMValueRef)
	void LLVMGetParams(LLVMValueRef, LLVMValueRef *)
	LLVMValueRef LLVMGetParam(LLVMValueRef, unsigned)
	LLVMValueRef LLVMGetParamParent(LLVMValueRef)
	LLVMValueRef LLVMGetFirstParam(LLVMValueRef)
	LLVMValueRef LLVMGetLastParam(LLVMValueRef)
	LLVMValueRef LLVMGetNextParam(LLVMValueRef)
	LLVMValueRef LLVMGetPreviousParam(LLVMValueRef)
	void LLVMAddAttribute(LLVMValueRef, LLVMAttribute)
	void LLVMRemoveAttribute(LLVMValueRef, LLVMAttribute)
	LLVMAttribute LLVMGetAttribute(LLVMValueRef)
	void LLVMSetParamAlignment(LLVMValueRef, unsigned)

	# Operations on basic blocks
	LLVMValueRef LLVMBasicBlockAsValue(LLVMBasicBlockRef)
	LLVMBool LLVMValueIsBasicBlock(LLVMValueRef)
	LLVMBasicBlockRef LLVMValueAsBasicBlock(LLVMValueRef)
	LLVMValueRef LLVMGetBasicBlockParent(LLVMBasicBlockRef)
	LLVMValueRef LLVMGetBasicBlockTerminator(LLVMBasicBlockRef)
	unsigned LLVMCountBasicBlocks(LLVMValueRef)
	void LLVMGetBasicBlocks(LLVMValueRef, LLVMBasicBlockRef*)
	LLVMBasicBlockRef LLVMGetFirstBasicBlock(LLVMValueRef)
	LLVMBasicBlockRef LLVMGetLastBasicBlock(LLVMValueRef)
	LLVMBasicBlockRef LLVMGetNextBasicBlock(LLVMBasicBlockRef)
	LLVMBasicBlockRef LLVMGetPreviousBasicBlock(LLVMBasicBlockRef)
	LLVMBasicBlockRef LLVMGetEntryBasicBlock(LLVMValueRef)

	LLVMBasicBlockRef LLVMAppendBasicBlockInContext(LLVMContextRef, LLVMValueRef, char*)
	LLVMBasicBlockRef LLVMInsertBasicBlockInContext(LLVMContextRef, LLVMBasicBlockRef, char*)

	LLVMBasicBlockRef LLVMAppendBasicBlock(LLVMValueRef, char*)
	LLVMBasicBlockRef LLVMInsertBasicBlock(LLVMBasicBlockRef, char*)
	void LLVMDeleteBasicBlock(LLVMBasicBlockRef)
	void LLVMRemoveBasicBlockFromParent(LLVMBasicBlockRef)

	void LLVMMoveBasicBlockBefore(LLVMBasicBlockRef, LLVMBasicBlockRef)
	void LLVMMoveBasicBlockAfter(LLVMBasicBlockRef, LLVMBasicBlockRef)

	LLVMValueRef LLVMGetFirstInstruction(LLVMBasicBlockRef)
	LLVMValueRef LLVMGetLastInstruction(LLVMBasicBlockRef)

	# Operations on instructions
	LLVMBasicBlockRef LLVMGetInstructionParent(LLVMValueRef)
	LLVMValueRef LLVMGetNextInstruction(LLVMValueRef)
	LLVMValueRef LLVMGetPreviousInstruction(LLVMValueRef)
	void LLVMInstructionEraseFromParent(LLVMValueRef)
	LLVMOpcode   LLVMGetInstructionOpcode(LLVMValueRef)
	LLVMIntPredicate LLVMGetICmpPredicate(LLVMValueRef)

	# Operations on call sites
	void LLVMSetInstructionCallConv(LLVMValueRef, unsigned)
	unsigned LLVMGetInstructionCallConv(LLVMValueRef)
	void LLVMAddInstrAttribute(LLVMValueRef, unsigned, LLVMAttribute)
	void LLVMRemoveInstrAttribute(LLVMValueRef, unsigned,  LLVMAttribute)
	void LLVMSetInstrParamAlignment(LLVMValueRef, unsigned, unsigned)

	# Operations on call instructions (only)
	LLVMBool LLVMIsTailCall(LLVMValueRef)
	void LLVMSetTailCall(LLVMValueRef, LLVMBool)

	# Operations on switch instructions (only)
	LLVMBasicBlockRef LLVMGetSwitchDefaultDest(LLVMValueRef)

	# Operations on phi nodes
	void LLVMAddIncoming(LLVMValueRef, LLVMValueRef*, LLVMBasicBlockRef*, unsigned)
	unsigned LLVMCountIncoming(LLVMValueRef)
	LLVMValueRef LLVMGetIncomingValue(LLVMValueRef, unsigned)
	LLVMBasicBlockRef LLVMGetIncomingBlock(LLVMValueRef, unsigned)

	# = Instruction builders =

	LLVMBuilderRef LLVMCreateBuilderInContext(LLVMContextRef)
	LLVMBuilderRef LLVMCreateBuilder()
	void LLVMPositionBuilder(LLVMBuilderRef Builder, LLVMBasicBlockRef, LLVMValueRef)
	void LLVMPositionBuilderBefore(LLVMBuilderRef Builder, LLVMValueRef)
	void LLVMPositionBuilderAtEnd(LLVMBuilderRef, LLVMBasicBlockRef)
	LLVMBasicBlockRef LLVMGetInsertBlock(LLVMBuilderRef)
	void LLVMClearInsertionPosition(LLVMBuilderRef)
	void LLVMInsertIntoBuilder(LLVMBuilderRef, LLVMValueRef)
	void LLVMInsertIntoBuilderWithName(LLVMBuilderRef, LLVMValueRef, char*)
	void LLVMDisposeBuilder(LLVMBuilderRef)

	# Metadata
	void LLVMSetCurrentDebugLocation(LLVMBuilderRef, LLVMValueRef)
	LLVMValueRef LLVMGetCurrentDebugLocation(LLVMBuilderRef)
	void LLVMSetInstDebugLocation(LLVMBuilderRef, LLVMValueRef)

	# Terminators
	LLVMValueRef LLVMBuildRetVoid(LLVMBuilderRef)
	LLVMValueRef LLVMBuildRet(LLVMBuilderRef, LLVMValueRef)
	LLVMValueRef LLVMBuildAggregateRet(LLVMBuilderRef, LLVMValueRef*, unsigned)
	LLVMValueRef LLVMBuildBr(LLVMBuilderRef, LLVMBasicBlockRef)
	LLVMValueRef LLVMBuildCondBr(LLVMBuilderRef, LLVMValueRef, LLVMBasicBlockRef, LLVMBasicBlockRef)
	LLVMValueRef LLVMBuildSwitch(LLVMBuilderRef, LLVMValueRef, LLVMBasicBlockRef, unsigned)
	LLVMValueRef LLVMBuildIndirectBr(LLVMBuilderRef, LLVMValueRef, unsigned)
	LLVMValueRef LLVMBuildInvoke(LLVMBuilderRef, LLVMValueRef, LLVMValueRef*, unsigned, LLVMBasicBlockRef, LLVMBasicBlockRef, char*)
	LLVMValueRef LLVMBuildLandingPad(LLVMBuilderRef, LLVMTypeRef, LLVMValueRef, unsigned, char*)
	LLVMValueRef LLVMBuildResume(LLVMBuilderRef, LLVMValueRef)
	LLVMValueRef LLVMBuildUnreachable(LLVMBuilderRef)

	# Add a case to the switch instruction
	void LLVMAddCase(LLVMValueRef, LLVMValueRef, LLVMBasicBlockRef)

	# Add a destination to the indirectbr instruction
	void LLVMAddDestination(LLVMValueRef, LLVMBasicBlockRef)

	# Add a catch or filter clause to the landingpad instruction
	void LLVMAddClause(LLVMValueRef, LLVMValueRef)

	# Set the 'cleanup' flag in the landingpad instruction
	void LLVMSetCleanup(LLVMValueRef, LLVMBool)

	# Arithmetic
	LLVMValueRef LLVMBuildAdd(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNSWAdd(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNUWAdd(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFAdd(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildSub(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNSWSub(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNUWSub(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFSub(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildMul(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNSWMul(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNUWMul(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFMul(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildUDiv(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildSDiv(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildExactSDiv(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFDiv(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildURem(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildSRem(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFRem(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildShl(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildLShr(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildAShr(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildAnd(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildOr(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildXor(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildBinOp(LLVMBuilderRef, LLVMOpcode, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNeg(LLVMBuilderRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNSWNeg(LLVMBuilderRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNUWNeg(LLVMBuilderRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFNeg(LLVMBuilderRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildNot(LLVMBuilderRef, LLVMValueRef, char*)

	# Memory
	LLVMValueRef LLVMBuildMalloc(LLVMBuilderRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildArrayMalloc(LLVMBuilderRef, LLVMTypeRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildAlloca(LLVMBuilderRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildArrayAlloca(LLVMBuilderRef, LLVMTypeRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFree(LLVMBuilderRef, LLVMValueRef)
	LLVMValueRef LLVMBuildLoad(LLVMBuilderRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildStore(LLVMBuilderRef, LLVMValueRef, LLVMValueRef)
	LLVMValueRef LLVMBuildGEP(LLVMBuilderRef, LLVMValueRef, LLVMValueRef*, unsigned, char*)
	LLVMValueRef LLVMBuildInBoundsGEP(LLVMBuilderRef, LLVMValueRef, LLVMValueRef*, unsigned, char*)
	LLVMValueRef LLVMBuildStructGEP(LLVMBuilderRef, LLVMValueRef, unsigned, char*)
	LLVMValueRef LLVMBuildGlobalString(LLVMBuilderRef, char*, char*)
	LLVMValueRef LLVMBuildGlobalStringPtr(LLVMBuilderRef, char*, char*)

	# Casts
	LLVMValueRef LLVMBuildTrunc(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildZExt(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildSExt(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildFPToUI(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildFPToSI(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildUIToFP(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildSIToFP(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildFPTrunc(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildFPExt(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildPtrToInt(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildIntToPtr(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildBitCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildAddrSpaceCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildZExtOrBitCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildSExtOrBitCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildTruncOrBitCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildCast(LLVMBuilderRef, LLVMOpcode Op, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildPointerCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildIntCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*) # Signed Cast!
	LLVMValueRef LLVMBuildFPCast(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)

	# Comparisons
	LLVMValueRef LLVMBuildICmp(LLVMBuilderRef, LLVMIntPredicate, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildFCmp(LLVMBuilderRef, LLVMRealPredicate, LLVMValueRef, LLVMValueRef, char*)

	# Miscellaneous instructions
	LLVMValueRef LLVMBuildPhi(LLVMBuilderRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildCall(LLVMBuilderRef, LLVMValueRef, LLVMValueRef*, unsigned, char*)
	LLVMValueRef LLVMBuildSelect(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildVAArg(LLVMBuilderRef, LLVMValueRef, LLVMTypeRef, char*)
	LLVMValueRef LLVMBuildExtractElement(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildInsertElement(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildShuffleVector(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildExtractValue(LLVMBuilderRef, LLVMValueRef, unsigned, char*)
	LLVMValueRef LLVMBuildInsertValue(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, unsigned, char*)
	LLVMValueRef LLVMBuildFence(LLVMBuilderRef, LLVMAtomicOrdering, LLVMBool, char*)
	LLVMValueRef LLVMBuildAtomicRMW(LLVMBuilderRef, LLVMAtomicRMWBinOp, LLVMValueRef, LLVMValueRef, LLVMAtomicOrdering, LLVMBool)

	LLVMValueRef LLVMBuildIsNull(LLVMBuilderRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildIsNotNull(LLVMBuilderRef, LLVMValueRef, char*)
	LLVMValueRef LLVMBuildPtrDiff(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, char*)

	# = Module providers =

	# Changes the type of M so it can be passed to FunctionPassManagers and the
	# JIT.  They take ModuleProviders for historical reasons.
	LLVMModuleProviderRef LLVMCreateModuleProviderForExistingModule(LLVMModuleRef)
	void LLVMDisposeModuleProvider(LLVMModuleProviderRef)

	# = Memory buffers =

	LLVMBool LLVMCreateMemoryBufferWithContentsOfFile(char*, LLVMMemoryBufferRef*, char**)
	LLVMBool LLVMCreateMemoryBufferWithSTDIN(LLVMMemoryBufferRef*, char**)
	void LLVMDisposeMemoryBuffer(LLVMMemoryBufferRef)
	LLVMMemoryBufferRef LLVMCreateMemoryBufferWithMemoryRange(char*, size_t, char*, LLVMBool)
	LLVMMemoryBufferRef LLVMCreateMemoryBufferWithMemoryRangeCopy(char*, size_t, char*)
	char* LLVMGetBufferStart(LLVMMemoryBufferRef)
	size_t LLVMGetBufferSize(LLVMMemoryBufferRef)

	# = Pass Registry =
	# Return the global pass registry, for use with initialization functions.
	LLVMPassRegistryRef LLVMGetGlobalPassRegistry()

	# = Pass Managers =

	# Constructs a new whole-module pass pipeline. This type of pipeline is
	# suitable for link-time optimization and whole-module transformations.
	LLVMPassManagerRef LLVMCreatePassManager()

	# Constructs a new function-by-function pass pipeline over the module
	# provider. It does not take ownership of the module provider. This type of
	# pipeline is suitable for code generation and JIT compilation tasks.
	LLVMPassManagerRef LLVMCreateFunctionPassManagerForModule(LLVMModuleRef)

	#LLVMPassManagerRef LLVMCreateFunctionPassManager(LLVMModuleProviderRef MP) # Deprecated

	# Initializes, executes on the provided module, and finalizes all of the
	# passes scheduled in the pass manager. Returns 1 if any of the passes
	# modified the module, 0 otherwise.
	LLVMBool LLVMRunPassManager(LLVMPassManagerRef, LLVMModuleRef)

	# Initializes all of the function passes scheduled in the function pass
	# manager. Returns 1 if any of the passes modified the module, 0 otherwise.
	LLVMBool LLVMInitializeFunctionPassManager(LLVMPassManagerRef)

	# Executes all of the function passes scheduled in the function pass manager
	# on the provided function. Returns 1 if any of the passes modified the
	# function, false otherwise.
	LLVMBool LLVMRunFunctionPassManager(LLVMPassManagerRef, LLVMValueRef)

	# Finalizes all of the function passes scheduled in in the function pass
	# manager. Returns 1 if any of the passes modified the module, 0 otherwise.
	LLVMBool LLVMFinalizeFunctionPassManager(LLVMPassManagerRef)

	# Frees the memory of a pass pipeline. For function pipelines, does not free
	# the module provider.
	void LLVMDisposePassManager(LLVMPassManagerRef)

	Module* unwrap_Module "::llvm::unwrap" (LLVMModuleRef)
	LLVMModuleRef wrap_Module "::llvm::wrap" (Module*)

	Value* unwrap_Value "::llvm::unwrap" (LLVMValueRef)
	LLVMValueRef wrap_Value "::llvm::wrap" (Value*)

	Type* unwrap_Type "::llvm::unwrap" (LLVMTypeRef)
	LLVMTypeRef wrap_Type "::llvm::wrap" (Type*)

	LLVMContext* unwrap_Context "::llvm::unwrap" (LLVMContextRef)
	LLVMContextRef wrap_Context "::llvm::wrap" (LLVMContext*)

	LLVMBool LLVMIsMultithreaded()
