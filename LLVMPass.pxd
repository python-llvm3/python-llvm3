#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
# 
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from LLVMCore cimport LLVMBool, LLVMPassManagerRef, LLVMCreatePassManager, LLVMCreateFunctionPassManagerForModule, LLVMRunPassManager, \
	LLVMInitializeFunctionPassManager, LLVMRunFunctionPassManager, LLVMFinalizeFunctionPassManager, LLVMDisposePassManager, \
	LLVMGetGlobalPassRegistry

cdef extern from "llvm-c/PassManagerBuilder.h":
	
	cdef cppclass LLVMPassManagerBuilder:
		pass
	
	cdef struct LLVMOpaquePassManagerBuilder:
		pass
	ctypedef LLVMOpaquePassManagerBuilder* LLVMPassManagerBuilderRef
	
	LLVMPassManagerBuilderRef LLVMPassManagerBuilderCreate()
	
	void LLVMPassManagerBuilderDispose(LLVMPassManagerBuilderRef)
	
	void LLVMPassManagerBuilderSetOptLevel(LLVMPassManagerBuilderRef, unsigned)

	void LLVMPassManagerBuilderSetSizeLevel(LLVMPassManagerBuilderRef, unsigned)

	void LLVMPassManagerBuilderSetDisableUnitAtATime(LLVMPassManagerBuilderRef, Value)

	void LLVMPassManagerBuilderSetDisableUnrollLoops(LLVMPassManagerBuilderRef, LLVMBool)

	LLVMPassManagerBuilderSetDisableSimplifyLibCalls(LLVMPassManagerBuilderRef, LLVMBool)

	void LLVMPassManagerBuilderUseInlinerWithThreshold(LLVMPassManagerBuilderRef, unsigned)

	void LLVMPassManagerBuilderPopulateFunctionPassManager(LLVMPassManagerBuilderRef, LLVMPassManagerRef)

	void LLVMPassManagerBuilderPopulateModulePassManager(LLVMPassManagerBuilderRef, LLVMPassManagerRef)

	void LLVMPassManagerBuilderPopulateLTOPassManager(LLVMPassManagerBuilderRef, LLVMPassManagerRef, bint, bint)

	PassManagerBuilder* unwrap_PassManagerBuilder(LLVMPassManagerBuilderRef) "::llvm::unwrap"
	PassManagerBuilder* wrap_PassManagerBuilder(LLVMPassManagerBuilderRef) "::llvm::wrap"

