%int = type i24
%t1 = type { %int }

!namedmetadata = !{ !1 }
!1 = metadata !{ i32 0, null }
!2 = metadata !{ i32 0, null }

define %int @test (%t1 %x) {
entry:
	%0 = extractvalue %t1 %x, 0, !somemetadata !2
	ret %int %0
}
