#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from LLVMCore cimport LLVMTypeRef, LLVMPassManagerRef, LLVMValueRef

cdef extern from "llvm-c/Target.h":

	cdef struct LLVMOpaqueTargetData:
		pass
	ctypedef LLVMOpaqueTargetData* LLVMTargetDataRef

	cdef struct LLVMOpaqueTargetLibraryInfoData:
		pass
	ctypedef LLVMOpaqueTargetLibraryInfoData* LLVMTargetLibraryInfoRef

	cdef struct LLVMStructLayout:
		pass
	ctypedef LLVMStructLayout* LLVMStructLayoutRef

	enum LLVMByteOrdering:
		LLVMBigEndian,
		LLVMLittleEndian

	bint LLVMInitializeNativeTarget()
	void LLVMInitializeAllTargets()
	void LLVMInitializeAllTargetInfos()
	void LLVMInitializeAllTargetMCs()
	void LLVMInitializeAllAsmPrinters()

	LLVMTargetDataRef LLVMCreateTargetData(char*)

	void LLVMAddTargetData(LLVMTargetDataRef, LLVMPassManagerRef)

	void LLVMAddTargetLibraryInfo(LLVMTargetLibraryInfoRef, LLVMPassManagerRef)

	char *LLVMCopyStringRepOfTargetData(LLVMTargetDataRef) # string must be disposed with LLVMDisposeMessage.

	LLVMByteOrdering LLVMByteOrder(LLVMTargetDataRef)

	unsigned LLVMPointerSize(LLVMTargetDataRef)

	unsigned LLVMPointerSizeForAS(LLVMTargetDataRef, unsigned)

	LLVMTypeRef LLVMIntPtrType(LLVMTargetDataRef)

	unsigned long long LLVMSizeOfTypeInBits(LLVMTargetDataRef, LLVMTypeRef)

	unsigned long long LLVMStoreSizeOfType(LLVMTargetDataRef, LLVMTypeRef)

	unsigned long long LLVMABISizeOfType(LLVMTargetDataRef, LLVMTypeRef)

	unsigned LLVMABIAlignmentOfType(LLVMTargetDataRef, LLVMTypeRef)

	unsigned LLVMCallFrameAlignmentOfType(LLVMTargetDataRef, LLVMTypeRef)

	unsigned LLVMPreferredAlignmentOfType(LLVMTargetDataRef, LLVMTypeRef)

	unsigned LLVMPreferredAlignmentOfGlobal(LLVMTargetDataRef, LLVMValueRef)

	unsigned LLVMElementAtOffset(LLVMTargetDataRef, LLVMTypeRef, unsigned long long)

	unsigned long long LLVMOffsetOfElement(LLVMTargetDataRef, LLVMTypeRef, unsigned)

	void LLVMDisposeTargetData(LLVMTargetDataRef)
