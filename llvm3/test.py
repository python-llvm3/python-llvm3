import unittest
import gc, random, tempfile
from llvm3 import context, types, module, values, target

'''
	(c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
	This file is part of python-llvm3.

	python-llvm3 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	python-llvm3 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
'''

class TestContext(unittest.TestCase):

	def __init__(self, *args):
		self.gctx = context.globalContext
		self.ctx = context.LLVMContext()
		super().__init__(*args)

	def test_context(self):
		# make sure globalContext is a context object
		self.assertIs(type(self.gctx), context.LLVMContext)

		# make sure created Context is a context object
		self.assertIs(type(self.ctx), context.LLVMContext)

		# both Contextes must be different from each other
		self.assertNotEqual(self.ctx, self.gctx)

		# Two created Contextes must be different from each other
		self.assertNotEqual(self.ctx, context.LLVMContext())

	def test_mdkindid(self):
		# test that the assigned id to a yet unknown MD Kind is greater than 2
		self.assertGreater(self.ctx.getMDKindID('xundefined'), 2)
		# test well known MDKindIDs
		self.assertEqual(self.ctx.getMDKindID('prof'), 2)
		self.assertEqual(self.ctx.getMDKindID('tbaa'), 1)
		self.assertEqual(self.ctx.getMDKindID('dbg'), 0)

	# as contexts are opaque, no more checks are possible


class TestTypes(unittest.TestCase):

	def __init__(self, *args):
		self.ctx = context.LLVMContext()
		self.ctx2 = context.LLVMContext()
		self.bitwidth_sample = random.sample(range(1,128), 25)
		self.bitwidth_sample_big = random.sample(range(128,2**23-1), 50)
		super().__init__(*args)

	def test_caching(self):
		# type objects should be cached as long as a reference exists
		a = types.VoidType(self.ctx)
		b = types.VoidType(self.ctx)
		self.assertEqual(id(a), id(b))

		# but not longer as needed
		a = types.LabelType(self.ctx)
		a_id = id(a)
		del a
		gc.collect()
		b = types.LabelType(self.ctx)
		self.assertNotEqual(a_id, id(b))

	def _simple_type_tests(self, t, *args, **kwargs):
		kwargs['context'] = self.ctx
		kwargs2 = kwargs.copy()
		kwargs2['context'] = self.ctx2

		# type objects of the same context with the same paramaters should be identical
		a, b = t(*args, **kwargs), t(*args, **kwargs)
		self.assertEqual(a, b)

		# type objects of different contextes should not by identical
		self.assertNotEqual(t(*args, **kwargs), t(*args, **kwargs2))

	def _int_type_tests(self, t, bitwidth):
		self._simple_type_tests(t, bitwidth)

		# make sure computed bitwidth matches given bitwidth
		self.assertEqual(t(bitwidth, self.ctx).bitwidth, bitwidth)

		self.assertFalse(t(bitwidth, self.ctx).isEmptyType())
		self.assertFalse(t(bitwidth, self.ctx).isDerivedType())
		self.assertFalse(t(bitwidth, self.ctx).isAggregateType())
		self.assertTrue(t(bitwidth, self.ctx).isFirstClassType())
		self.assertTrue(t(bitwidth, self.ctx).isSingleValueType())


	def test_void(self):
		# make sure VoidType passes some basic checks
		self._simple_type_tests(types.VoidType)

	def test_label(self):
		# make sure LabelType passes some basic checks
		self._simple_type_tests(types.LabelType)

	def test_x86mmx(self):
		# make sure X86MMXType passes some basic checks
		self._simple_type_tests(types.X86MMXType)

	def test_integer(self):
		# make sure invalid integer types are not instantiable
		self.assertRaises(ValueError, types.IntegerType, 0, self.ctx)
		self.assertRaises(ValueError, types.IntegerType, -1, self.ctx)
		# make sure usual integer types pass some basic checks
		self._int_type_tests(types.IntegerType, 1)
		self._int_type_tests(types.IntegerType, 8)
		self._int_type_tests(types.IntegerType, 16)
		self._int_type_tests(types.IntegerType, 32)
		self._int_type_tests(types.IntegerType, 64)

		self.assertEqual(types.IntegerType(1, self.ctx), types.I1Type(self.ctx))
		self.assertEqual(types.IntegerType(8, self.ctx), types.I8Type(self.ctx))
		self.assertEqual(types.IntegerType(16, self.ctx), types.I16Type(self.ctx))
		self.assertEqual(types.IntegerType(32, self.ctx), types.I32Type(self.ctx))
		self.assertEqual(types.IntegerType(64, self.ctx), types.I64Type(self.ctx))

	def test_integer_arbitrary(self):
		# check 25 random small integer types
		for bitwidth in self.bitwidth_sample:
			self._int_type_tests(types.IntegerType, bitwidth)
		# check 50 random big integer types
		for bitwidth in self.bitwidth_sample_big:
			self._int_type_tests(types.IntegerType, bitwidth)
		self._int_type_tests(types.IntegerType, 2**23-1)

	def test_floatingpoint(self):
		self._simple_type_tests(types.HalfType)
		self._simple_type_tests(types.FloatType)
		self._simple_type_tests(types.DoubleType)
		self._simple_type_tests(types.X86_FP80Type)
		self._simple_type_tests(types.FP128Type)
		self._simple_type_tests(types.PPC_FP128Type)

	def test_metadata(self):
		# Metadata types are uninstantiable
		self.assertRaises(TypeError, types.MetadataType, self.ctx)

	def test_basetypes(self):
		# All base types must be uninstantiable
		self.assertRaises(TypeError, types.Type, self.ctx)
		self.assertRaises(TypeError, types.FloatingPointType, self.ctx)
		self.assertRaises(TypeError, types.CompositeType, self.ctx)
		self.assertRaises(TypeError, types.SequentialType, self.ctx)

	def test_arraytype(self):
		# test non-type element type
		self.assertRaises(TypeError, types.ArrayType, None, 1)
		# test illegal element types
		for t in (types.VoidType, types.LabelType, types.X86MMXType):
			self.assertRaises(ValueError, types.ArrayType, t(self.ctx), 1)

		# Test for context consistency checks
		i1 = types.IntegerType(1, self.ctx)
		self.assertRaises(ValueError, types.ArrayType, i1, 0, self.ctx2)

		# test integer arrays
		for bitwidth in self.bitwidth_sample:
			i = types.IntegerType(bitwidth, self.ctx)
			ty = types.ArrayType(i, 0)
			self.assertIsInstance(ty,types.ArrayType)
			ty = types.ArrayType(i, random.randrange(1, 2**32-1))
			self.assertIsInstance(ty,types.ArrayType)
			ty = types.ArrayType(i, 2**32-1)
			self.assertIsInstance(ty,types.ArrayType)

		# test floating point arrays
		for fp_t in (types.FloatType, types.DoubleType, types.X86_FP80Type, types.FP128Type, types.PPC_FP128Type):
			fp = fp_t(self.ctx)
			ty = types.ArrayType(fp, 0)
			self.assertIsInstance(ty,types.ArrayType)
			ty = types.ArrayType(fp, random.randrange(1,2**32-1))
			self.assertIsInstance(ty,types.ArrayType)
			ty = types.ArrayType(fp, 2**32-1)
			self.assertIsInstance(ty,types.ArrayType)

	def test_vectortype(self):
		# test non-type element type
		self.assertRaises(TypeError, types.VectorType, None, 1)
		# test illegal element types
		for t in (types.VoidType, types.LabelType, types.X86MMXType):
			self.assertRaises(ValueError, types.VectorType, t(self.ctx), 1)
		ptr = types.PointerType(types.PointerType(types.DoubleType(self.ctx)))
		self.assertRaises(ValueError, types.VectorType, ptr, 2)

		# Test for context consistency checks
		i1 = types.IntegerType(1, self.ctx)
		self.assertRaises(ValueError, types.VectorType, i1, 2, self.ctx2)

		# test integer vectors
		for bitwidth in self.bitwidth_sample:
			i = types.IntegerType(bitwidth, self.ctx)
			self.assertRaises(ValueError, types.VectorType, i, 0)

			ty = types.VectorType(i, random.randrange(1, 2**32-1))
			self.assertIsInstance(ty,types.VectorType)
			ty = types.VectorType(i, 2**32-1)
			self.assertIsInstance(ty,types.VectorType)
			self.assertEqual(ty.elementType, i)
			self.assertEqual(ty.elementCount, 2**32-1)

		# test pointer of integer vectors
		for bitwidth in self.bitwidth_sample:
			ptr = types.PointerType(types.IntegerType(bitwidth, self.ctx))
			self.assertRaises(ValueError, types.VectorType, ptr, 0)

			ty = types.VectorType(ptr, random.randrange(1, 2**32-1))
			self.assertIsInstance(ty,types.VectorType)
			self.assertEqual(ty.elementType, ptr)

		# test floating point vectors
		for fp_t in (types.FloatType, types.DoubleType, types.X86_FP80Type, types.FP128Type, types.PPC_FP128Type):
			fp = fp_t(self.ctx)
			self.assertRaises(ValueError, types.VectorType, fp, 0)

			ty = types.VectorType(fp, random.randrange(1,2**32-1))
			self.assertIsInstance(ty,types.VectorType)
			ty = types.VectorType(fp, 2**32-1)
			self.assertIsInstance(ty,types.VectorType)
			self.assertEqual(ty.elementType, fp)
			self.assertEqual(ty.elementCount, 2**32-1)

		# test pointer of floating point vectors
		for fp_t in (types.FloatType, types.DoubleType, types.X86_FP80Type, types.FP128Type, types.PPC_FP128Type):
			ptr = types.PointerType(fp_t(self.ctx))
			self.assertRaises(ValueError, types.VectorType, ptr, 0)

			ty = types.VectorType(ptr, random.randrange(1, 2**32-1))
			self.assertIsInstance(ty,types.VectorType)
			self.assertEqual(ty.elementType, ptr)

	def test_pointertype(self):
		# test non-type element type
		self.assertRaises(TypeError, types.PointerType, None)
		# test illegal element types
		for t in (types.VoidType, types.LabelType, types.X86MMXType):
			self.assertRaises(ValueError, types.PointerType, t(self.ctx), 0)

		# test address spaces
		ptr = types.PointerType(types.DoubleType(self.ctx), 2**24-1)
		self.assertEqual(ptr.elementType, types.DoubleType(self.ctx))
		self.assertEqual(ptr.addressSpace, 2**24-1)

		# Test that len(PointerType) fails
		self.assertRaises(NotImplementedError, len, ptr)


	def test_struct(self):
		# test non-type element types
		self.assertRaises(TypeError, types.StructType, None)
		self.assertRaises(TypeError, types.StructType, [None])
		self.assertRaises(TypeError, types.StructType, [None], True)
		# test illegal element types
		self.assertRaises(ValueError, types.StructType, [types.IntegerType(32, self.ctx), types.X86MMXType(self.ctx)], True)
		self.assertRaises(ValueError, types.StructType, [types.IntegerType(32, self.ctx), types.VoidType(self.ctx)])
		# test undeductable context
		self.assertRaises(ValueError, types.StructType, [])

		# test empty struct
		ty = types.StructType([], False, self.ctx)
		self.assertEqual(ty.elementCount, 0)
		self.assertIs(type(ty), types.StructType)

		# test some structs
		contents = [types.IntegerType(65535, self.ctx), types.PointerType(ty), types.ArrayType(types.DoubleType(self.ctx), 2**32-1)]
		ty = types.StructType(contents, False, self.ctx)
		self.assertEqual(ty.elementCount, len(contents))
		self.assertIs(type(ty), types.StructType)
		for el1, el2 in zip(ty.elementTypes, contents):
			self.assertEqual(el1, el2)
		self.assertFalse(ty.packed)

		ty2 = types.StructType(contents, True, self.ctx)
		self.assertEqual(ty2.elementCount, len(contents))
		self.assertIs(type(ty2), types.StructType)
		for el1, el2 in zip(ty2.elementTypes, contents):
			self.assertEqual(el1, el2)
		self.assertTrue(ty2.packed)

		contents = [types.IntegerType(65535, self.ctx), types.PointerType(types.ArrayType(types.DoubleType(self.ctx), 0)), types.FP128Type(self.ctx)]
		ty3 = types.StructType(contents, True, self.ctx)
		self.assertEqual(ty3.elementCount, len(contents))
		self.assertIs(type(ty3), types.StructType)
		self.assertFalse(ty.isomorphic(ty3))
		self.assertFalse(ty2.isomorphic(ty3))

	def test_identified_struct(self):
		contents = [types.IntegerType(65535, self.ctx), types.PointerType(types.ArrayType(types.DoubleType(self.ctx), 0)), types.FP128Type(self.ctx)]
		ty = types.IdentifiedStructType(None, context=self.ctx)
		self.assertIsNone(ty.name)
		self.assertTrue(ty.opaque)
		self.assertIsNone(ty.elementTypes)

		ty.setBody(contents, True)
		self.assertFalse(ty.opaque)
		self.assertEqual(ty.elementTypes, contents)

		ty2 = types.IdentifiedStructType("test", contents)
		self.assertFalse(ty2.opaque)
		self.assertEqual(ty2.elementTypes, contents)
		self.assertEqual(ty2.name, "test")

		# Check for isomorphism
		self.assertNotEqual(ty, ty2)
		self.assertTrue(ty.isomorphic(ty2))

	def test_functiontype(self):
		ty = types.FunctionType(types.VoidType(self.ctx), [])
		self.assertFalse(ty.varArg)
		self.assertEqual(ty.parameterCount, 0)
		self.assertEqual(ty.returnType, types.VoidType(self.ctx))
		self.assertEqual(ty.parameterTypes, [])

		ty = types.FunctionType(types.DoubleType(self.ctx), [types.X86MMXType(self.ctx)], True)
		self.assertTrue(ty.varArg)
		self.assertEqual(ty.parameterCount, 1)
		self.assertEqual(ty.returnType, types.DoubleType(self.ctx))
		self.assertEqual(ty.parameterTypes, [types.X86MMXType(self.ctx)])


class TestModules(unittest.TestCase):

	def __init__(self, *args):
		self.ctx = context.globalContext
		super().__init__(*args)

	def test_module(self):
		mod = module.Module('mod1', self.ctx)
		# Make sure module is a Module instance
		self.assertIsInstance(mod,module.Module)
		# Make sure the identifier is saved correctly
		self.assertEqual(mod.identifier, 'mod1')
		# Make sure the context is saved correctly
		self.assertIs(mod.context, self.ctx)
		# Directly instantiated modules aren't lazy
		self.assertFalse(mod.lazy)
		# As module is freshly instantiated it shouldn't contain anything
		self.assertCountEqual(mod.globals, [])
		self.assertCountEqual(mod.functions, [])

	def test_bitcodereader(self):
		# bitcode for a rather simple module (see test.ll for source)
		bitcode = b'BC\xc0\xde!\x0c\x00\x00W\x00\x00\x00\x01\x10\x00\x00\x12\x00\x00\x00\x07\x81#\x91A\xc8'\
			b'\x04I\x06\x1029\x92\x01\x84\x0c%\x05\x08\x19\x1e\x04\x8bb\x80\x0cE\x02B\x92\x0bBd\x102\x148\x08'\
			b'\x18I\n2D$H\n\x90!#\xc4R\x80\x0c\x19!r$\x07\xc8\xc8\x10b\xa8\xa0\xa8@\xc6\xf0\x01\x00\x00\x00\x89'\
			b' \x00\x00\r\x00\x00\x002"\xc8\x08 e\x82\x84\x00&CH\t\t&C\xc6\tC!)$\x98\x0c\x19\x17\x08\xc9\x98 8'\
			b'\xe6\x08\xb0\x13\xa6\x1a\x05(\x02B\x1a\x08\x98#\x00\x83\x11\x00\x00\x00\x00C\x94\x01\x00\x00\x00'\
			b'\x00\x00\x00\xc8\x02\x01\x05\x00\x00\x002\x1e\x98\x08\x19\x11L\x90\x8c\t&G\xc6\x04C\xea\x08\x00\x00'\
			b'\x00y\x18\x00\x00\x08\x00\x00\x00C\x88"0\x80\x11\x8d;\x84C;\x94\x039\xb4C9\xd0C8\x90C8\xd0C8L\t\x00'\
			b'\x00\x00\x00a \x00\x00\x0c\x00\x00\x00\x13\x04\xc1h\x82\x00\xe4\x10\x0e\x04\x00\x00\x03\x00\x00\x00'\
			b'\x07P\x10\xcd\x14a&\x10\\\x00\x00\x00\x011\x00\x00\x02\x00\x00\x00[\x06`\x00\x00\x00\x00\x00\x00\x00'\
			b'\x00\x00y\x18\x00\x00\x0c\x00\x00\x003\x08\x80\x1c\xc4\xe1\x1cf\x14\x01=\x88C8\x84\xc3\x8cB\x80\x07'\
			b'yx\x07s\x98\xd1\x0c\xf3\xf0\x0e\xedP\x0e\xedP\x0e\xf4\x10\x0e\xe4\x10\x0e\xf4\x10\x0e\x00\x00\x00q '\
			b'\x00\x00\x02\x00\x00\x00\x06@L\x844\x01\x00\x00\x00\x00\x00\x00'

		# Try to instantiate from this bitcode
		mod = module.Module.fromBitcode(bitcode)
		# Make sure module is a Module instance
		self.assertIsInstance(mod,module.Module)
		# Not instantiated as lazy
		self.assertFalse(mod.lazy)
		# Check that named structures are correctly read back
		self.assertTrue(isinstance(mod.getTypeByName('t1'), types.IdentifiedStructType))
		self.assertIs(mod.getTypeByName('t1').elementTypes[0], types.IntegerType(24, context.globalContext))
		# Check that functions are correctly read back
		self.assertTrue(mod.functions['test'])
		# Check named metadata
		self.assertTrue(len(mod.namedMetadata['namedmetadata']) == 1)

	def test_bitcodewriter(self):
		# bitcode for a rather simple module (see test.ll for source)
		bitcode = b'BC\xc0\xde!\x0c\x00\x00W\x00\x00\x00\x01\x10\x00\x00\x12\x00\x00\x00\x07\x81#\x91A\xc8'\
			b'\x04I\x06\x1029\x92\x01\x84\x0c%\x05\x08\x19\x1e\x04\x8bb\x80\x0cE\x02B\x92\x0bBd\x102'\
			b'\x148\x08\x18I\n2D$H\n\x90!#\xc4R\x80\x0c\x19!r$\x07\xc8\xc8\x10b\xa8\xa0\xa8@\xc6\xf0'\
			b'\x01\x00\x00\x00\x89 \x00\x00\r\x00\x00\x002"\xc8\x08 e\x82\x84\x00&CH\t\t&C\xc6\tC!)$'\
			b'\x98\x0c\x19\x17\x08\xc9\x98 8\xe6\x08\xb0\x13\xa6\x1a\x05(\x02B\x1a\x08\x98#\x00\x83\x11'\
			b'\x00\x00\x00\x00C\x94\x01\x00\x00\x00\x00\x00\x00\xc8\x02\x01\x05\x00\x00\x002\x1e\x98\x08'\
			b'\x19\x11L\x90\x8c\t&G\xc6\x04C\xea\x08\x00\x00\x00y\x18\x00\x00\x08\x00\x00\x00C\x88"0\x80'\
			b'\x11\x8d;\x84C;\x94\x039\xb4C9\xd0C8\x90C8\xd0C8L\t\x00\x00\x00\x00a \x00\x00\x0c\x00\x00\x00'\
			b'\x13\x04\xc1h\x82\x00\xe4\x10\x0e\x04\x00\x00\x03\x00\x00\x00\x07P\x10\xcd\x14a&\x10\\\x00\x00'\
			b'\x00\x011\x00\x00\x02\x00\x00\x00[\x06`\x00\x00\x00\x00\x00\x00\x00\x00\x00y\x18\x00\x00\x0c'\
			b'\x00\x00\x003\x08\x80\x1c\xc4\xe1\x1cf\x14\x01=\x88C8\x84\xc3\x8cB\x80\x07yx\x07s\x98\xd1\x0c'\
			b'\xf3\xf0\x0e\xedP\x0e\xedP\x0e\xf4\x10\x0e\xe4\x10\x0e\xf4\x10\x0e\x00\x00\x00q \x00\x00\x02'\
			b'\x00\x00\x00\x06@L\x844\x01\x00\x00\x00\x00\x00\x00'

		# Instantiate from this bitcode
		mod = module.Module.fromBitcode(bitcode)

		# Write bitcode
		with tempfile.TemporaryFile() as f:
			mod.writeBitcodeToFile(f)

			# Check that bitcode was written
			f.seek(0)
			self.assertEqual(f.read(4), b'BC\xc0\xde')

	def test_namedmetadata(self):
		mod = module.Module('mod1', self.ctx)

		node = values.MDNode([42, None], self.ctx)

		self.assertFalse('namedmetadata' in mod.namedMetadata)
		mod.namedMetadata.append('namedmetadata', node)
		self.assertTrue('namedmetadata' in mod.namedMetadata)
		self.assertEqual(mod.namedMetadata['namedmetadata'], [node])
		del mod.namedMetadata['namedmetadata']
		self.assertFalse('namedmetadata' in mod.namedMetadata)

	def test_functions(self):
		mod = module.Module('test', self.ctx)
		# Function table must be empty on a newly created module
		self.assertEqual(len(mod.functions), 0)
		# Create a function and check if count changed
		mod.getOrInsertFunction('test', types.FunctionType(types.VoidType(self.ctx), []))
		self.assertEqual(len(mod.functions), 1)
		# Check types
		self.assertIs(type(mod.functions['test']), values.Function)
		self.assertIs(type(mod.functions['test'].type), types.PointerType)

class TestValues(unittest.TestCase):
	def __init__(self, *args):
		super().__init__(*args)
		self.ctx = context.globalContext
		self.m = module.Module('test.values', self.ctx)

	def test_integer_constant(self):
		v = values.ConstantInt(0, 64, self.ctx)
		self.assertIsInstance(v,values.ConstantInt)
		self.assertTrue(isinstance(v.type, types.IntegerType))
		self.assertEqual(v.type.bitwidth, 64)
		self.assertEqual(v.sextValue, 0)
		self.assertEqual(v.zextValue, 0)
		self.assertEqual(str(v), 'i64 0')

		v = values.ConstantInt(4095, 12, self.ctx)
		self.assertEqual(v.type.bitwidth, 12)
		self.assertEqual(v.sextValue, -1)
		self.assertEqual(v.zextValue, 4095)

		v = values.ConstantInt(4096, 12, self.ctx)
		self.assertEqual(v.type.bitwidth, 12)
		self.assertEqual(v.sextValue, 0)
		self.assertEqual(v.zextValue, 0)

		v = values.ConstantInt(1, 1, self.ctx)
		self.assertEqual(v.sextValue, -1)
		self.assertEqual(v.zextValue, 1)
		self.assertEqual(str(v), 'i1 true')

	def test_float_constant(self):
		t = types.DoubleType(self.ctx)
		v = values.ConstantFP(0, t)
		self.assertIsInstance(v, values.ConstantFP)
		self.assertTrue(v.type == t)
		self.assertTrue(v.isZero())
		self.assertEqual(v.value, 0.0)
		self.assertTrue(str(v).startswith('double 0.0'))

		v = values.ConstantFP(10e14, t)
		self.assertEqual(v.value, 10e14)

		v = values.ConstantFP(-10e14, t)
		self.assertEqual(v.value, -10e14)

	def test_constexpr_gep(self):
		m = self.m
		constarray = values.ConstantArray([values.ConstantInt(23, 42, self.ctx)])
		# Test if non-pointer raises ValueError
		with self.assertRaises(ValueError):
			values.GetElementPtrConstantExpr(constarray, [values.ConstantInt(1,32, self.ctx)])
		# Test if access to pointer to constant array works
		glob = m.globals.add('arrayptr', constarray.type)
		glob.initializer = constarray
		cgep = values.GetElementPtrConstantExpr(glob, [values.ConstantInt(1,32, self.ctx), values.ConstantInt(33,32, self.ctx)])
		self.assertIsInstance(cgep, values.GetElementPtrConstantExpr)
		self.assertEqual(cgep.operands[0], glob)
		# Test invalid (too deep) indexing
		with self.assertRaises(TypeError):
			cgep = values.GetElementPtrConstantExpr(glob, [values.ConstantInt(0,32, self.ctx), values.ConstantInt(0,32, self.ctx), values.ConstantInt(0,32, self.ctx)])
		# Test invalid (out of struct) indexing
		structtype = types.StructType([types.I8Type(self.ctx)])
		glob = m.globals.add('structptr', structtype)
		with self.assertRaises(ValueError):
			values.GetElementPtrConstantExpr(glob, [values.ConstantInt(23,32, self.ctx), values.ConstantInt(33,32, self.ctx)])



class TestTargets(unittest.TestCase):

	def __init__(self, *args):
		super().__init__(*args)

	def test_static_variables(self):
		# Triples must be strings
		self.assertIs(type(target.DEFAULT_TARGET_TRIPLE), str)
		self.assertIs(type(target.HOSTTRIPLE), str)
		# Native arch name must be a string
		self.assertIs(type(target.NATIVE_ARCH), str)
		# Triples must contain at least two dashes
		self.assertGreaterEqual(target.DEFAULT_TARGET_TRIPLE.count('-'), 2)
		self.assertGreaterEqual(target.HOSTTRIPLE.count('-'), 2)

	def test_targetproperties(self):
		t = target.getDefaultTarget()
		# Check types of properties
		self.assertIs(type(t.description), str)
		self.assertIs(type(t.hasAsmBackend), bool)
		self.assertIs(type(t.hasJIT), bool)
		self.assertIs(type(t.hasTargetMachine), bool)
		if t.hasTargetMachine:
			self.assertEqual(type(t.getTargetData(target.DEFAULT_TARGET_TRIPLE)), target.TargetData)

	def test_targetdata(self):
		t = target.getDefaultTarget()
		if not t.hasTargetMachine:
			self.skipTest("No TargetMachine for default target")
			return
		# Get default target data
		td = t.getTargetData(target.DEFAULT_TARGET_TRIPLE)
		# Check string representation
		self.assertIs(type(td.dataLayoutString), str)
		# Check endianess
		self.assertEqual(td.byteOrder, 'little-endian' if td.isLittleEndian() else 'big-endian')
		self.assertNotEqual(td.isLittleEndian(), td.isBigEndian())
		# Check nativeIntegerSizes for reasonable results
		self.assertIs(type(td.nativeIntegerSizes), tuple)
		self.assertNotEqual(td.nativeIntegerSizes, [])
		# Check getTypeSizeInBits for reasonable results
		ty1 = types.IntegerType(8, context.globalContext)
		ty2 = types.ArrayType(ty1, 10)
		ty3 = types.StructType([ty1,ty2,ty1])
		ty4 = types.PointerType(ty1)
		self.assertGreaterEqual(td.getTypeSizeInBits(ty1), 8)
		self.assertGreaterEqual(td.getTypeSizeInBits(ty2), td.getTypeSizeInBits(ty1)*10)
		self.assertGreaterEqual(td.getTypeSizeInBits(ty3), td.getTypeSizeInBits(ty2)+td.getTypeSizeInBits(ty1)*2)
		# Check getTypeStoreSize for reasonable results
		self.assertGreaterEqual(td.getTypeStoreSize(ty3)*8, td.getTypeSizeInBits(ty3))
		# Check getTypeABISize for reasonable results
		self.assertGreaterEqual(td.getTypeABISize(ty3)*8, td.getTypeSizeInBits(ty3))
		self.assertEqual(td.getTypeABISize(ty4), td.pointerSize)
		# Offset check
		self.assertGreaterEqual(td.getOffsetOfElement(ty3, 2), 11)
		self.assertEqual(td.getElementContainingOffset(ty3, 10), 1)
		# Check getPointerSizeIntType
		self.assertEqual(td.getTypeSizeInBits(td.getPointerSizeIntType(context.globalContext)), td.getTypeSizeInBits(ty4))
		# TODO: getTypeABIAlignment, getPreferredAlignment



if __name__ == '__main__':
    unittest.main()
