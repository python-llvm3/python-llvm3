#!/usr/bin/cython
# coding: utf-8
#
# distutils: language = c++
# distutils: libraries = LLVMCore LLVMSupport tinfo
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

cimport LLVMCore as _llvm
from llvm3.values cimport Value, BasicBlock
from llvm3.types cimport Type
from libc.stdlib cimport calloc, free
from cython cimport sizeof

from llvm3.values import Function, Instruction, Constant, ConstantInt, \
	IPREDICATES, FPREDICATES, UndefValue, ConstantVector, ConstantPointerNull, \
	MDNode, MDString, ATOMICORDERINGS, RMWBINOPS
from llvm3.types import VectorType, IntegerType, PointerType, FunctionType, \
	StructType, I32Type, FloatType, DoubleType, I8Type, VoidType
from llvm3 import exceptions as _exceptions

cdef object _get_llvmvalue(_llvm.LLVMValueRef ref, IRBuilder builder):
	if ref == NULL:
		raise ValueError()
	cdef Value v = Value.__new__(Value, builder.context)
	v._ref = ref
	return v._get_correct_instance()

cdef class IRBuilder:
	'''
	| IRBuilder(block) -> IRBuilder instance
	| IRBuilder(None, context) -> IRBuilder instance

	This provides a uniform API for creating instructions and inserting them into a basic block: either at the end of a BasicBlock, or at a specific position in a block.

	.. note::
		Note that the builder does not expose the full generality of LLVM instructions as the LLVM C-API is limited.
	'''

	def __cinit__(self, BasicBlock block, context=None):
		if block is None:
			if context:
				self.context = <LLVMContext>context
			else:
				raise ValueError('No context given')
			self.function = None
			self.basicBlock = None
		else:
			self.function = block.parent
			self.context = block.context
			self.basicBlock = block

		self._ref = _llvm.LLVMCreateBuilderInContext(self.context._ref)
		if self._ref == NULL:
			raise _exceptions.LLVMException("Error on IR builder creation")

		if self.basicBlock is not None:
			_llvm.LLVMPositionBuilderAtEnd(self._ref, block._bref)

	def setCurrentDebugLocation(self, unsigned line, unsigned column, Value scope, Value original_scope=None):
		'''
			x.setCurrentDebugLocation(line, column, scope[, original_scope])

			Set location information used by debugging information.
		'''
		if scope is not None and not isinstance(scope, MDNode):
			raise TypeError('scope must be a MDNode or None')
		if original_scope is not None and not isinstance(original_scope, MDNode):
			raise TypeError('original_scope must be a MDNode or None')

		node = MDNode([ConstantInt(line, 32, self.context), ConstantInt(column, 32, self.context), scope, original_scope], self.context)
		return self.setCurrentDebugLocationByMDNode(node)

	def setCurrentDebugLocationByMDNode(Value self, Value node):
		'''
			x.setCurrentDebugLocationByMDNode(node)

			Set location information used by debugging information.
			See the LLVM documentation for details.
		'''
		if node is None or not isinstance(node, MDNode):
			raise TypeError("not a valid MDNode instance")
		_llvm.LLVMSetCurrentDebugLocation(self._ref, node._ref)

	def setInstDebugLocation(Value self, Value instruction):
		'''
			x.setInstDebugLocation(instruction)

			If this builder has a current debug location, set it on the specified instruction.
		'''
		if not isinstance(instruction, Instruction):
			raise TypeError('cannot set debug location on non-instruction')
		_llvm.LLVMSetInstDebugLocation(self._ref, instruction._ref)

	def getDebugLocation(self):
		cdef LLVMValueRef ref = _llvm.LLVMGetCurrentDebugLocation(self._ref)
		if ref == NULL:
			return None
		return _get_llvmvalue(ref, self.context)

	#void LLVMSetInstDebugLocation(LLVMBuilderRef, LLVMValueRef)

	def __dealloc__(self):
		_llvm.LLVMDisposeBuilder(self._ref)

	def __init__(self, block, context=None):
		pass

	def positionAfter(self, Value instruction):
		'''
			x.positionAfter(instruction)

			Move the IRBuilder after the given instruction.
		'''
		if not self.basicBlock:
			if instruction.context != self.context:
				raise ValueError('Target instruction must be in current context')
			self.basicBlock = instruction.parent
			self.function = self.basicBlock.parent

		if not isinstance(instruction, Instruction):
			raise TypeError("Not an instruction")
		if instruction.parent != self.basicBlock:
			raise ValueError("Instruction must be in current basic block")
		_llvm.LLVMPositionBuilder(self._ref, self.basicBlock._bref, instruction._ref)

	def positionBefore(self, Value instruction):
		'''
			x.positionBefore(instruction)

			Move the IRBuilder before the given instruction.
		'''
		if not self.basicBlock:
			if instruction.context != self.context:
				raise ValueError('Target instruction must be in current context')
			self.basicBlock = instruction.parent
			self.function = self.basicBlock.parent

		if not isinstance(instruction, Instruction):
			raise TypeError("Not an instruction")
		if instruction.parent != self.basicBlock:
			raise ValueError("Instruction must be in current basic block")
		_llvm.LLVMPositionBuilderBefore(self._ref, instruction._ref)

	def positionAtEnd(self, BasicBlock block=None):
		'''
			x.positionAtEnd([block])

			Move the IRBuilder to the end of the basic block.
		'''
		if block is None:
			block = self.basicBlock
		if block is not None:
			_llvm.LLVMPositionBuilderAtEnd(self._ref, block._bref)
			self.basicBlock = block
			self.function = block.parent

	def getCurrentFunctionReturnType(self):
		'''Get the return type of the current function that we're emitting into. '''
		return self.function.type.returnType

	def getInt1(self, bint v):
		'''Get a constant value representing either true or false.'''
		return ConstantInt(v, 1, self.context)

	def getTrue(self):
		'''Get the constant value for i1 true.'''
		return ConstantInt(1, 1, self.context)

	def getFalse(self):
		'''Get the constant value for i1 false.'''
		return ConstantInt(0, 1, self.context)

	def getInt8(self, v):
		'''Get a constant 8-bit value.'''
		return ConstantInt(v, 8, self.context)

	def getInt16(self, v):
		'''Get a constant 16-bit value.'''
		return ConstantInt(v, 16, self.context)

	def getInt32(self, v):
		'''Get a constant 32-bit value.'''
		return ConstantInt(v, 32, self.context)

	def getInt64(self, v):
		'''Get a constant 64-bit value.'''
		return ConstantInt(v, 64, self.context)

	def getInt(self, value, unsigned bitwidth):
		'''
			x.getInt(value, bitwidth)

			Get a constant integer value.
		'''
		return ConstantInt(value, bitwidth, self.context)

	def getIntTy(self, int bitwidth):
		'''Fetch the type representing a integer of given bitwidth.'''
		return IntegerType(bitwidth, self.context)
	def getInt1Ty(self):
		'''Fetch the type representing a single bit.'''
		return IntegerType(1, self.context)
	def getInt8Ty(self):
		'''Fetch the type representing an 8-bit integer.'''
		return IntegerType(8, self.context)
	def getInt16Ty(self):
		'''Fetch the type representing an 16-bit integer.'''
		return IntegerType(16, self.context)
	def getInt32Ty(self):
		'''Fetch the type representing an 32-bit integer.'''
		return I32Type(self.context)
	def getInt64Ty(self):
		'''Fetch the type representing an 64-bit integer.'''
		return IntegerType(64, self.context)

	def getFloatTy(self):
		'''Fetch the type representing an 32-bit floating point value.'''
		return FloatType(self.context)
	def getDoubleTy(self):
		'''Fetch the type representing an 64-bit floating point value.'''
		return DoubleType(self.context)

	def getVoidTy(self):
		'''Fetch the type representing void.'''
		return VoidType(self.context)

	def getInt8PtrTy(self):
		'''Fetch the type representing a pointer to an 8-bit integer value.'''
		return PointerType(I8Type(self.context))

	def insert(self, Value instruction, str name = None):
		'''
			x.insert(instruction[, name]) -> Instruction

			Insert an instruction at the current position.
		'''
		cdef bytes namedata
		if isinstance(instruction, Constant):
			return instruction
		elif isinstance(instruction, Instruction):
			if name:
				namedata = name.encode("utf-8")
				_llvm.LLVMInsertIntoBuilderWithName(self._ref, instruction._ref, namedata)
			else:
				_llvm.LLVMInsertIntoBuilder(self._ref, instruction._ref)
			return instruction
		else:
			raise TypeError

	def Return(self, Value value=None):
		''' x.Return([value]) -> ReturnInst '''
		if value is None:
			return _get_llvmvalue(_llvm.LLVMBuildRetVoid(self._ref), self)
		elif isinstance(value, (list, tuple)):
			return _get_llvmvalue(self._aggregate_return(value), self)
		else:
			return _get_llvmvalue(_llvm.LLVMBuildRet(self._ref, value._ref), self)

	cdef _llvm.LLVMValueRef _aggregate_return(self, values) except NULL:
		if values is None:
			raise ValueError

		cdef _llvm.LLVMValueRef* elarray = NULL
		cdef unsigned length = len(values)
		cdef _llvm.LLVMValueRef ref
		cdef Value obj

		elarray = <LLVMValueRef*>calloc(length, sizeof(_llvm.LLVMValueRef))
		try:
			for i, item in enumerate(values):
				if not isinstance(item, Value):
					raise TypeError('{0!r} is no valid LLVM value'.format(item))
				elarray[i] = (<Value>item)._ref
			ref = _llvm.LLVMBuildAggregateRet(self._ref, elarray, length)
		finally:
			free(elarray)
		if ref == NULL:
			raise _exceptions.ValueCreationError('Could not create aggregate return instruction from {0!r}'.format(values))
		return ref

	def Br(self, BasicBlock destination):
		''' x.Br(destination) -> BrInst '''
		if destination is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildBr(self._ref, destination._bref), self)

	def CondBr(self, Value condition, BasicBlock destination_true, BasicBlock destination_false, int branchWeight=0):
		''' x.Br(condition, destination_true, destination_false[, branchWeight]) -> CondBrInst '''
		if condition is None or destination_true is None or destination_false is None:
			raise ValueError
		result =  _get_llvmvalue(_llvm.LLVMBuildCondBr(self._ref, condition._ref, destination_true._bref, destination_false._bref), self)
		if branchWeight:
			if branchWeight > 0:
				weightTrue = branchWeight
				weightFalse = 0
			else:
				weightTrue = 0
				weightFalse = -branchWeight
			metadata = MDNode([
				MDString('branch_weights', self.context),
				ConstantInt(weightTrue, 32, self.context),
				ConstantInt(weightFalse, 32, self.context)
			])
			result.setMetadata('prof', metadata)
		return result

	def Switch(self, Value value, BasicBlock defaultdest, destinations):
		''' x.Switch(value, defaultdest, destinations) -> SwitchInst '''
		if defaultdest is None:
			raise ValueError("a default destination must be specified.")
		if value is None:
			raise ValueError("a switch value must be specified.")

		if isinstance(dict, destinations):
			destinations = list(destinations.items())
		for val, dest in destinations:
			if not isinstance(dest, BasicBlock):
				raise ValueError("Destinations must be basic blocks")
			if not isinstance(val, Constant):
				raise ValueError("Switch comparison values must be constants")

		cdef Value switch = _get_llvmvalue(_llvm.LLVMBuildSwitch(self._ref, value._ref, defaultdest._bref, len(destinations)), self)

		for val, dest in destinations:
			_llvm.LLVMAddCase(switch._ref, (<Value>val)._ref, (<BasicBlock>dest)._bref)

		return switch

	def IndirectBr(self, Value address, destinations):
		''' x.IndirectBr(address, destinations) -> IndirectBrInst '''
		if address is None:
			raise ValueError("an address must be specified.")

		for dest in destinations:
			if not isinstance(dest, BasicBlock):
				raise ValueError("Destinations must be basic blocks")

		cdef Value indirectbr = _get_llvmvalue(_llvm.LLVMBuildIndirectBr(self._ref, address._ref, len(destinations)), self)

		for dest in destinations:
			_llvm.LLVMAddDestination(indirectbr._ref, (<BasicBlock>dest)._bref)

		return indirectbr


	def Invoke(self, Value function, arguments, BasicBlock then_label, BasicBlock exception_label, str name=None):
		''' x.InvokeInst(function, arguments, then_label, exception_label[, name]) -> InvokeInst '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		cdef _llvm.LLVMValueRef* elarray = NULL
		cdef unsigned length = len(arguments)
		cdef _llvm.LLVMValueRef ref

		if function is None:
			raise ValueError
		if not isinstance(function.type, PointerType) or not isinstance(function.type.elementType, FunctionType):
			raise ValueError("Function must be a pointer to function type")

		elarray = <LLVMValueRef*>calloc(length, sizeof(_llvm.LLVMValueRef))
		try:
			for i, item in enumerate(arguments):
				if not isinstance(item, Value):
					raise TypeError('{0!r} is no valid LLVM value'.format(item))
				elarray[i] = (<Value>item)._ref
			ref = _llvm.LLVMBuildInvoke(self._ref, function._ref, elarray, length, then_label._bref, exception_label._bref, namedata)
		finally:
			free(elarray)
		if ref == NULL:
			raise _exceptions.ValueCreationError('Could not create Invoke instruction from {0!r} and {1!r}'.format(function, arguments))
		return _get_llvmvalue(ref, self)

	def LandingPad(self, Type resulttype, Value personality, clauses, bint cleanup=False, str name=None):
		''' x.LandingPad(resulttype, personality, clauses[, cleanup[, name]]) -> LandingPadInst '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if not clauses and not cleanup:
			raise ValueError("LandingPad must either have clauses or be a cleanup")

		if clauses is None:
			clauses = []
		for clause in clauses:
			if not isinstance(clause, Value):
				raise ValueError("clauses must be Values")

		cdef Value val = _get_llvmvalue(_llvm.LLVMBuildLandingPad(self._ref, resulttype._ref, personality._ref, len(clauses), namedata), self)
		if cleanup:
			_llvm.LLVMSetCleanup(val._ref, True)
		for clause in clauses:
			_llvm.LLVMAddClause(val._ref, (<Value>clause)._ref)

		return val

	def Resume(self, Value exception):
		''' x.Resume(exception) -> ResumeInst '''
		if exception is None:
			raise ValueError("Exception cannot be None")
		return _get_llvmvalue(_llvm.LLVMBuildResume(self._ref, exception._ref), self)

	def Unreachable(self):
		''' x.Unreachable() -> UnreachableInst '''
		return _get_llvmvalue(_llvm.LLVMBuildUnreachable(self._ref), self)

	# Arithmetic
	def Add(self, Value op1, Value op2, bint nuw=False, bint nsw=False, str name = None):
		''' x.Add(op1, op2[, nuw[, nsw[, name]]]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		if nuw and nsw:
			raise ValueError('\'nsw\' and \'nuw\' flag currently cannot be set at the same time')
		if nsw:
			return _get_llvmvalue(_llvm.LLVMBuildNSWAdd(self._ref, op1._ref, op2._ref, namedata), self)
		elif nuw:
			return _get_llvmvalue(_llvm.LLVMBuildNUWAdd(self._ref, op1._ref, op2._ref, namedata), self)
		else:
			return _get_llvmvalue(_llvm.LLVMBuildAdd(self._ref, op1._ref, op2._ref, namedata), self)

	def FAdd(self, Value op1, Value op2, str name = None):
		''' x.FAdd(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFAdd(self._ref, op1._ref, op2._ref, namedata), self)

	def Sub(self, Value op1, Value op2, bint nuw=False, bint nsw=False, str name = None):
		''' x.Sub(op1, op2[, nuw[, nsw[, name]]]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		if nuw and nsw:
			raise ValueError('\'nsw\' and \'nuw\' flag currently cannot be set at the same time')
		if nsw:
			return _get_llvmvalue(_llvm.LLVMBuildNSWSub(self._ref, op1._ref, op2._ref, namedata), self)
		elif nuw:
			return _get_llvmvalue(_llvm.LLVMBuildNUWSub(self._ref, op1._ref, op2._ref, namedata), self)
		else:
			return _get_llvmvalue(_llvm.LLVMBuildSub(self._ref, op1._ref, op2._ref, namedata), self)

	def FSub(self, Value op1, Value op2, str name = None):
		''' x.FSub(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFSub(self._ref, op1._ref, op2._ref, namedata), self)

	def Mul(self, Value op1, Value op2, bint nuw=False, bint nsw=False, str name = None):
		''' x.Mul(op1, op2[, nuw[, nsw[, name]]]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		if nuw and nsw:
			raise ValueError('\'nsw\' and \'nuw\' flag currently cannot be set at the same time')
		if nsw:
			return _get_llvmvalue(_llvm.LLVMBuildNSWMul(self._ref, op1._ref, op2._ref, namedata), self)
		elif nuw:
			return _get_llvmvalue(_llvm.LLVMBuildNUWMul(self._ref, op1._ref, op2._ref, namedata), self)
		else:
			return _get_llvmvalue(_llvm.LLVMBuildMul(self._ref, op1._ref, op2._ref, namedata), self)

	def FMul(self, Value op1, Value op2, str name = None):
		''' x.FMul(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFMul(self._ref, op1._ref, op2._ref, namedata), self)

	def UDiv(self, Value op1, Value op2, str name = None):
		''' x.UDiv(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildUDiv(self._ref, op1._ref, op2._ref, namedata), self)

	def SDiv(self, Value op1, Value op2, bint exact = False, str name = None):
		''' x.SDiv(op1, op2[, exact[, name]]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		if exact:
			return _get_llvmvalue(_llvm.LLVMBuildExactSDiv(self._ref, op1._ref, op2._ref, namedata), self)
		else:
			return _get_llvmvalue(_llvm.LLVMBuildSDiv(self._ref, op1._ref, op2._ref, namedata), self)

	def FDiv(self, Value op1, Value op2, str name = None):
		''' x.FDiv(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFDiv(self._ref, op1._ref, op2._ref, namedata), self)

	def URem(self, Value op1, Value op2, str name = None):
		''' x.URem(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildURem(self._ref, op1._ref, op2._ref, namedata), self)

	def SRem(self, Value op1, Value op2, str name = None):
		''' x.SRem(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildSRem(self._ref, op1._ref, op2._ref, namedata), self)

	def FRem(self, Value op1, Value op2, str name = None):
		''' x.FRem(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFRem(self._ref, op1._ref, op2._ref, namedata), self)

	def Shl(self, Value op1, Value op2, str name = None):
		''' x.Shl(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildShl(self._ref, op1._ref, op2._ref, namedata), self)

	def LShr(self, Value op1, Value op2, str name = None):
		''' x.LShr(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildLShr(self._ref, op1._ref, op2._ref, namedata), self)

	def AShr(self, Value op1, Value op2, str name = None):
		''' x.AShr(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildAShr(self._ref, op1._ref, op2._ref, namedata), self)

	def And(self, Value op1, Value op2, str name = None):
		''' x.And(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildAnd(self._ref, op1._ref, op2._ref, namedata), self)

	def Or(self, Value op1, Value op2, str name = None):
		''' x.Or(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildOr(self._ref, op1._ref, op2._ref, namedata), self)

	def Xor(self, Value op1, Value op2, str name = None):
		''' x.Xor(op1, op2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op1 is None or op2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildXor(self._ref, op1._ref, op2._ref, namedata), self)

	'''
	def BinOp(self):
		return _get_llvmvalue(_llvm.LLVMBuildBinOp(self._ref, LLVMOpcode, LLVMValueRef, LLVMValueRef, char*))
	'''

	def Neg(self, Value op, bint nuw=False, bint nsw=False, str name = None):
		''' x.Neg(op[, nuw[, nsw[, name]]]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None:
			raise ValueError
		if nuw and nsw:
			raise ValueError('\'nsw\' and \'nuw\' flag currently cannot be set at the same time')
		if nsw:
			return _get_llvmvalue(_llvm.LLVMBuildNSWNeg(self._ref, op._ref, namedata), self)
		elif nuw:
			return _get_llvmvalue(_llvm.LLVMBuildNUWNeg(self._ref, op._ref, namedata), self)
		else:
			return _get_llvmvalue(_llvm.LLVMBuildNeg(self._ref, op._ref, namedata), self)

	def FNeg(self, Value op, str name = None):
		''' x.FNeg(op[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFNeg(self._ref, op._ref, namedata), self)

	def Not(self, Value op, str name = None):
		''' x.Not(op[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildNot(self._ref, op._ref, namedata), self)

	# Memory
	def Malloc(self, Type type, count=1, str name = None):
		''' x.Malloc(type[, count[, name]]) -> CallInst '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if type is None:
			raise ValueError
		if isinstance(count, int):
			if count == 1:
				return _get_llvmvalue(_llvm.LLVMBuildMalloc(self._ref, type._ref, namedata), self)
			count = ConstantInt(count, 32 if count < 2**31 else 64, type.context)
		elif not isinstance(count, Value):
			raise ValueError("count must be an integer or a Value")

		return _get_llvmvalue(_llvm.LLVMBuildArrayMalloc(self._ref, type._ref, (<Value>count)._ref, namedata), self)


	def Alloca(self, Type type, count=1, str name = None):
		''' x.Alloca(type[, count[, name]]) -> AllocaInst '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if type is None:
			raise ValueError
		if isinstance(count, int):
			if count == 1:
				return _get_llvmvalue(_llvm.LLVMBuildAlloca(self._ref, type._ref, namedata), self)
			count = ConstantInt(count, 32 if count < 2**31 else 64, type.context)
		elif not isinstance(count, Value):
			raise ValueError("count must be an integer or a Value")

		return _get_llvmvalue(_llvm.LLVMBuildArrayAlloca(self._ref, type._ref, (<Value>count)._ref, namedata), self)

	def Free(self, Value op):
		''' x.Free(op) -> CallInst '''
		if op is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFree(self._ref, op._ref), self)

	def Load(self, Value op, str name = None):
		''' x.Load(ptr[, name]) -> LoadInst '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None:
			raise ValueError
		if not isinstance(op.type, PointerType):
			raise TypeError('first argument to Load must be of pointer type, not {1}'.format(op.type))
		return _get_llvmvalue(_llvm.LLVMBuildLoad(self._ref, op._ref, namedata), self)

	def Store(self, Value value, Value ptr):
		''' x.Store(value, ptr) -> StoreInst '''
		if value is None or ptr is None:
			raise ValueError
		if not isinstance(ptr.type, PointerType):
			raise TypeError('second argument to Store must be of pointer type, not {0}'.format(ptr.type))
		if ptr.type.elementType != value.type:
			raise TypeError('incompatible types for Store: {0} and {1}'.format(value.type, ptr.type))
		return _get_llvmvalue(_llvm.LLVMBuildStore(self._ref, value._ref, ptr._ref), self)

	def GEP(self, Value op, indexes, bint inbounds=False, str name = None):
		''' x.GEP(op, indexes[, inbounds[, name]]) -> Instruction '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		cdef _llvm.LLVMValueRef* elarray = NULL
		cdef unsigned length = len(indexes)
		cdef _llvm.LLVMValueRef ref

		if op is None:
			raise ValueError

		elarray = <LLVMValueRef*>calloc(length, sizeof(_llvm.LLVMValueRef))
		try:
			for i, item in enumerate(indexes):
				if isinstance(item, int):
					item = ConstantInt(item, 32, op.context)
				if not isinstance(item, Value):
					raise TypeError('{0!r} is no valid LLVM value'.format(item))
				elarray[i] = (<Value>item)._ref
			if inbounds:
				ref = _llvm.LLVMBuildInBoundsGEP(self._ref, op._ref, elarray, length, namedata)
			else:
				ref = _llvm.LLVMBuildGEP(self._ref, op._ref, elarray, length, namedata)
		finally:
			free(elarray)
		if ref == NULL:
			raise _exceptions.ValueCreationError('Could not create getElementPtr instruction from {0!r} and {1!r}'.format(op, indexes))
		return _get_llvmvalue(ref, self)

	'''
	def StructGEP(self):
		return _get_llvmvalue(_llvm.LLVMBuildStructGEP(self._ref, LLVMValueRef, unsigned, char*))
	'''

	def GlobalString(self, bytes content, str name):
		''' x.GlobalString(content, name) -> Constant '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if content is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildGlobalString(self._ref, content, namedata), self)

	def GlobalStringPtr(self, bytes content, str name):
		''' x.GlobalStringPtr(content, name) -> Constant '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if content is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildGlobalStringPtr(self._ref, content, namedata), self)

	# Casts
	def Trunc(self, Value op, Type type, str name = None):
		''' x.Trunc(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildTrunc(self._ref, op._ref, type._ref, namedata), self)

	def ZExt(self, Value op, Type type, str name = None):
		''' x.ZExt(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildZExt(self._ref, op._ref, type._ref, namedata), self)

	def SExt(self, Value op, Type type, str name = None):
		''' x.SExt(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildSExt(self._ref, op._ref, type._ref, namedata), self)

	def FPToUI(self, Value op, Type type, str name = None):
		''' x.FPToUI(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFPToUI(self._ref, op._ref, type._ref, namedata), self)

	def FPToSI(self, Value op, Type type, str name = None):
		''' x.FPToSI(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFPToSI(self._ref, op._ref, type._ref, namedata), self)

	def UIToFP(self, Value op, Type type, str name = None):
		''' x.UIToFP(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildUIToFP(self._ref, op._ref, type._ref, namedata), self)

	def SIToFP(self, Value op, Type type, str name = None):
		''' x.SIToFP(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		cdef _llvm.LLVMValueRef tmp = _llvm.LLVMBuildSIToFP(self._ref, op._ref, type._ref, namedata)
		return _get_llvmvalue(tmp, self)

	def FPTrunc(self, Value op, Type type, str name = None):
		''' x.FPTrunc(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFPTrunc(self._ref, op._ref, type._ref, namedata), self)

	def FPExt(self, Value op, Type type, str name = None):
		''' x.FPExt(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFPExt(self._ref, op._ref, type._ref, namedata), self)

	def PtrToInt(self, Value op, Type type, str name = None):
		''' x.PtrToInt(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildPtrToInt(self._ref, op._ref, type._ref, namedata), self)

	def IntToPtr(self, Value op, Type type, str name = None):
		''' x.IntToPtr(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildIntToPtr(self._ref, op._ref, type._ref, namedata), self)

	def BitCast(self, Value op, Type type, str name = None):
		''' x.BitCast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildBitCast(self._ref, op._ref, type._ref, namedata), self)

	def AddrSpaceCast(self, Value op, Type type, str name = None):
		''' x.AddrSpaceCast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildAddrSpaceCast(self._ref, op._ref, type._ref, namedata), self)


	def ZExtOrBitCast(self, Value op, Type type, str name = None):
		''' x.ZExtOrBitCast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildBitCast(self._ref, op._ref, type._ref, namedata), self)

	def SExtOrBitCast(self, Value op, Type type, str name = None):
		''' x.SExtOrBitCast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildBitCast(self._ref, op._ref, type._ref, namedata), self)

	def TruncOrBitCast(self, Value op, Type type, str name = None):
		''' x.TruncOrBitCast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildBitCast(self._ref, op._ref, type._ref, namedata), self)

	"""
	def Cast(self, Value op, Type type, str name = None):
		''' x.Cast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildBitCast(self._ref, op._ref, type._ref, namedata), self)
	"""

	def PointerCast(self, Value op, Type type, str name = None):
		''' x.PointerCast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildPointerCast(self._ref, op._ref, type._ref, namedata), self)

	def IntCast(self, Value op, Type type, str name = None):
		'''
			x.IntCast(op, type[, name]) -> Value

			Note: Signed Cast!
		'''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildIntCast(self._ref, op._ref, type._ref, namedata), self)

	def FPCast(self, Value op, Type type, str name = None):
		''' x.FPCast(op, type[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if op is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildFPCast(self._ref, op._ref, type._ref, namedata), self)

	# Comparisons
	def ICmp(self, predicate, Value lhs, Value rhs, str name = None):
		'''
			x.ICmp(predicate, lhs, rhs[, name]) -> Value

			Predicate can be one of 'eq', 'ne', 'ugt', 'uge', 'ult', 'ule',
			'sgt', 'sge', 'slt', 'sle'.
		'''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if lhs is None or rhs is None:
			raise ValueError
		if isinstance(predicate, str):
			pdict = dict((b,a) for (a,b) in IPREDICATES.items())
			if predicate.lower() not in pdict:
				raise ValueError('invalid predicate {0!r}'.format(predicate))
			predicate = pdict[predicate.lower()]
		elif not isinstance(predicate, int):
			raise TypeError('invalid type for predicate')

		return _get_llvmvalue(_llvm.LLVMBuildICmp(self._ref, <_llvm.LLVMIntPredicate>predicate, lhs._ref, rhs._ref, namedata), self)

	def FCmp(self, predicate, Value lhs, Value rhs, str name = None):
		'''
			x.FCmp(predicate, lhs, rhs[, name]) -> Value

			Predicate can be one of 'false', 'oeq', 'ogt', 'oge', 'olt', 'ole',
			'one', 'ord', 'uno', 'ueq', 'ugt', 'uge', 'ult', 'ule', 'une', 'true'.
		'''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if lhs is None or rhs is None:
			raise ValueError
		if isinstance(predicate, str):
			pdict = dict((b,a) for (a,b) in FPREDICATES.items())
			if predicate.lower() not in pdict:
				raise ValueError('invalid predicate {0!r}'.format(predicate))
			predicate = pdict[predicate.lower()]
		elif not isinstance(predicate, int):
			raise TypeError('invalid type for predicate')

		return _get_llvmvalue(_llvm.LLVMBuildFCmp(self._ref, <_llvm.LLVMRealPredicate>predicate, lhs._ref, rhs._ref, namedata), self)

	# Miscellaneous instructions
	def Phi(self, Type type, str name = None):
		''' x.Phi(type[, name]) -> PHINode '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildPhi(self._ref, type._ref, namedata), self)

	def Call(self, Value function, arguments, str name = None):
		''' x.Call(function, arguments[, name]) -> CallInst '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		cdef _llvm.LLVMValueRef* elarray = NULL
		cdef unsigned length = len(arguments)
		cdef _llvm.LLVMValueRef ref

		if function is None:
			raise ValueError
		if not isinstance(function.type, FunctionType) and (not isinstance(function.type, PointerType) or not isinstance(function.type.elementType, FunctionType)):
			raise ValueError("Function must be of function type or be a pointer to function type")

		elarray = <LLVMValueRef*>calloc(length, sizeof(_llvm.LLVMValueRef))
		try:
			for i, item in enumerate(arguments):
				if not isinstance(item, Value):
					raise TypeError('{0!r} is no valid LLVM value'.format(item))
				elarray[i] = (<Value>item)._ref
			ref = _llvm.LLVMBuildCall(self._ref, function._ref, elarray, length, namedata)
		finally:
			free(elarray)
		if ref == NULL:
			raise _exceptions.ValueCreationError('Could not create Call instruction from {0!r} and {1!r}'.format(function, arguments))
		return _get_llvmvalue(ref, self)

	def Select(self, Value condition, Value val1, Value val2, str name = None):
		''' x.Select(condition, val1, val2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if condition is None or val1 is None or val2 is None:
			raise ValueError
		if (not isinstance(condition.type, IntegerType) or condition.type.bitwidth != 1) and (not isinstance(condition.type, VectorType) or not isinstance(condition.type.elementType, IntegerType) or condition.type.elementType.bitwidth != 1):
			raise ValueError('Condition must be of i1 type or i1 vector type')

		return _get_llvmvalue(_llvm.LLVMBuildSelect(self._ref, condition._ref, val1._ref, val2._ref, namedata), self)

	def VAArg(self, Value va_list_ptr, Type type, str name = None):
		''' x.VAArg(va_list_ptr, type[, name]) -> Instruction '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if va_list_ptr is None or type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildVAArg(self._ref, va_list_ptr._ref, type._ref, namedata), self)

	def ExtractElement(self, Value vector, index, str name = None):
		''' x.ExtractElement(vector, index[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if vector is None:
			raise ValueError
		if isinstance(index, int):
			index = ConstantInt(index, 32, vector.context)
		if not isinstance(index, Value):
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildExtractElement(self._ref, vector._ref, (<Value>index)._ref, namedata), self)

	def InsertElement(self, Value vector, Value element, index, str name = None):
		''' x.InsertElement(vector, element, index[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if vector is None:
			raise ValueError
		if isinstance(index, int):
			index = ConstantInt(index, 32, vector.context)
		if not isinstance(index, Value):
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildInsertElement(self._ref, vector._ref, element._ref, (<Value>index)._ref, namedata), self)

	def ShuffleVector(self, Value vector1, Value vector2, mask, str name = None):
		''' x.ShuffleVector(vector1, vector2, mask[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if vector1 is None:
			raise ValueError
		if vector2 is None:
			vector2 = <Value>UndefValue(vector1.type)
		if isinstance(mask, (tuple, list)) and all(isinstance(x, int) for x in mask):
			mask = ConstantVector([ ConstantInt(x, 32, self.context) for x in mask ])
		if not isinstance(mask, (ConstantVector, UndefValue)):
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildInsertElement(self._ref, vector1._ref, vector2._ref, (<Value>mask)._ref, namedata), self)

	def ExtractValue(self, Value aggregate, unsigned index, str name = None):
		''' x.ExtractValue(aggregate, index[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if aggregate is None:
			raise ValueError
		if index > aggregate.type.elementCount:
			raise ValueError("Index out of bounds")
		return _get_llvmvalue(_llvm.LLVMBuildExtractValue(self._ref, aggregate._ref, index, namedata), self)

	def InsertValue(self, Value aggregate, Value element, unsigned index, str name = None):
		''' x.ExtractValue(aggregate, value, index[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if aggregate is None or element is None:
			raise ValueError
		if index > aggregate.type.elementCount:
			raise ValueError("Index out of bounds")
		return _get_llvmvalue(_llvm.LLVMBuildInsertValue(self._ref, aggregate._ref, element._ref, index, namedata), self)

	def Fence(self, str ordering, bint singlethread=False, str name = None):
		''' x.Fence(ordering[, singlethread[, name]]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		cdef _llvm.LLVMAtomicOrdering orderingcode = ATOMICORDERINGS[ordering]
		return _get_llvmvalue(_llvm.LLVMBuildFence(self._ref, orderingcode, singlethread, namedata), self)

	def AtomicRMW(self, str op, Value ptr, Value value, str ordering, bint singlethread=False):
		''' x.AtomicRMW(op, ptr, value, ordering[, singlethread]) -> Value '''
		cdef _llvm.LLVMAtomicRMWBinOp opcode = RMWBINOPS[op]
		cdef _llvm.LLVMAtomicOrdering orderingcode = ATOMICORDERINGS[ordering]
		if ptr is None or value is None:
			raise ValueError
		if not isinstance(ptr.type, PointerType):
			raise TypeError
		if ptr.type.elementType != value.type:
			raise TypeError
		return _get_llvmvalue(_llvm.LLVMBuildAtomicRMW(self._ref, opcode, ptr._ref, value._ref, orderingcode, singlethread), self)


	def IsNull(self, Value value, str name=None):
		''' x.IsNull(value[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if value is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildIsNull(self._ref, value._ref, namedata), self)

	def IsNotNull(self, Value value, str name=None):
		''' x.IsNotNull(value[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if value is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildIsNotNull(self._ref, value._ref, namedata), self)

	def PtrDiff(self, Value ptr1, Value ptr2, str name=None):
		''' x.PtrDiff(ptr1, prt2[, name]) -> Value '''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if ptr1 is None or ptr2 is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMBuildPtrDiff(self._ref, ptr1._ref, ptr2._ref, namedata), self)

	def RaiseException(self, Value exception, BasicBlock unwind_label, BasicBlock error_label, str name=None):
		''' x.RaiseException(exception, unwind_label, error_label[, name]) -> Instruction '''
		if exception is None or unwind_label is None or error_label is None:
			raise TypeError
		if not isinstance(exception.type, PointerType) or not isinstance(exception.type.elementType, StructType):
			raise ValueError("Must be a pointer to a struct.")

		if not self.function:
			raise _exceptions.LLVMException("Cannot create a RaiseException instruction sequence without associated function.")

		ftype = FunctionType(I32Type(self.context), [exception.type])

		f = self.function.parent.getOrInsertFunction('_Unwind_RaiseException', ftype, cconv="ccc")

		self.Invoke(f, [exception], error_label, unwind_label, name)

	def LifetimeStart(self, Value ptr, size=-1):
		'''
			x.LifetimeStart(ptr[, size])

			Create a lifetime.start intrinsic.

			Size can be an int or a ConstantInt instance.
			If the pointer isn't i8* it will be converted.
		'''
		if not isinstance(ptr.type, PointerType):
			raise TypeError('first argument to IRBuilter.LifetimeStart() must be of PointerType')
		if isinstance(size, Value):
			if not isinstance(size.type, IntegerType):
				raise TypeError('second argument to IRBuilter.LifetimeStart() must be of IntegerType')
			if size.bitwidth < 64:
				size = self.ZExtOrBitCast(size)
			else:
				size = <Value>size
		else:
			size = self.getInt64(size)

		if ptr.type.elementType != self.getInt8Ty():
			ptr = self.BitCast(ptr, self.getInt8Ty())

		func = self.function.parent.getOrInsertFunction('llvm.lifetime.start', FunctionType(self.getVoidTy(), [self.getInt64Ty(), self.getInt8PtrTy()]))
		return self.Call(func, [size, ptr])

	def LifetimeEnd(self, Value ptr, size=-1):
		'''
			x.LifetimeEnd(ptr[, size])

			Create a lifetime.end intrinsic.

			Size can be an int or a ConstantInt instance.
			If the pointer isn't i8* it will be converted.
		'''
		if not isinstance(ptr.type, PointerType):
			raise TypeError('first argument to IRBuilter.LifetimeEnd() must be of PointerType')
		if isinstance(size, Value):
			if not isinstance(size.type, IntegerType):
				raise TypeError('second argument to IRBuilter.LifetimeEnd() must be of IntegerType')
			if size.bitwidth < 64:
				size = self.ZExtOrBitCast(size)
			else:
				size = <Value>size
		else:
			size = self.getInt64(size)

		if ptr.type.elementType != self.getInt8Ty():
			ptr = self.BitCast(ptr, self.getInt8Ty())

		func = self.function.parent.getOrInsertFunction('llvm.lifetime.end', FunctionType(self.getVoidTy(), [self.getInt64Ty(), self.getInt8PtrTy()]))
		return self.Call(func, [size, ptr])

	def DoNothing(self):
		'''
			x.DoNothing()

			Create a donothing intrinsic.
		'''
		func = self.function.parent.getOrInsertFunction('llvm.donothing', FunctionType(self.getVoidTy(), []))
		return self.Call(func, [])



