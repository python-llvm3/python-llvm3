#!/usr/bin/cython
# coding: utf-8
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from LLVMCore cimport LLVMContextRef

cdef LLVMContext _getContext(LLVMContextRef ref)

cdef class LLVMContext:

	cdef LLVMContextRef _ref
	cdef int _owning

	# Use LLVMContext to cache Python objects representing various things
	cdef object _module_cache
	cdef object _type_cache

	cdef object __weakref__
