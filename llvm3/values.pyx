#!/usr/bin/cython
# coding: utf-8
#
# distutils: language = c++
# distutils: libraries = LLVMCore LLVMSupport tinfo
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from cython cimport final
cimport LLVMCore as _llvm
from llvm3.types cimport Type
from libc.stdlib cimport calloc, free
from cython cimport sizeof
from cython.operator cimport dereference as deref
from libc.stdint cimport uintptr_t, uint64_t
cimport cython

from llvm3.context import globalContext as _globalContext
from llvm3.types import IntegerType, FloatingPointType, PointerType, ArrayType, StructType, VectorType, VoidType, FloatType, DoubleType, I8Type, I16Type, I32Type, I64Type, CompositeType, FunctionType
import llvm3.exceptions as _exceptions
from weakref import ref as _weakref
from warnings import warn as _warn
import os as _os
from collections import Iterable, Iterator, Sized, Container

class _named_int(int):

	def __new__(cls, int code, str name):
		obj = super(_named_int, cls).__new__(cls, code)
		obj.name = str(name)
		return obj

	def __eq__(self, other):
		return super(_named_int, self).__eq__(other) or self.name.__eq__(other)

	def __str__(self):
		return self.name

	def __repr__(self):
		return '<{0!r}|{1!r}>'.format(int(self), self.name)

CALLINGCONVENTIONS = {
	_llvm.LLVMCCallConv:'ccc',
	_llvm.LLVMFastCallConv:'fastcc',
	_llvm.LLVMColdCallConv:'coldcc',
	_llvm.LLVMWebKitJSCallConv: 'webkit_jscc',
	_llvm.LLVMAnyRegCallConv:'anyregcc',
	_llvm.LLVMX86StdcallCallConv:'x86_stdcall',
	_llvm.LLVMX86FastcallCallConv: 'x86_fastcall',
	70: 'x86_thiscall',
	66: 'arm_apcs',
	67: 'arm_aapcs',
	68: 'arm_aapcs_vfp',
}

LINKAGES = {
	_llvm.LLVMExternalLinkage: 'external',
	_llvm.LLVMAvailableExternallyLinkage: 'available_externally',
	_llvm.LLVMLinkOnceAnyLinkage: 'linkonce',
	_llvm.LLVMLinkOnceODRLinkage: 'linkonce_odr',
	_llvm.LLVMWeakAnyLinkage: 'weak',
	_llvm.LLVMWeakODRLinkage: 'weak_odr',
	_llvm.LLVMAppendingLinkage: 'appending',
	_llvm.LLVMInternalLinkage: 'internal',
	_llvm.LLVMPrivateLinkage: 'private',
	_llvm.LLVMExternalWeakLinkage: 'extern_weak',
	_llvm.LLVMCommonLinkage: 'common',
	_llvm.LLVMLinkerPrivateLinkage: 'linker_private',
	_llvm.LLVMLinkerPrivateWeakLinkage: 'linker_private_weak',
}

DLLSTORAGECLASSES = {
	_llvm.LLVMDefaultStorageClass: 'default',
	_llvm.LLVMDLLImportStorageClass: 'dllimport',
	_llvm.LLVMDLLExportStorageClass: 'dllexport'
}

VISIBILITIES = {
	_llvm.LLVMDefaultVisibility: 'default',
	_llvm.LLVMHiddenVisibility: 'hidden',
	_llvm.LLVMProtectedVisibility: 'protected',
}

IPREDICATES = {
	_llvm.LLVMIntEQ: 'eq',
	_llvm.LLVMIntNE: 'ne',
	_llvm.LLVMIntUGT: 'ugt',
	_llvm.LLVMIntUGE: 'uge',
	_llvm.LLVMIntULT: 'ult',
	_llvm.LLVMIntULE: 'ule',
	_llvm.LLVMIntSGT: 'sgt',
	_llvm.LLVMIntSGE: 'sge',
	_llvm.LLVMIntSLT: 'slt',
	_llvm.LLVMIntSLE: 'sle',
}

FPREDICATES = {
	_llvm.LLVMRealPredicateFalse: 'false',
	_llvm.LLVMRealPredicateTrue: 'true',

	_llvm.LLVMRealOEQ: 'oeq',
	_llvm.LLVMRealONE: 'one',
	_llvm.LLVMRealOGT: 'ogt',
	_llvm.LLVMRealOGE: 'oge',
	_llvm.LLVMRealOLT: 'olt',
	_llvm.LLVMRealOLE: 'ole',
	_llvm.LLVMRealORD: 'ord',

	_llvm.LLVMRealUEQ: 'ueq',
	_llvm.LLVMRealUNE: 'une',
	_llvm.LLVMRealUGT: 'ugt',
	_llvm.LLVMRealUGE: 'uge',
	_llvm.LLVMRealULT: 'ult',
	_llvm.LLVMRealULE: 'ule',
	_llvm.LLVMRealUNO: 'uno',
}

BINOPS = {
	_llvm.LLVMAdd: 'add',
	_llvm.LLVMFAdd: 'fadd',
	_llvm.LLVMSub: 'sub',
	_llvm.LLVMFSub: 'fsub',
	_llvm.LLVMMul: 'mul',
	_llvm.LLVMFMul: 'fmul',
	_llvm.LLVMUDiv: 'udiv',
	_llvm.LLVMSDiv: 'sdiv',
	_llvm.LLVMFDiv: 'fdiv',
	_llvm.LLVMURem: 'urem',
	_llvm.LLVMSRem: 'srem',
	_llvm.LLVMFRem: 'frem'
}

ATOMICORDERINGS = {
	_llvm.LLVMAtomicOrderingNotAtomic: 'not_atomic',
	_llvm.LLVMAtomicOrderingUnordered: 'unordered',
	_llvm.LLVMAtomicOrderingMonotonic: 'monotonic',
	_llvm.LLVMAtomicOrderingAcquire: 'acquire',
	_llvm.LLVMAtomicOrderingRelease: 'release',
	_llvm.LLVMAtomicOrderingAcquireRelease: 'acq_rel',
	_llvm.LLVMAtomicOrderingSequentiallyConsistent: 'seq_cst'
}

RMWBINOPS = {
	_llvm.LLVMAtomicRMWBinOpXchg: 'xchg',
	_llvm.LLVMAtomicRMWBinOpAdd: 'add',
	_llvm.LLVMAtomicRMWBinOpSub: 'sub',
	_llvm.LLVMAtomicRMWBinOpAnd: 'and',
	_llvm.LLVMAtomicRMWBinOpNand: 'nand',
	_llvm.LLVMAtomicRMWBinOpOr: 'or',
	_llvm.LLVMAtomicRMWBinOpXor: 'xor',
	_llvm.LLVMAtomicRMWBinOpMax: 'max',
	_llvm.LLVMAtomicRMWBinOpMin: 'min',
	_llvm.LLVMAtomicRMWBinOpUMax: 'umax',
	_llvm.LLVMAtomicRMWBinOpUMin: 'umin',
}

cdef type _get_valueclass(_llvm.LLVMValueRef ref):
	cdef _llvm.LLVMOpcode op
	cdef _llvm.LLVMTypeRef t = NULL
	if _llvm.LLVMIsAArgument(ref): return Argument
	if _llvm.LLVMIsABasicBlock(ref): return BasicBlock
	if _llvm.LLVMIsAInlineAsm(ref): return InlineAsm
	if _llvm.LLVMIsAMDNode(ref): return MDNode
	if _llvm.LLVMIsAMDString(ref): return MDString
	if _llvm.LLVMIsAUser(ref):
		if _llvm.LLVMIsAConstant(ref):
			if _llvm.LLVMIsABlockAddress(ref): return BlockAddress
			if _llvm.LLVMIsAConstantAggregateZero(ref): return ConstantAggregateZero
			if _llvm.LLVMIsAConstantDataSequential(ref):
				if _llvm.LLVMIsAConstantDataArray(ref):
					t = _llvm.LLVMGetElementType(_llvm.LLVMTypeOf(ref))
					if _llvm.LLVMGetTypeKind(t) == _llvm.LLVMIntegerTypeKind:
						if _llvm.LLVMGetIntTypeWidth(t) == 8:
							return ConstantString
					return ConstantDataArray
				if _llvm.LLVMIsAConstantDataVector(ref):
					return ConstantDataVector
			if _llvm.LLVMIsAConstantArray(ref):
				return ConstantArray
			if _llvm.LLVMIsAConstantExpr(ref):
				op = _llvm.LLVMGetConstOpcode(ref)
				if _llvm.LLVMAdd <= op <= _llvm.LLVMFRem: # Binary operations
					return BinaryConstantExpr
				if _llvm.LLVMICmp <= op <= _llvm.LLVMFCmp: # Comparisons
					return CompareConstantExpr
				if _llvm.LLVMGetElementPtr == op: # GetElementPtr
					return GetElementPtrConstantExpr
				if _llvm.LLVMTrunc <= op <= _llvm.LLVMBitCast: # Casts
					if _llvm.LLVMTrunc == op:
						return TruncConstantExpr
					if _llvm.LLVMZExt == op:
						return ZExtConstantExpr
					if _llvm.LLVMSExt == op:
						return SExtConstantExpr
					if _llvm.LLVMFPTrunc == op:
						return FPTruncConstantExpr
					if _llvm.LLVMFPExt == op:
						return FPExtConstantExpr
					if _llvm.LLVMFPToUI == op:
						return FPToUIConstantExpr
					if _llvm.LLVMFPToSI == op:
						return FPToSIConstantExpr
					if _llvm.LLVMUIToFP == op:
						return UIToFPConstantExpr
					if _llvm.LLVMSIToFP == op:
						return SIToFPConstantExpr
					if _llvm.LLVMIntToPtr == op:
						return IntToPtrConstantExpr
					if _llvm.LLVMPtrToInt == op:
						return PtrToIntConstantExpr
					if _llvm.LLVMBitCast == op:
						return BitCastConstantExpr
					if _llvm.LLVMAddrSpaceCast == op:
						return AddrSpaceCastConstantExpr
					return UnaryConstantExpr
				if _llvm.LLVMSelect == op: # Select
					return SelectConstantExpr
				if _llvm.LLVMExtractElement == op: # ExtractElement
					return ExtractElementConstantExpr
				if _llvm.LLVMInsertElement == op: # InsertElement
					return InsertElementConstantExpr
				if _llvm.LLVMExtractValue == op: # ExtractValue
					return ExtractValueConstantExpr
				if _llvm.LLVMInsertValue == op: # InsertValue
					return InsertValueConstantExpr
				if _llvm.LLVMShuffleVector == op: # ShuffleVector
					return ShuffleVectorConstantExpr
				return ConstantExpr
			if _llvm.LLVMIsAConstantFP(ref): return ConstantFP
			if _llvm.LLVMIsAConstantInt(ref): return ConstantInt
			if _llvm.LLVMIsAConstantPointerNull(ref): return ConstantPointerNull
			if _llvm.LLVMIsAConstantStruct(ref): return ConstantStruct
			if _llvm.LLVMIsAConstantVector(ref): return ConstantVector
			if _llvm.LLVMIsAGlobalValue(ref):
				if _llvm.LLVMIsAFunction(ref):
					if _llvm.LLVMGetIntrinsicID(ref) != 0:
						return Intrinsic
					return Function
				if _llvm.LLVMIsAGlobalAlias(ref): return GlobalAlias
				if _llvm.LLVMIsAGlobalVariable(ref): return GlobalVariable
				return GlobalValue
			if _llvm.LLVMIsAUndefValue(ref): return UndefValue
			return Constant
		if _llvm.LLVMIsAInstruction(ref):
			if _llvm.LLVMIsABinaryOperator(ref): return BinaryOperator
			if _llvm.LLVMIsACallInst(ref):
				if _llvm.LLVMIsAIntrinsicInst(ref):
					if _llvm.LLVMIsADbgInfoIntrinsic(ref):
						if _llvm.LLVMIsADbgDeclareInst(ref): return DbgDeclareInst
						return DbgInfoIntrinsic
					if _llvm.LLVMIsAMemIntrinsic(ref):
						if _llvm.LLVMIsAMemCpyInst(ref): return MemCpyInst
						if _llvm.LLVMIsAMemMoveInst(ref): return MemMoveInst
						if _llvm.LLVMIsAMemSetInst(ref): return MemSetInst
						return MemIntrinsic
					return IntrinsicInst
				return CallInst
			if _llvm.LLVMIsACmpInst(ref):
				if _llvm.LLVMIsAFCmpInst(ref): return FCmpInst
				if _llvm.LLVMIsAICmpInst(ref): return ICmpInst
				return CmpInst
			if _llvm.LLVMIsAExtractElementInst(ref): return ExtractElementInst
			if _llvm.LLVMIsAGetElementPtrInst(ref): return GetElementPtrInst
			if _llvm.LLVMIsAInsertElementInst(ref): return InsertElementInst
			if _llvm.LLVMIsAInsertValueInst(ref): return InsertValueInst
			if _llvm.LLVMIsALandingPadInst(ref): return LandingPadInst
			if _llvm.LLVMIsAPHINode(ref): return PHINode
			if _llvm.LLVMIsASelectInst(ref): return SelectInst
			if _llvm.LLVMIsAShuffleVectorInst(ref): return ShuffleVectorInst
			if _llvm.LLVMIsAStoreInst(ref): return StoreInst
			if _llvm.LLVMIsATerminatorInst(ref):
				if _llvm.LLVMIsABranchInst(ref): return BranchInst
				if _llvm.LLVMIsAIndirectBrInst(ref): return IndirectBrInst
				if _llvm.LLVMIsAInvokeInst(ref): return InvokeInst
				if _llvm.LLVMIsAReturnInst(ref): return ReturnInst
				if _llvm.LLVMIsASwitchInst(ref): return SwitchInst
				if _llvm.LLVMIsAUnreachableInst(ref): return UnreachableInst
				if _llvm.LLVMIsAResumeInst(ref): return ResumeInst
				return TerminatorInst
			if _llvm.LLVMIsAUnaryInstruction(ref):
				if _llvm.LLVMIsAAllocaInst(ref): return AllocaInst
				if _llvm.LLVMIsACastInst(ref):
					if _llvm.LLVMIsABitCastInst(ref): return BitCastInst
					if _llvm.LLVMIsAAddrSpaceCastInst(ref): return AddrSpaceCastInst
					if _llvm.LLVMIsAFPExtInst(ref): return FPExtInst
					if _llvm.LLVMIsAFPToSIInst(ref): return FPToSIInst
					if _llvm.LLVMIsAFPToUIInst(ref): return FPToUIInst
					if _llvm.LLVMIsAFPTruncInst(ref): return FPTruncInst
					if _llvm.LLVMIsAIntToPtrInst(ref): return IntToPtrInst
					if _llvm.LLVMIsAPtrToIntInst(ref): return PtrToIntInst
					if _llvm.LLVMIsASExtInst(ref): return SExtInst
					if _llvm.LLVMIsASIToFPInst(ref): return SIToFPInst
					if _llvm.LLVMIsATruncInst(ref): return TruncInst
					if _llvm.LLVMIsAUIToFPInst(ref): return UIToFPInst
					if _llvm.LLVMIsAZExtInst(ref): return ZExtInst
					return CastInst
				if _llvm.LLVMIsAExtractValueInst(ref): return ExtractValueInst
				if _llvm.LLVMIsALoadInst(ref): return LoadInst
				if _llvm.LLVMIsAVAArgInst(ref): return VAArgInst
				return UnaryInstruction
			#if _llvm.LLVMIsAAtomicRMWInst(ref): return AtomicRMWInst
			#if _llvm.LLVMIsAFenceInst(ref): return FenceInst
			return Instruction
		return User
	return Value

cdef Value _get_llvmvalue(_llvm.LLVMValueRef ref, LLVMContext context):
	if ref == NULL:
		raise _exceptions.ValueCreationError('Could not get LLVM value (null pointer)')
	cdef type cls = _get_valueclass(ref)
	cdef Value v = Value.__new__(cls, context)
	v._ref = ref
	return v

cdef BasicBlock _get_llvmbblock(_llvm.LLVMBasicBlockRef ref, LLVMContext context):
	if ref == NULL:
		raise _exceptions.ValueCreationError('Could not get LLVM basic block (null pointer)')
	cdef BasicBlock v = BasicBlock.__new__(BasicBlock, context)
	v._ref = _llvm.LLVMBasicBlockAsValue(ref)
	v._bref = ref
	v.__init__()
	return v

@final
cdef class _Value_Users_Iterator:

	cdef Value _val
	cdef _llvm.LLVMUseRef _first, _cur
	cdef int _done
	cdef readonly object _current

	def __cinit__(self, Value v):
		if v is None:
			raise ValueError
		self._val = v
		self._first = _llvm.LLVMGetFirstUse(self._val._ref)
		self._cur = NULL
		self._done = 0
		self._current = None

	cdef _finish(self):
		self._done = 1
		self._cur = NULL
		self._current = None
		self._val = None
		raise StopIteration

	cdef inline _make_value(self):
		return _get_llvmvalue(_llvm.LLVMGetUser(self._cur), self._val.context)

	def __iter__(self):
		return self

	def __next__(self):
		if self._done:
			raise StopIteration
		if self._cur == NULL:
			self._cur = self._first
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current
		else:
			self._cur = _llvm.LLVMGetNextUse(self._cur)
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current

	def __repr__(self):
		data = {'clsmod': type(self).__module__, 'val': self._val, 'addr': id(self)}
		return "<{clsmod}.Value.users iterator for value {val!r} at 0x{addr:x}>".format(**data)

@final
cdef class _Value_Users:

	cdef Value _val

	def __cinit__(self, Value v):
		if v is None:
			raise ValueError
		self._val = v

	def __init__(self, *args):
		raise TypeError("Cannot be manually instantiated")

	def __bool__(self):
		return _llvm.LLVMGetFirstUse(self._val._ref) != NULL

	def __iter__(self):
		return _Value_Users_Iterator(self._val)

	def __contains__(self, symbol):
		cdef _llvm.LLVMUseRef use
		cdef _llvm.LLVMValueRef user
		cdef _llvm.LLVMValueRef target
		if isinstance(symbol, User):
			use = _llvm.LLVMGetFirstUse(self._val._ref)
			target = (<Value>symbol)._ref
			while use != NULL:
				user = _llvm.LLVMGetUser(use)
				if target is user:
					return True
				use = _llvm.LLVMGetNextUse(use)
		return False

	def __len__(self):
		cdef _llvm.LLVMUseRef x = _llvm.LLVMGetFirstUse(self._val._ref)
		cdef unsigned long i = 0
		while True:
			if x == NULL:
				break
			x = _llvm.LLVMGetNextUse(x)
			i+=1
		return i

	def __repr__(self):
		data = {'clsmod': type(self).__module__, 'val': self._val, 'addr': id(self)}
		return "<{clsmod}.Value.users for value {val!r}>".format(**data)

	cdef object __weakref__

cdef class Value:
	"""
		LLVM Value Representation.

		This is a very important LLVM class. It is the base class of all values
		computed by a program that may be used as operands to other values.
		Value is the super class of other important classes such as Instruction
		and Function. All Values have a :py:class:`Type`. Type is not a subclass of Value.
		Some values can have a name and they belong to some :py:class:`Module`. Setting the
		name on the Value automatically updates the module's symbol table.
	"""

	def __cinit__(self, LLVMContext context, *args, **kwargs):
		self._ref = NULL
		self.context = context
		self._users = None

	def __init__(self, *args, **kwargs):
		raise TypeError("Cannot be instantiated directly")

	__hash__ = None

	def __richcmp__(self, other, int op):
		if op == 2:
			if not isinstance(other, Value) or not isinstance(self, Value):
				return False
			return (<Value>self)._ref == (<Value>other)._ref
		elif op == 3:
			if not isinstance(other, Value) or not isinstance(self, Value):
				return True
			return (<Value>self)._ref != (<Value>other)._ref
		else:
			return NotImplemented

	def dump(self):
		'''
			x.dump()

			Dump a representation of this value to STDERR.

			For debugging purposes only.
		'''
		_llvm.LLVMDumpValue(self._ref)

	def __str__(Value self):
		cdef char* string = _llvm.LLVMPrintValueToString(self._ref)
		cdef bytes data
		try:
			data = string
			return data.decode('utf-8').rstrip()
		finally:
			_llvm.LLVMDisposeMessage(string)

	property name:
		"""
			The values global name

			When changing, a new unique name is used if the provided name is taken.
			Use del x.name or set to ``""`` to remove the value's name.
		"""
		def __get__(self):
			cdef bytes namedata = _llvm.LLVMGetValueName(self._ref)
			if namedata:
				return namedata.decode("utf-8")
			else:
				return None
		def __set__(self, str name):
			cdef bytes namedata = name.encode("utf-8")
			_llvm.LLVMSetValueName(self._ref, namedata)
		def __del__(self):
			_llvm.LLVMSetValueName(self._ref, b"")

	cpdef takeName(self, Value other):
		"""
			x.takeName(other)

			Transfer the name from `other` to this value, setting `other`'s name to empty.
		"""
		if other is None:
			raise ValueError
		if self == other:
			raise ValueError('Cannot take own name.')
		cdef bytes namedata = _llvm.LLVMGetValueName(other._ref)
		_llvm.LLVMSetValueName(other._ref, b"")
		_llvm.LLVMSetValueName(self._ref, namedata)

	property type:
		"""
			The values type (:py:class:`llvm3.types.Type` instance)
		"""
		def __get__(self):
			cdef Type typ = Type.__new__(Type)
			typ._ref = _llvm.LLVMTypeOf(self._ref)
			if typ._ref == NULL:
				raise _exceptions.LLVMException('Could not get type of {0} object at {1} (null pointer)'.format(type(self).__name__), hex(id(self)))

			typ.context = self.context
			return typ._get_correct_instance()

	property users:
		"""
			The values users list.

			Iterating yields all values using this value as operand.
		"""
		def __get__(self):
			if self._users == None or self._users() == None:
				obj = _Value_Users.__new__(_Value_Users, self)
				self._users = _weakref(obj)
			else:
				obj = self._users()
			return obj

	cpdef hasOneUser(self):
		"""
			x.hasOneUser() -> bool

			Return ``True`` if there is exactly one user of this value. This is
			specialized because it is a common request and does not require
			traversing the whole use list.
		"""
		cdef _llvm.LLVMUseRef x = _llvm.LLVMGetFirstUse(self._ref)
		if x == NULL:
			return False
		x = _llvm.LLVMGetNextUse(x)
		return x == NULL

	cpdef replaceAllUsesWith(self, Value replacement):
		"""
			x.replaceAllUsesWith(replacement)

			Go through the users list for this definition and make each use point
			to `replacement` instead of `self`. After this completes, self's
			users list is guaranteed to be empty.
		"""
		if replacement is None:
			raise ValueError
		_llvm.LLVMReplaceAllUsesWith(self._ref, replacement._ref)

	def _get_correct_instance(self):
		return _get_llvmvalue(self._ref, self.context)

	def __repr__(self):
		data = {
			'clsmod':  type(self).__module__,
			'cls':     type(self).__name__,
			'type':    self.type,
			'context': ' in context {0}'.format(hex(id(self.context))) if self.context != _globalContext else '',
			'str':    str(self),
		}
		return "<{clsmod}.{cls} {str}{context}>".format(**data)

class Argument(Value):

	@property
	def rawAttributes(Value self):
		cdef uint64_t attrs = <uint64_t>_llvm.LLVMGetAttribute(self._ref)
		return attrs

	def _get_attr(Value self, uint64_t attr):
		return bool(self.rawAttributes & attr)

	def _set_attr(Value self, uint64_t attr, bint y):
		if y:
			_llvm.LLVMAddAttribute(self._ref, <_llvm.LLVMAttribute>attr)
		else:
			_llvm.LLVMRemoveAttribute(self._ref, <_llvm.LLVMAttribute>attr)

	def _get_zeroext(Value self):
		return self._get_attr(_llvm.LLVMZExtAttribute)
	def _set_zeroext(Value self, bint v):
		self._set_attr(_llvm.LLVMZExtAttribute, v)
	zeroext = property(_get_zeroext, _set_zeroext)

	def _get_signext(Value self):
		return self._get_attr(_llvm.LLVMSExtAttribute)
	def _set_signext(Value self, bint v):
		self._set_attr(_llvm.LLVMSExtAttribute, v)
	signext = property(_get_signext, _set_signext)

	def _get_inreg(Value self):
		return self._get_attr(_llvm.LLVMInRegAttribute)
	def _set_inreg(Value self, bint v):
		self._set_attr(_llvm.LLVMInRegAttribute, v)
	inreg = property(_get_inreg, _set_inreg)

	def _get_byval(Value self):
		return self._get_attr(_llvm.LLVMByValAttribute)
	def _set_byval(Value self, bint v):
		self._set_attr(_llvm.LLVMByValAttribute, v)
	byval = property(_get_byval, _set_byval)

	def _get_sret(Value self):
		return self._get_attr(_llvm.LLVMStructRetAttribute)
	def _set_sret(Value self, bint v):
		self._set_attr(_llvm.LLVMStructRetAttribute, v)
	sret = property(_get_sret, _set_sret)

	def _get_noalias(Value self):
		return self._get_attr(_llvm.LLVMNoAliasAttribute)
	def _set_noalias(Value self, bint v):
		self._set_attr(_llvm.LLVMNoAliasAttribute, v)
	noalias = property(_get_noalias, _set_noalias)

	def _get_nocapture(Value self):
		return self._get_attr(_llvm.LLVMNoCaptureAttribute)
	def _set_nocapture(Value self, bint v):
		self._set_attr(_llvm.LLVMNoCaptureAttribute, v)
	nocapture = property(_get_nocapture, _set_nocapture)

	def _get_nest(Value self):
		return self._get_attr(_llvm.LLVMNestAttribute)
	def _set_nest(Value self, bint v):
		self._set_attr(_llvm.LLVMNestAttribute, v)
	nest = property(_get_nest, _set_nest)

	def _get_readonly(Value self):
		return self._get_attr(_llvm.LLVMReadOnlyAttribute)
	def _set_readonly(Value self, bint v):
		self._set_attr(_llvm.LLVMReadOnlyAttribute, v)
	readonly = property(_get_readonly, _set_readonly)

	def _get_readnone(Value self):
		return self._get_attr(_llvm.LLVMReadOnlyAttribute)
	def _set_readnone(Value self, bint v):
		self._set_attr(_llvm.LLVMReadNoneAttribute, v)
	readnone = property(_get_readnone, _set_readnone)


cdef class _BasicBlock_Iterator:

	cdef BasicBlock _block
	cdef _llvm.LLVMValueRef _first, _cur
	cdef int _done
	cdef readonly object _current
	cdef readonly int _mode

	def __cinit__(self, BasicBlock b, int mode=0):
		if b is None:
			raise ValueError
		self._block = b
		if mode == 1:
			self._first = _llvm.LLVMGetLastInstruction(self._block._bref)
		else:
			self._first = _llvm.LLVMGetFirstInstruction(self._block._bref)
		self._cur = NULL
		self._done = 0
		self._mode = mode
		self._current = None

	cdef _finish(self):
		self._done = 1
		self._cur = NULL
		self._current = None
		self._block = None
		raise StopIteration

	cdef inline _make_value(self):
		return _get_llvmvalue(self._cur, (<Value>self._block).context)

	def __iter__(self):
		return self

	def __next__(self):
		if self._done:
			raise StopIteration
		if self._cur == NULL:
			self._cur = self._first
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current
		else:
			if self._mode == 1:
				self._cur = _llvm.LLVMGetPreviousInstruction(self._cur)
			else:
				self._cur = _llvm.LLVMGetNextInstruction(self._cur)
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current

	def __repr__(self):
		return "<LLVM Instruction iterator for BasicBlock 0x{1:x}>".format(id(self._block))


cdef class BasicBlock(Value):

	def __cinit__(self, *args, **kwargs):
		self._ref = NULL
		self._bref = NULL
		self.parent = None

	def __init__(self, *args, **kwargs):
		if self._ref == NULL or self._bref == NULL:
			raise TypeError("BasicBlock cannot be instantiated directly")
		else:
			self.parent = _get_llvmvalue(_llvm.LLVMGetBasicBlockParent(self._bref), self.context)

	def __dealloc__(self):
		if self._bref and _llvm.LLVMGetBasicBlockParent(self._bref) == NULL and self.parent is None:
			_llvm.LLVMDeleteBasicBlock(self._bref)
			self._bref = NULL

	def __iter__(self):
		return _BasicBlock_Iterator(self, 0)

	def __reversed__(self):
		return _BasicBlock_Iterator(self, 1)

	cpdef empty(self):
		return _llvm.LLVMGetFirstInstruction(self._bref) == NULL

	property terminator:
		def __get__(self):
			return _get_llvmvalue(_llvm.LLVMGetBasicBlockTerminator(self._bref), (<Value>self).context)

	def builder(self):
		'''
			x.builder() -> IRBuilder instance

			Create a new :py:class:`llvm3.irbuilder.IRBuilder` for this basic block.
		'''

		from llvm3.irbuilder import IRBuilder
		return IRBuilder(self)

	def __len__(self):
		cdef _llvm.LLVMValueRef v = _llvm.LLVMGetFirstInstruction(self._bref)
		cdef unsigned long i = 0
		while (v != NULL):
			i += 1
			v = _llvm.LLVMGetNextInstruction(v)
		return i

	cdef inline _llvm.LLVMValueRef _firstNonPHI(self):
		cdef _llvm.LLVMValueRef v = _llvm.LLVMGetFirstInstruction(self._bref)
		while (v != NULL):
			if _llvm.LLVMIsAPHINode(v) == NULL:
				return v
			v = _llvm.LLVMGetNextInstruction(v)
		return NULL

	def getFirstNonPHI(self):
		'''
			x.getFirstNonPHI() -> Instruction

			Get the first instruction that isn't a phi node.

			Returns ``None`` if none such instruction exists.
		'''
		cdef _llvm.LLVMValueRef v = self._firstNonPHI()
		if (v != NULL):
			return _get_llvmvalue(v, (<Value>self).context)
		return None

	def isLandingPad(self):
		'''
			x.isLandingPad() -> bool

			Return ``True`` if this basic block is a landing pad.
		'''
		cdef _llvm.LLVMValueRef v = self._firstNonPHI()
		return _llvm.LLVMIsALandingPadInst(v) != NULL

	def remove(self):
		'''
			x.remove()

			Remove the basic block from its parent function.

			.. warning::
				If the last direct reference to a :class:`BasicBlock` instance removed from its parent ceases to exist,
				the block and its contents are destroyed.

				If a reference to an instruction inside the block is held after destruction of the basic block, SEGFAULTs may occur when operating on the instruction.
		'''
		_llvm.LLVMRemoveBasicBlockFromParent(self._bref)
		self.parent = None

	def moveBefore(self, BasicBlock other):
		'''
			x.moveBefore(other)

			Move the block before `other`.
		'''
		if other is None:
			raise ValueError
		_llvm.LLVMMoveBasicBlockBefore(self._bref, other._bref)
		self.parent = _get_llvmvalue(_llvm.LLVMGetBasicBlockParent(self._bref), (<Value>self).context)

	def moveAfter(self, BasicBlock other):
		'''
			x.moveBefore(other)

			Move the block after `other`.
		'''
		if other is None:
			raise ValueError
		_llvm.LLVMMoveBasicBlockAfter(self._bref, other._bref)

	cdef _llvm.LLVMValueRef _get_instr(self, unsigned int index):
		cdef _llvm.LLVMValueRef v = _llvm.LLVMGetFirstInstruction(self._bref)
		cdef unsigned long i = 0
		while (i < index):
			if v == NULL:
				return NULL
			i += 1
			v = _llvm.LLVMGetNextInstruction(v)
		return v

	def __getitem__(self, unsigned int index):
		cdef _llvm.LLVMValueRef v = self._get_instr(index)
		if (v != NULL):
			return _get_llvmvalue(v, (<Value>self).context)
		raise IndexError(index)

	def __delitem__(self, unsigned int index):
		cdef _llvm.LLVMValueRef v = self._get_instr(index)
		if (v != NULL):
			_llvm.LLVMInstructionEraseFromParent(v)
		raise IndexError(index)

	def __str__(Value self):
		return 'basic block{0}'.format(
			' in function {0}'.format(self.parent.name if self.parent.name else hex(id(self.parent))) if self.parent else ''
		) + Value.__str__(self)

	def __repr__(Value self):
		data = {
			'clsmod':  type(self).__module__,
			'cls':     type(self).__name__,
			'type':    self.type,
			'context': ' in context {0}'.format(hex(id(self.context))) if self.context != _globalContext else '',
			'name':    repr(self.name) if self.name else hex(<uintptr_t>self._ref),
			'len':     len(self),
			'parent':  'function {0}'.format(self.parent.name if self.parent.name else hex(<uintptr_t>(<Value>self.parent)._ref)) if self.parent else 'no function'
		}
		return "<{clsmod}.{cls} {name} in {parent} ({len} instructions){context}>".format(**data)

class InlineAsm(Value):

	def __new__(cls, Type type, str asmstring, str constraints, bint sideeffect, bint alignstack):
		if not isinstance(type, FunctionType):
			raise TypeError('InlineAsm needs a function type')

		cdef bytes asmdata = asmstring.encode('utf-8')
		cdef bytes constraintsdata = constraints.encode('ascii')
		return _get_llvmvalue(_llvm.LLVMConstInlineAsm(type._ref, asmdata, constraintsdata, sideeffect, alignstack), type.context)

	def __init__(self, *args, **kwargs):
		pass

class MDNode(Value):

	def __new__(cls, elements, LLVMContext context=None):
		if elements is None:
			raise ValueError

		if len(elements) == 0 and context is None:
			raise ValueError('Cannot derive context from empty element list. `context\' parameter must be specified.')

		cdef LLVMValueRef* elarray = NULL
		cdef unsigned length = len(elements)
		cdef Value obj

		if context is None:
			for item in elements:
				if isinstance(item, Value):
					context = (<Value>item).context
					break

		if context is None:
			raise ValueError('No valid context specified and context could not be inferred')

		if length:
			elarray = <LLVMValueRef*>calloc(length, sizeof(LLVMValueRef))
		try:
			for i, item in enumerate(elements):
				if item is None:
					elarray[i] = NULL
				elif isinstance(item, int):
					elarray[i] = (<Value>ConstantInt(item, 32, context))._ref
				elif isinstance(item, Value):
					if context != (<Value>item).context:
						raise ValueError('Item with different context found')
					elarray[i] = (<Value>item)._ref
				else:
					raise TypeError('{0!r} is no valid LLVM value'.format(item))

			obj = Value.__new__(cls, context)
			obj._ref = _llvm.LLVMMDNodeInContext(context._ref, elarray, length)
			if obj._ref == NULL:
				raise _exceptions.ValueCreationError('Could not create MDNode from {0!r}'.format(elements))
		finally:
			free(elarray)
		return obj

	def __init__(self, *args, **kwargs):
		pass

	@property
	def operands(Value self):
		cdef list l = []
		cdef long i = 0
		cdef LLVMValueRef ref = NULL
		for i in range(_llvm.LLVMGetNumOperands(self._ref)):
			ref = _llvm.LLVMGetOperand(self._ref, i)
			if ref == NULL:
				l.append(None)
			else:
				l.append(_get_llvmvalue(ref, self.context))
		return l

	def __iter__(self):
		return self.operands.__iter__()

	def __getitem__(self, x):
		return self.operands.__getitem__(x)

	def __hash__(Value self):
		return <long>(self._ref)


class MDString(Value):

	def __new__(cls, str value, LLVMContext context):
		if value is None or context is None:
			raise ValueError

		cdef bytes valuedata = value.encode("utf-8")

		cdef Value obj = Value.__new__(cls, context)
		obj._ref = _llvm.LLVMMDStringInContext(context._ref, valuedata, len(valuedata))
		if obj._ref == NULL:
				raise _exceptions.ValueCreationError('Could not create MDString from {0!r}'.format(value))
		return obj

	def __init__(self, *args, **kwargs):
		pass

	def __hash__(Value self):
		return <long>(self._ref)

	@property
	def value(Value self):
		cdef unsigned length
		cdef char* val = <char*>_llvm.LLVMGetMDString(self._ref, &length)
		return val[:length].decode('utf-8')

class User(Value):

	@property
	def operands(self):
		'''Operands of this value.'''
		#TODO: make Operands class for modification of operands
		l = []
		cdef long i
		cdef LLVMValueRef ref
		for i in range(_llvm.LLVMGetNumOperands((<Value>self)._ref)):
			ref = _llvm.LLVMGetOperand((<Value>self)._ref, i)
			l.append(_get_llvmvalue(ref, (<Value>self).context))
		return l

	def setOperand(Value self, int index, Value operand):
		'''
			x.setOperand(index, operand)

			Set the `index`-th operand of the value to `operand`.

			Fails with :py:exc:`IndexError` if `index` is out of bounds.

			Fails with :py:exc:`TypeError` if operands of this subclass instance cannot be changed
			(e.g. :py:class:`Constant`).
		'''
		cdef int count = _llvm.LLVMGetNumOperands(self._ref)
		cdef LLVMValueRef ref = NULL
		if not 0 < index < count:
			raise IndexError('operand index out of bounds')
		if isinstance(self, Constant):
			raise TypeError('cannot change operands of a Constant')
		if operand is not None:
			ref = operand._ref
		_llvm.LLVMSetOperand(self._ref, index, ref)


class Constant(User):
	'''
		Constant(value[, type[, context[, \*\*kwargs]]]) -> Constant instance

		Create a :class:`Value` object representing a constant value.

		Can be used as a shortcut function for the constructors of :class:`ConstantInt`,
		:class:`ConstantFP`, :class:`BlockAddress`, :class:`ConstantStruct`,
		:class:`ConstantVector`, :class:`ConstantArray` and :class:`ConstantString`.

		Raises :py:exc:`TypeError` if `value` cannot be used to construct a Constant or if `type` and `value` don't match.

		Raises :py:exc:`ValueError` if `value` cannot be converted to a Constant without specifying a `type`.

		Note that Constant instances are hashable as they cannot change.
	'''

	def __new__(cls, object value, Type t = None, LLVMContext context=None, **kwargs):
		if cls is not Constant:
			raise TypeError('Subclassing does not work correctly on LLVM value objects.')

		if isinstance(value, float):
			if t is not None:
				return ConstantFP(value, t, **kwargs)
			else:
				raise ValueError("too unspecific without type specifier")
		elif isinstance(value, int):
			if t is not None:
				return ConstantInt(value, t, **kwargs)
			else:
				raise ValueError("too unspecific without type specifier")
		elif isinstance(value, BasicBlock):
			if t is not None and not isinstance(t, BlockAddress):
				raise TypeError("invalid type for this value")
			return BlockAddress(value, **kwargs)
		elif isinstance(value, list):
			if len(value) == 0:
				if t is None:
					raise ValueError("too unspecific without type specifier")
			else:
				if t is None:
					t = ArrayType(value[0].type, len(value), **kwargs)
			if isinstance(t, ArrayType):
				return ConstantArray(value, t, **kwargs)
			elif isinstance(t, VectorType):
				return ConstantVector(value, **kwargs)
			elif isinstance(t, StructType):
				return ConstantStruct(value, context, **kwargs)
			else:
				raise TypeError("invalid type for value")
		elif isinstance(value, tuple):
			if len(value) == 0:
				if t is None:
					t = StructType([], context)
			else:
				if t is None:
					if 'packed' in kwargs:
						t = StructType([v.type for v in value], True, context)
						del kwargs['packed']
					else:
						t = StructType([v.type for v in value], context)
			if isinstance(t, ArrayType):
				return ConstantArray(value, t, **kwargs)
			elif isinstance(t, VectorType):
				return ConstantVector(value, **kwargs)
			elif isinstance(t, StructType):
				return ConstantStruct(value, context, **kwargs)
			else:
				raise TypeError("invalid type for value")
		elif isinstance(value, (bytes, bytearray)):
			value = bytes(value)
			if 'nullterminated' in kwargs and not kwargs['nullterminated']:
				valuelen = len(value)
			else:
				valuelen = len(value)+1
			if t is not None:
				if not isinstance(t, ArrayType) or not isinstance(t.elementType, IntegerType):
					raise TypeError("Invalid type for string. Must be [{0} x i8]".format(valuelen))
				if len(t.elementType) != valuelen and len(t.elementType) != 0:
					raise TypeError("Invalid type for string. Must be [{0} x i8]".format(valuelen))
			return ConstantString(value, context, **kwargs)

		raise TypeError("cannot transform into constant: {0!r}".format(value))

	@staticmethod
	def getNull(Type type):
		'''
			Constant.getNull(type) -> Constant instance

			Obtain a constant value referring to the null instance of `type`.
		'''
		if type is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMConstNull(type._ref), type.context)

	@staticmethod
	def getAllOnes(Type type):
		'''
			Constant.getAllOnes(type) -> Constant instance

			Obtain a constant value referring to the instance of a type consisting of all ones.

			.. note::
				This is only valid for integer types. Calling with any other type will cause a :py:exc:`TypeError`.
		'''

		if not isinstance(type, IntegerType):
			raise TypeError('Not an integer type')

		return _get_llvmvalue(_llvm.LLVMConstAllOnes(type._ref), type.context)

	def isNull(Value self):
		''' x.isNull() -> bool '''
		return _llvm.LLVMIsNull(self._ref)

	def __hash__(Value self):
		return <long>(self._ref)

class BlockAddress(Constant):

	def __new__(cls, BasicBlock value):
		if not isinstance(value.parent, Function):
			raise ValueError
		if cls is not BlockAddress:
			raise TypeError('Subclassing does not work correctly on LLVM value objects.')
		return _get_llvmvalue(_llvm.LLVMBlockAddress((<Value>value.parent)._ref, value._bref), value.context)

	def __init__(self, *args, **kwargs):
		pass

class GlobalValue(Constant):

	@property
	def parent(Value self):
		cdef _llvm.LLVMModuleRef ref = _llvm.LLVMGetGlobalParent(self._ref)
		return self.context._module_cache[<uintptr_t>ref]

	@property
	def declaration(Value self):
		return _llvm.LLVMIsDeclaration(self._ref)

	def _get_linkage(Value self):
		'''The linkage of this global'''
		cdef _llvm.LLVMLinkage v = _llvm.LLVMGetLinkage(self._ref)
		return LINKAGES.get(v, v)
	def _set_linkage(Value self, str newval):
		ldict = dict((b,a) for (a,b) in LINKAGES.items())
		if newval in ldict:
			_llvm.LLVMSetLinkage(self._ref, ldict[newval])
		else:
			raise ValueError("unknown linkage {0!r}".format(newval))
	linkage = property(_get_linkage, _set_linkage)

	def hasLocalLinkage(Value self):
		'''
			x.hasLocalLinkage() -> bool

			Return ``True`` if this global has internal, private, linker_private
			or linker_private_weak linkage.
		'''
		cdef _llvm.LLVMLinkage l = _llvm.LLVMGetLinkage(self._ref)
		return l in (
			_llvm.LLVMInternalLinkage,
			_llvm.LLVMPrivateLinkage,
			_llvm.LLVMLinkerPrivateLinkage,
			_llvm.LLVMLinkerPrivateWeakLinkage
		)

	def _get_visibility(Value self):
		'''The visibility style of this global'''
		cdef _llvm.LLVMVisibility v = _llvm.LLVMGetVisibility(self._ref)
		return VISIBILITIES.get(v, v)
	def _set_visibility(Value self, str newval):
		vdict = dict((b,a) for (a,b) in VISIBILITIES.items())
		if newval in vdict:
			_llvm.LLVMSetVisibility(self._ref, vdict[newval])
		else:
			raise ValueError("unknown visibility {0!r}".format(newval))
	visibility = property(_get_visibility, _set_visibility)

	def _get_dllStorageClass(Value self):
		'''The DLL storage class of this global'''
		cdef _llvm.LLVMDLLStorageClass v = _llvm.LLVMGetDLLStorageClass(self._ref)
		return DLLSTORAGECLASSES.get(v, v)
	def _set_dllStorageClass(Value self, str newval):
		if not newval:
			_llvm.LLVMSetDLLStorageClass(self._ref, _llvm.LLVMDefaultStorageClass)
		else:
			vdict = dict((b,a) for (a,b) in DLLSTORAGECLASSES.items())
			if newval in vdict:
				_llvm.LLVMSetDLLStorageClass(self._ref, vdict[newval])
			else:
				raise ValueError("unknown DLL storage class {0!r}".format(newval))
	dllStorageClass = property(_get_dllStorageClass, _set_dllStorageClass)

	def _get_section(Value self):
		cdef bytes s = _llvm.LLVMGetSection(self._ref)
		if s:
			return s.decode("utf-8")
		else:
			return None
	def _set_section(Value self, str newval):
		cdef bytes s
		if newval is None:
			_llvm.LLVMSetSection(self._ref, '')
		else:
			s = newval.encode('utf-8')
			_llvm.LLVMSetSection(self._ref, s)
	section = property(_get_section, _set_section)

	def _get_alignment(Value self):
		'''Alignment of this symbol, must be power of two'''
		return _llvm.LLVMGetAlignment(self._ref)
	def _set_alignment(Value self, unsigned newval):
		_llvm.LLVMSetAlignment(self._ref, newval)
	alignment = property(_get_alignment, _set_alignment)

	@property
	def addressSpace(Value self):
		'''Address space where this global should belong to'''
		return _llvm.LLVMGetPointerAddressSpace((<Type>self.type)._ref)

	def _get_unnamedAddr(Value self):
		'''If this value's address is not significant'''
		return _llvm.LLVMHasUnnamedAddr(self._ref)

	def _set_unnamedAddr(Value self, bint v):
		_llvm.LLVMSetUnnamedAddr(self._ref, v)

	unnamedAddr = property(_get_unnamedAddr, _set_unnamedAddr)

class GlobalVariable(GlobalValue):

	def __init__(self, *args, **kwargs):
		pass

	def _get_initializer(Value self):
		cdef _llvm.LLVMValueRef ref = _llvm.LLVMGetInitializer(self._ref)
		if ref is not NULL:
			return _get_llvmvalue(_llvm.LLVMGetInitializer(self._ref), self.context)
		else:
			return None
	def _set_initializer(Value self, Value newval):
		if not isinstance(newval, Constant):
			raise ValueError("Initializers must be constant")
		_llvm.LLVMSetInitializer(self._ref, newval._ref)
	initializer = property(_get_initializer, _set_initializer)

	def setZeroInitializer(Value self):
		'''
			x.setZeroInitializer()

			Set the initializer to the **zeroinitializer**.

			See `ConstantAggregateZero` and :py:class:`Constant`.`getNull`().
		'''
		_llvm.LLVMSetInitializer(self._ref, _llvm.LLVMConstNull(_llvm.LLVMGetElementType(_llvm.LLVMTypeOf(self._ref))))

	def _get_threadLocal(Value self):
		return _llvm.LLVMIsThreadLocal(self._ref)
	def _set_threadLocal(Value self, bint newval):
		_llvm.LLVMSetThreadLocal(self._ref, newval)
	threadLocal = property(_get_threadLocal, _set_threadLocal)

	def _get_constant(Value self):
		return _llvm.LLVMIsGlobalConstant(self._ref)
	def _set_constant(Value self, bint newval):
		_llvm.LLVMSetGlobalConstant(self._ref, newval)
	constant = property(_get_constant, _set_constant)

class GlobalAlias(GlobalValue):

	def __init__(self, *args, **kwargs):
		pass

	@property
	def aliasee(self):
		for x in self.operands:
			return x

cdef class _Function_Iterator:

	cdef Value _func
	cdef _llvm.LLVMBasicBlockRef _first, _cur
	cdef int _done
	cdef readonly object _current
	cdef readonly int _mode

	def __cinit__(self, Value f, int mode=0):
		if f is None or not isinstance(f, Function):
			raise ValueError
		self._func = f
		if mode == 1:
			self._first = _llvm.LLVMGetLastBasicBlock(self._func._ref)
		else:
			self._first = _llvm.LLVMGetFirstBasicBlock(self._func._ref)
		self._cur = NULL
		self._done = 0
		self._mode = mode
		self._current = None

	cdef _finish(self):
		self._done = 1
		self._cur = NULL
		self._current = None
		self._func = None
		raise StopIteration

	cdef inline _make_value(self):
		return _get_llvmbblock(self._cur, (<Value>self._func).context)

	def __iter__(self):
		return self

	def __next__(self):
		if self._done:
			raise StopIteration
		if self._cur == NULL:
			self._cur = self._first
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current
		else:
			if self._mode == 1:
				self._cur = _llvm.LLVMGetPreviousBasicBlock(self._cur)
			else:
				self._cur = _llvm.LLVMGetNextBasicBlock(self._cur)
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current

	def __repr__(self):
		return "<LLVM Basic Block iterator for Function 0x{1:x}>".format(id(self._block))


class Function(GlobalValue):

	def __init__(self, *args, **kwargs):
		pass

	def _get_callingconvention(Value self):
		cdef unsigned cc = _llvm.LLVMGetFunctionCallConv(self._ref)
		return _named_int(cc, CALLINGCONVENTIONS.get(cc, 'cc {0}'.format(cc)))
	def _set_callingconvention(Value self, cconv):
		cdef unsigned cc
		if isinstance(cconv, str):
			ccdict = dict((b,a) for (a,b) in CALLINGCONVENTIONS.items())

			if cconv in ccdict:
				cc = ccdict[cconv]
			elif cconv.startswith('cc ') and cconv[3:].isdigit():
				cc = <unsigned>int(cconv[3:])
			else:
				raise ValueError("unknown calling convention string {0!r}".format(cconv))
		elif isinstance(cconv, int):
			if cconv >= 0:
				cc = cconv
			else:
				raise ValueError("negative numbers not allowed as calling convention codes")
		else:
			raise TypeError("Invalid type for calling convention {0!r}".format(type(cconv).__name__))
		_llvm.LLVMSetFunctionCallConv(self._ref, cc)
	callingConvention = property(_get_callingconvention, _set_callingconvention)

	def _get_gc(Value self):
		cdef bytes s
		cdef char* cstr = <char*>_llvm.LLVMGetGC(self._ref)
		if cstr != NULL:
			s = cstr
		else:
			return None

		if s:
			return s.decode("utf-8")
		else:
			return s
	def _set_gc(Value self, str newval):
		cdef bytes s
		s = newval.encode('utf-8')
		_llvm.LLVMSetGC(self._ref, s)
	def _del_gc(Value self):
		_llvm.LLVMSetGC(self._ref, NULL)
	gc = property(_get_gc, _set_gc, _del_gc)

	@property
	def rawAttributes(Value self):
		cdef uint64_t attrs = <uint64_t>_llvm.LLVMGetFunctionAttr(self._ref)
		return attrs

	def _get_attr(Value self, uint64_t attr):
		return bool(self.rawAttributes & attr)

	def _set_attr(Value self, uint64_t attr, bint y):
		if y:
			_llvm.LLVMAddFunctionAttr(self._ref, <_llvm.LLVMAttribute>attr)
		else:
			_llvm.LLVMRemoveFunctionAttr(self._ref, <_llvm.LLVMAttribute>attr)

	def _get_address_safety(Value self):
		if sizeof(_llvm.LLVMAttribute) <= 4:
			return None
		return self._get_attr(1<<32)
	def _set_address_safety(Value self, bint v):
		self._set_attr(1<<32, v)
	address_safety = property(_get_address_safety, _set_address_safety)

	def _get_alignstack(Value self):
		cdef uint64_t m = _llvm.LLVMStackAlignment, i = 0
		while m & 1 == 0:
			m >>= 1
			i += 1
		return (1 << ((self.rawAttributes & _llvm.LLVMStackAlignment) >> i)) >> 1

	def _set_alignstack(Value self, unsigned v):
		cdef uint64_t m = _llvm.LLVMStackAlignment, pos = 0, i = 0
		if (v & (v - 1)) != 0:
			raise ValueError("Must be a power of two")
		if v > 64:
			raise ValueError("Number too big")

		_llvm.LLVMRemoveFunctionAttr(self._ref, _llvm.LLVMStackAlignment)

		if v != 0:
			v <<= 1
			while m & 1 == 0:
				m >>= 1
				pos += 1
			while v & 1 == 0:
				v >>= 1
				i += 1
			_llvm.LLVMAddFunctionAttr(self._ref, <_llvm.LLVMAttribute>((i << pos) & _llvm.LLVMStackAlignment))

	alignstack = property(_get_alignstack, _set_alignstack)

	def _get_alwaysinline(Value self):
		return self._get_attr(_llvm.LLVMAlwaysInlineAttribute)
	def _set_alwaysinline(Value self, bint v):
		self._set_attr(_llvm.LLVMAlwaysInlineAttribute, v)
	alwaysinline = property(_get_alwaysinline, _set_alwaysinline)

	def _get_nonlazybind(Value self):
		return self._get_attr(_llvm.LLVMNonLazyBind)
	def _set_nonlazybind(Value self, bint v):
		if v:
			_llvm.LLVMAddFunctionAttr(self._ref, _llvm.LLVMNonLazyBind)
		else:
			_llvm.LLVMRemoveFunctionAttr(self._ref, _llvm.LLVMNonLazyBind)
	nonlazybind = property(_get_nonlazybind, _set_nonlazybind)

	def _get_inlinehint(Value self):
		return self._get_attr(_llvm.LLVMInlineHintAttribute)
	def _set_inlinehint(Value self, bint v):
		self._set_attr(_llvm.LLVMInlineHintAttribute, v)
	inlinehint = property(_get_inlinehint, _set_inlinehint)

	def _get_naked(Value self):
		return self._get_attr(_llvm.LLVMNakedAttribute)
	def _set_naked(Value self, bint v):
		self._set_attr(_llvm.LLVMNakedAttribute, v)
	naked = property(_get_naked, _set_naked)

	def _get_noimplicitfloat(Value self):
		return self._get_attr(_llvm.LLVMNoImplicitFloatAttribute)
	def _set_noimplicitfloat(Value self, bint v):
		self._set_attr(_llvm.LLVMNoImplicitFloatAttribute, v)
	noimplicitfloat = property(_get_noimplicitfloat, _set_noimplicitfloat)

	def _get_noinline(Value self):
		return self._get_attr(_llvm.LLVMNoInlineAttribute)
	def _set_noinline(Value self, bint v):
		self._set_attr(_llvm.LLVMNoInlineAttribute, v)
	noinline = property(_get_noinline, _set_noinline)

	def _get_noredzone(Value self):
		return self._get_attr(_llvm.LLVMNoRedZoneAttribute)
	def _set_noredzone(Value self, bint v):
		self._set_attr(_llvm.LLVMNoRedZoneAttribute, v)
	noredzone = property(_get_noredzone, _set_noredzone)

	def _get_noreturn(Value self):
		return self._get_attr(_llvm.LLVMNoReturnAttribute)
	def _set_noreturn(Value self, bint v):
		self._set_attr(_llvm.LLVMNoReturnAttribute, v)
	noreturn = property(_get_noreturn, _set_noreturn)

	def _get_nounwind(Value self):
		return self._get_attr(_llvm.LLVMNoUnwindAttribute)
	def _set_nounwind(Value self, bint v):
		self._set_attr(_llvm.LLVMNoUnwindAttribute, v)
	nounwind = property(_get_nounwind, _set_nounwind)

	def _get_optsize(Value self):
		return self._get_attr(_llvm.LLVMOptimizeForSizeAttribute)
	def _set_optsize(Value self, bint v):
		self._set_attr(_llvm.LLVMOptimizeForSizeAttribute, v)
	optsize = property(_get_optsize, _set_optsize)

	def _get_readnone(Value self):
		return self._get_attr(_llvm.LLVMReadNoneAttribute)
	def _set_readnone(Value self, bint v):
		self._set_attr(_llvm.LLVMReadNoneAttribute, v)
	readnone = property(_get_readnone, _set_readnone)

	def _get_readonly(Value self):
		return self._get_attr(_llvm.LLVMReadOnlyAttribute)
	def _set_readonly(Value self, bint v):
		self._set_attr(_llvm.LLVMReadOnlyAttribute, v)
	readonly = property(_get_readonly, _set_readonly)

	def _get_returns_twice(Value self):
		return self._get_attr(_llvm.LLVMReturnsTwice)
	def _set_returns_twice(Value self, bint v):
		self._set_attr(_llvm.LLVMReturnsTwice, v)
	returns_twice = property(_get_returns_twice, _set_returns_twice)

	def _get_ssp(Value self):
		return self._get_attr(_llvm.LLVMStackProtectAttribute)
	def _set_ssp(Value self, bint v):
		self._set_attr(_llvm.LLVMStackProtectAttribute, v)
	ssp = property(_get_ssp, _set_ssp)

	def _get_sspreq(Value self):
		return self._get_attr(_llvm.LLVMStackProtectReqAttribute)
	def _set_sspreq(Value self, bint v):
		self._set_attr(_llvm.LLVMStackProtectReqAttribute, v)
	sspreq = property(_get_sspreq, _set_sspreq)

	def _get_uwtable(Value self):
		return self._get_attr(_llvm.LLVMUWTable)
	def _set_uwtable(Value self, bint v):
		self._set_attr(_llvm.LLVMUWTable, v)
	uwtable = property(_get_uwtable, _set_uwtable)

	@property
	def arguments(Value self):
		cdef list l = []
		cdef unsigned i = 0
		for i in range(_llvm.LLVMCountParams(self._ref)):
			l.append(_get_llvmvalue(_llvm.LLVMGetParam(self._ref, i), self.context))
		return l

	def setArgumentAlignment(Value self, unsigned align):
		'''
			x.setArgumentAlignment(align: int)

			Set the assumed alignment of the pointer argument to `align`.
		'''
		_llvm.LLVMSetParamAlignment(self._ref, align)

	@property
	def entry(Value self):
		'''The entry basic block for this function'''

		# Check if basic block list is empty:
		if not _llvm.LLVMCountBasicBlocks(self._ref):
			return None
		# Get entry basic block
		cdef LLVMBasicBlockRef ref = _llvm.LLVMGetEntryBasicBlock(self._ref)
		if ref == NULL:
			return None
		else:
			return _get_llvmbblock(ref, self.context)

	def __iter__(self):
		return _Function_Iterator(self, 0)

	def __reversed__(self):
		return _Function_Iterator(self, 1)

	def __getitem__(Value self, index):
		cdef LLVMBasicBlockRef block = _llvm.LLVMGetFirstBasicBlock(self._ref)
		if index >= 0:
			for i in range(index):
				block = _llvm.LLVMGetNextBasicBlock(block)
				if block == NULL:
					raise IndexError("Out of bounds")
		else:
			block = _llvm.LLVMGetLastBasicBlock(self._ref)
			for i in range((-index)-1):
				block = _llvm.LLVMGetPreviousBasicBlock(block)
				if block == NULL:
					raise IndexError("Out of bounds")

	def newBasicBlock(Value self, str name=None, BasicBlock before=None):
		'''
			x.newBasicBlock([name[, before]]) -> BasicBlock instance

			Create a basic block inside this function.
			If `before` is not ``None``, the new block will be inserted before that block, else the block will be appended to the end of the function.
		'''
		cdef bytes namedata = name.encode('utf-8') if name else b''
		if before is None:
			return _get_llvmbblock(_llvm.LLVMAppendBasicBlockInContext(self.context._ref, self._ref, namedata), self.context)
		else:
			return _get_llvmbblock(_llvm.LLVMInsertBasicBlockInContext(self.context._ref, before._bref, namedata), self.context)

class Intrinsic(Function):

	@property
	def intrinsicId(Value self):
		return _llvm.LLVMGetIntrinsicID(self._ref)

class ConstantAggregateZero(Constant):
	'''
		ConstantAggregateZero(type) -> Constant instance

		Obtain a constant value representing a **zeroinitializer** value of the given aggregate type.

		Use :py:class:`Constant`.`getNull`() to get an equivalent constant for non-aggregate types.
	'''

	def __new__(self, type):
		if not isinstance(type, (ArrayType, VectorType, StructType)):
			_warn("ConstantAggregateZero should not be used on non-aggregate types. Use Constant.getNull(type) instead.", _exceptions.LLVMWarning)
		return Constant.getNull(type)

	def __init__(self, *args, **kwargs):
		pass

class ConstantArray(Constant):

	def __new__(cls, elements, Type type = None):
		if elements is None:
			raise ValueError

		cdef LLVMContext context
		if type is None:
			if len(elements) == 0:
				raise ValueError('Type must be specified if elements is empty.')
			else:
				context = elements[0].context
		else:
			context = type.context

		cdef LLVMValueRef* elarray = NULL
		cdef LLVMValueRef result = NULL
		cdef unsigned length = len(elements)
		cdef Value obj

		cdef Type item_t = type.elementType if type else elements[0].type

		elarray = <LLVMValueRef*>calloc(length, sizeof(LLVMValueRef))
		try:
			for i, item in enumerate(elements):
				if not isinstance(item, Value):
					raise TypeError('{0!r} is no valid LLVM value'.format(item))
				if item_t and item.type != item_t:
					raise ValueError('{0!r} doesn\'t match given element type'.format(item))
				elarray[i] = (<Value>item)._ref
			result = _llvm.LLVMConstArray(item_t._ref, elarray, length)
		finally:
			free(elarray)
		if result == NULL:
			raise _exceptions.ValueCreationError('Could not create ConstantArray from {0!r}'.format(elements))
		return _get_llvmvalue(result, context)

	def __init__(self, *args, **kwargs):
		pass

	def __len__(self):
		return len(self.operands)

	def __iter__(self):
		return iter(self.operands)

	def __reversed__(self):
		return reversed(self.operands)

class ConstantDataSequential(Constant):

	def __len__(Value self):
		cdef _llvm.ConstantDataSequential *item
		item = <_llvm.ConstantDataSequential*> _llvm.unwrap_Value(self._ref)
		return item.getNumElements()

	@property
	def operands(Value self):
		'''
			Elements of this ConstantDataSequential.

			Note that this has to compute a new constant to return, so it isn't as efficient as getElement().
		'''
		return tuple(self.__iter__())

	def __iter__(Value self):
		cdef unsigned i
		cdef _llvm.ConstantDataSequential *item
		item = <_llvm.ConstantDataSequential*> _llvm.unwrap_Value(self._ref)
		for i in range(item.getNumElements()):
			return _get_llvmvalue(_llvm.wrap_Value(item.getElementAsConstant(i)), self.context)

	def __reversed__(Value self):
		cdef int i
		cdef _llvm.ConstantDataSequential *item
		item = <_llvm.ConstantDataSequential*> _llvm.unwrap_Value(self._ref)
		for i in range(item.getNumElements()-1, -1, -1):
			return _get_llvmvalue(_llvm.wrap_Value(item.getElementAsConstant(i)), self.context)

	def getElement(Value self, int i):
		'''
			x.getElement(i)

			Return the specified element as a native value.
		'''
		cdef _llvm.ConstantDataSequential *item
		item = <_llvm.ConstantDataSequential*> _llvm.unwrap_Value(self._ref)
		cdef Type elementType = self.type.elementType
		if i < 0:
			i = item.getNumElements() + i
		if not 0 <= i < item.getNumElements():
			raise IndexError('index out of bounds')
		if isinstance(elementType, IntegerType):
			return item.getElementAsInteger(i)
		if isinstance(elementType, FloatType):
			return item.getElementAsFloat(i)
		if isinstance(elementType, DoubleType):
			return item.getElementAsDouble(i)

class ConstantDataArray(ConstantDataSequential):

	def __new__(cls, value, Type type):
		if isinstance(type, I8Type):
			return _ConstantDataArray_8(value, type.context)
		elif isinstance(type, I16Type):
			return _ConstantDataArray_16(value, type.context)
		elif isinstance(type, I32Type):
			return _ConstantDataArray_32(value, type.context)
		elif isinstance(type, I64Type):
			return _ConstantDataArray_64(value, type.context)
		elif isinstance(type, FloatType):
			return _ConstantDataArray_float(value, type.context)
		elif isinstance(type, DoubleType):
			return _ConstantDataArray_double(value, type.context)
		else:
			raise TypeError('cannot create a ConstantDataArray for type {0}'.format(type))

	def __init__(self, *args, **kwargs):
		pass

cdef _ConstantDataArray_8(value, LLVMContext context):
	cdef uintptr_t length = len(value)
	cdef _llvm.ArrayRef[_llvm.uint8_t]* ref = NULL
	cdef _llvm.uint8_t* elarray = <_llvm.uint8_t*>calloc(length, sizeof(_llvm.uint8_t))
	cdef _llvm.Value* result = NULL
	try:
		for i, item in enumerate(value):
			elarray[i] = item
		ref = new _llvm.ArrayRef[_llvm.uint8_t](elarray, length)
		result = _llvm.ConstantDataArray_get(deref(_llvm.unwrap_Context(context._ref)), deref(ref))
	finally:
		free(elarray)
	return _get_llvmvalue(_llvm.wrap_Value(result), context)

cdef _ConstantDataArray_16(value, LLVMContext context):
	cdef uintptr_t length = len(value)
	cdef _llvm.ArrayRef[_llvm.uint16_t]* ref = NULL
	cdef _llvm.uint16_t* elarray = <_llvm.uint16_t*>calloc(length, sizeof(_llvm.uint16_t))
	cdef _llvm.Value* result = NULL
	try:
		for i, item in enumerate(value):
			elarray[i] = item
		ref = new _llvm.ArrayRef[_llvm.uint16_t](elarray, length)
		result = _llvm.ConstantDataArray_get(deref(_llvm.unwrap_Context(context._ref)), deref(ref))
	finally:
		free(elarray)
	return _get_llvmvalue(_llvm.wrap_Value(result), context)

cdef _ConstantDataArray_32(value, LLVMContext context):
	cdef uintptr_t length = len(value)
	cdef _llvm.ArrayRef[_llvm.uint32_t]* ref = NULL
	cdef _llvm.uint32_t* elarray = <_llvm.uint32_t*>calloc(length, sizeof(_llvm.uint32_t))
	cdef _llvm.Value* result = NULL
	try:
		for i, item in enumerate(value):
			elarray[i] = item
		ref = new _llvm.ArrayRef[_llvm.uint32_t](elarray, length)
		result = _llvm.ConstantDataArray_get(deref(_llvm.unwrap_Context(context._ref)), deref(ref))
	finally:
		free(elarray)
	return _get_llvmvalue(_llvm.wrap_Value(result), context)

cdef _ConstantDataArray_64(value, LLVMContext context):
	cdef uintptr_t length = len(value)
	cdef _llvm.ArrayRef[_llvm.uint64_t]* ref = NULL
	cdef _llvm.uint64_t* elarray = <_llvm.uint64_t*>calloc(length, sizeof(_llvm.uint64_t))
	cdef _llvm.Value* result = NULL
	try:
		for i, item in enumerate(value):
			elarray[i] = item
		ref = new _llvm.ArrayRef[_llvm.uint64_t](elarray, length)
		result = _llvm.ConstantDataArray_get(deref(_llvm.unwrap_Context(context._ref)), deref(ref))
	finally:
		free(elarray)
	return _get_llvmvalue(_llvm.wrap_Value(result), context)

cdef _ConstantDataArray_float(value, LLVMContext context):
	cdef uintptr_t length = len(value)
	cdef _llvm.ArrayRef[float]* ref = NULL
	cdef float* elarray = <float*>calloc(length, sizeof(float))
	cdef _llvm.Value* result = NULL
	try:
		for i, item in enumerate(value):
			elarray[i] = item
		ref = new _llvm.ArrayRef[float](elarray, length)
		result = _llvm.ConstantDataArray_get(deref(_llvm.unwrap_Context(context._ref)), deref(ref))
	finally:
		free(elarray)
	return _get_llvmvalue(_llvm.wrap_Value(result), context)

cdef _ConstantDataArray_double(value, LLVMContext context):
	cdef uintptr_t length = len(value)
	cdef _llvm.ArrayRef[double]* ref = NULL
	cdef double* elarray = <double*>calloc(length, sizeof(double))
	cdef _llvm.Value* result = NULL
	try:
		for i, item in enumerate(value):
			elarray[i] = item
		ref = new _llvm.ArrayRef[double](elarray, length)
		result = _llvm.ConstantDataArray_get(deref(_llvm.unwrap_Context(context._ref)), deref(ref))
	finally:
		free(elarray)
	return _get_llvmvalue(_llvm.wrap_Value(result), context)

class ConstantDataVector(ConstantDataSequential):
	pass

class ConstantString(ConstantDataArray):

	def __new__(cls, bytes value, bint nullterminate=True, LLVMContext context=None):
		if context is None:
			raise ValueError("Context must be specified")
		return _get_llvmvalue(_llvm.LLVMConstStringInContext(context._ref, value, len(value), not nullterminate), context)

	def __init__(self, *args, **kwargs):
		pass

	@property
	def value(Value self):
		cdef _llvm.ConstantDataSequential *item
		item = <_llvm.ConstantDataSequential*> _llvm.unwrap_Value(self._ref)
		return _llvm.stringref2bytes(item.getAsString())

class ConstantExpr(Constant):

	@staticmethod
	def getSizeOf(Type t):
		'''
			ConstantExpr.getSizeOf(t: Type) -> Constant

			Return a ConstantExpr of type i64 that evaluates to the size of `t` in bytes.
		'''
		ptr = ConstantPointerNull(PointerType(t))
		gep = GetElementPtrConstantExpr(ptr, [ConstantInt(1, 32, t.context)])
		return PtrToIntConstantExpr(gep, I64Type(t.context))

	@staticmethod
	def getOffsetOf(Type t, index):
		'''
			ConstantExpr.getOffsetOf(t: CompositeType, index: int) -> Constant

			Return a ConstantExpr of type i64 that evaluates to the offset (in bytes)
			of a element in t.
		'''
		if not isinstance(t, CompositeType):
			raise TypeError('t must be a CompositeType')
		ptr = ConstantPointerNull(PointerType(t))
		if not isinstance(index, Constant):
			index = ConstantInt(index, 32, t.context)
		gep = GetElementPtrConstantExpr(ptr, [ConstantInt(0, 32, t.context), index])
		return PtrToIntConstantExpr(gep, I64Type(t.context))

	@staticmethod
	def getAlignOf(Type t):
		'''
			ConstantExpr.getAlignOf(t: Type) -> Constant

			Return a ConstantExpr of type i64 that evaluates to the alignment of `t` in bytes.
		'''
		strct = StructType([IntegerType(1, t.context), t])
		ptr = ConstantPointerNull(PointerType(strct))
		gep = GetElementPtrConstantExpr(ptr, [ConstantInt(0, 32, t.context), ConstantInt(1, 32, t.context)])
		return PtrToIntConstantExpr(gep, I64Type(t.context))

class BinaryConstantExpr(ConstantExpr):

	@property
	def opcode(Value self):
		return _llvm.LLVMGetConstOpcode(self._ref)

	@property
	def opname(Value self):
		return BINOPS[self.opcode]

	'''
	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))

		cdef LLVMValueRef ref = _llvm.<x>(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

	'''

class CompareConstantExpr(ConstantExpr):
	def __init__(self, *args, **kwargs):
		pass

class ExtractElementConstantExpr(ConstantExpr):
	def __init__(self, *args, **kwargs):
		pass

class ExtractValueConstantExpr(ConstantExpr):
	def __init__(self, *args, **kwargs):
		pass

class GetElementPtrConstantExpr(ConstantExpr):
	def __new__(cls, Value constValue, constIndices, bint inbounds=False):
		cdef LLVMValueRef* elarray = NULL
		cdef unsigned length = len(constIndices)
		cdef Value obj
		cdef LLVMContext context = constValue.context

		if not isinstance(constValue, Constant):
			raise TypeError('{0!r} is no valid LLVM constant value'.format(constValue))
		if not constValue.type.isPtrOrPtrVectorTy():
			raise ValueError("Non-pointer type {0!r} for constant GetElementPtr expression".format(constValue.type))

		currenttype = constValue.type

		elarray = <LLVMValueRef*>calloc(length, sizeof(LLVMValueRef))
		try:
			for i, item in enumerate(constIndices):
				if not isinstance(item, Constant):
					raise TypeError('{0!r} is no valid LLVM constant value'.format(item))
				if item.type.typeid != _llvm.LLVMIntegerTypeKind:
					raise ValueError("Non-integer type {0!r} for constant GetElementPtr index".format(item.type))
				elarray[i] = (<Value>item)._ref

				# Get element type in python to trigger exception when no valid elementType
				currenttype = currenttype.getElementType(item.sextValue)
			obj = Value.__new__(cls, context)
			if inbounds:
				obj._ref = _llvm.LLVMConstInBoundsGEP(constValue._ref, elarray, length)
			else:
				obj._ref = _llvm.LLVMConstGEP(constValue._ref, elarray, length)
			if obj._ref == NULL:
				raise _exceptions.ValueCreationError('Could not create GetElementPtrConstantExpr from {0!r},{1!r}'.format(constValue, constIndices))
		finally:
			free(elarray)

		return obj

	def __init__(self, *args, **kwargs):
		pass

class InsertElementConstantExpr(ConstantExpr):
	def __init__(self, *args, **kwargs):
		pass

class InsertValueConstantExpr(ConstantExpr):
	def __init__(self, *args, **kwargs):
		pass

class SelectConstantExpr(ConstantExpr):
	def __init__(self, *args, **kwargs):
		pass

class ShuffleVectorConstantExpr(ConstantExpr):
	def __init__(self, *args, **kwargs):
		pass

class UnaryConstantExpr(ConstantExpr):
	opname = '<unary-constexpr>'

	@property
	def operand(self) -> Constant:
		return self.operands[0]

	'''
	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))

		cdef LLVMValueRef ref = _llvm.<x>(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

	'''

class IntCastConstantExpr(UnaryConstantExpr):

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to, bint signed):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))

		cdef LLVMValueRef ref = _llvm.LLVMConstIntCast(op._ref, to._ref, signed)
		return _get_llvmvalue(ref, op.context)

class TruncConstantExpr(IntCastConstantExpr):
	opcode = _llvm.LLVMTrunc
	opname = 'trunc'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstTrunc(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class ZExtConstantExpr(IntCastConstantExpr):
	opcode = _llvm.LLVMZExt
	opname = 'zext'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstZExt(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class SExtConstantExpr(IntCastConstantExpr):
	opcode = _llvm.LLVMSExt
	opname = 'sext'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstSExt(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class FPCastConstantExpr(UnaryConstantExpr):

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))

		cdef LLVMValueRef ref = _llvm.LLVMConstFPCast(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class FPTruncConstantExpr(FPCastConstantExpr):
	opcode = _llvm.LLVMFPTrunc
	opname = 'fptrunc'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstFPTrunc(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class FPExtConstantExpr(FPCastConstantExpr):
	opcode = _llvm.LLVMFPExt
	opname = 'fpext'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstFPExt(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class FPToUIConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMFPToUI
	opname = 'fptoui'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstFPToUI(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class FPToSIConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMFPToSI
	opname = 'fptosi'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstFPToSI(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class UIToFPConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMUIToFP
	opname = 'uitofp'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstUIToFP(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class SIToFPConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMSIToFP
	opname = 'sitofp'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstSIToFP(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class IntToPtrConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMIntToPtr
	opname = 'inttoptr'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstIntToPtr(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class PtrToIntConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMPtrToInt
	opname = 'ptrtoint'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstPtrToInt(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class BitCastConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMBitCast
	opname = 'bitcast'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstBitCast(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)

class AddrSpaceCastConstantExpr(UnaryConstantExpr):
	opcode = _llvm.LLVMAddrSpaceCast
	opname = 'addrspacecast'

	def __init__(self, *args, **kwargs):
		pass

	def __new__(cls, Value op, Type to):
		if not isinstance(op, Constant):
			raise ValueError('first operand to {0.__name__} must be a Constant, not {1.__class__!r}'.format(cls, op))
		if to is None:
			raise ValueError('second operand to {0.__name__} must be a Type, not {1.__class__!r}'.format(cls, to))
		cdef LLVMValueRef ref = _llvm.LLVMConstAddrSpaceCast(op._ref, to._ref)
		return _get_llvmvalue(ref, op.context)


class ConstantFP(Constant):
	'''
		ConstantFP(value, type) -> ConstantFP instance

		Creates an LLVM floating point constant from a Python double.
		A type of class FloatingPointType must be given.
	'''

	def __new__(cls, double value, Type type):
		if type is None:
			raise ValueError
		if not isinstance(type, FloatingPointType):
			raise TypeError

		cdef Value obj = Value.__new__(cls, type.context)
		obj._ref = _llvm.LLVMConstReal(type._ref, value)
		if obj._ref == NULL:
			raise _exceptions.ValueCreationError('Could not create floating point value from {0!r}'.format(value))
		return obj

	def __init__(self, *args, **kwargs):
		pass

	def isZero(Value self):
		'''
			x.isZero() -> bool

			Return ``True`` if this Constant represents a value equal to zero.
		'''
		cdef _llvm.ConstantFP *item
		item = <_llvm.ConstantFP*> _llvm.unwrap_Value(self._ref)
		return item.isZero()

	@property
	def value(Value self):
		'''
			value converted to a Python float if possible.
		'''
		cdef _llvm.ConstantFP *item
		item = <_llvm.ConstantFP*> _llvm.unwrap_Value(self._ref)
		if &item.getValueAPF().getSemantics() == &_llvm.IEEEdouble:
			return item.getValueAPF().convertToDouble()
		if &item.getValueAPF().getSemantics() == &_llvm.IEEEsingle:
			return item.getValueAPF().convertToFloat()
		return NotImplemented

	@classmethod
	def fromString(type cls, str value, Type type):
		'''
		ConstantFP.fromString(value, type) -> ConstantFP instance

		Creates an LLVM floating point constant from a Python string.
		A type of class FloatingPointType must be given.
		'''
		if type is None:
			raise ValueError
		if not isinstance(type, FloatingPointType):
			raise TypeError

		cdef bytes valuedata = value.encode('utf-8')
		cdef Value obj = Value.__new__(cls, type.context)
		obj._ref = _llvm.LLVMConstRealOfString(type._ref, valuedata)
		if obj._ref == NULL:
			raise _exceptions.ValueCreationError('Could not create constant floating point value from string {0!r}'.format(value))
		return obj

class ConstantInt(Constant):
	'''
		ConstantInt(value[, (bitwidth|type)[, context]]) -> ConstantInt instance

		Creates an LLVM integer constant from a Python int.
		Either a bitwidth or a type of class IntegerType must be given.
		If a bitwidth is given a context must also be specified.
	'''

	def __new__(cls, object value, object bitwidth, LLVMContext context=None):
		cdef Type typ
		cdef bint signextend = value < 0
		cdef long long realvalue = value
		if isinstance(bitwidth, IntegerType):
			if context and context != bitwidth.context:
				raise ValueError("Context must match type context")
			typ = <Type>bitwidth
		else:
			if bitwidth < 1:
				raise ValueError("bitwidth must be greater than zero.")
			if context is None:
				raise ValueError("Context must be specified.")
			typ = IntegerType(bitwidth, context)

		cdef Value obj = Value.__new__(cls, context)
		obj._ref = _llvm.LLVMConstInt(typ._ref, realvalue, signextend)
		if obj._ref == NULL:
			raise _exceptions.ValueCreationError('Could not create constant integer value from {0!r}'.format(value))
		return obj

	def __init__(self, *args, **kwargs):
		pass

	@classmethod
	def fromString(type cls, str value, Type type, unsigned radix=10, LLVMContext context=_globalContext):
		'''ConstantInt.fromString(value, type[, radix[, context]]) -> ConstantInt instance'''
		if type is None or context is None:
			raise ValueError
		if not isinstance(type, IntegerType):
			raise TypeError

		cdef bytes valuedata = value.encode('utf-8')
		cdef Value obj = Value.__new__(cls, context)
		obj._ref = _llvm.LLVMConstIntOfString(type._ref, valuedata, radix)
		if obj._ref == NULL:
			raise _exceptions.ValueCreationError('Could not create constant integer value from string {0!r}'.format(value))
		return obj

	@property
	def zextValue(self):
		'''value interpreted as unsigned integer (if possible to represent as long long)'''
		return _llvm.LLVMConstIntGetZExtValue((<Value>self)._ref)

	@property
	def sextValue(self):
		'''value interpreted as signed integer (if possible to represent as long long)'''
		return _llvm.LLVMConstIntGetSExtValue((<Value>self)._ref)

class ConstantPointerNull(Constant):
	'''
		ConstantPointerNull(type) -> ConstantPointerNull instance

		Creates an LLVM null pointer constant.
		A type of class PointerType must be given.
	'''
	def __new__(cls, Type type):
		if type is None or not isinstance(type, PointerType):
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMConstPointerNull(type._ref), type.context)

	def __init__(self, *args, **kwargs):
		pass

class ConstantStruct(Constant):
	'''
		ConstantStruct(elements[, type]) -> ConstantStruct instance

		Creates an LLVM struct from a list of element constants.
		If elements is empty, a type of class StructType must be given.
	'''
	def __new__(cls, elements, Type type=None):
		if elements is None:
			raise ValueError

		cdef LLVMContext context
		if type:
			context = type.context
		else:
			if len(elements) > 0:
				context = elements[0].context
			else:
				raise ValueError('Type must be given if elements is empty.')

		cdef LLVMValueRef* elarray = NULL
		cdef unsigned length = len(elements)
		cdef Value obj

		elarray = <LLVMValueRef*>calloc(length, sizeof(LLVMValueRef))
		try:
			for i, (item, item_t) in enumerate(zip(elements, (type.elementTypes if type else (None,)*len(elements)))):
				if not isinstance(item, Value):
					raise TypeError('{0!r} is no valid LLVM value'.format(item))
				if item_t and item.type != item_t:
					raise ValueError('{0!r} does not match given element type'.format(item))
				elarray[i] = (<Value>item)._ref
			obj = Value.__new__(cls, context)
			if type and hasattr(type, 'name'):
				obj._ref = _llvm.LLVMConstNamedStruct(type._ref, elarray, length)
			else:
				obj._ref = _llvm.LLVMConstStructInContext(context._ref, elarray, length, <bint>(type.packed if type else False))
			if obj._ref == NULL:
				raise _exceptions.ValueCreationError('Could not create ConstantArray from {0!r}'.format(elements))
		finally:
			free(elarray)
		return obj

	def __init__(self, *args, **kwargs):
		pass

	def __len__(self):
		return len(self.operands)

	def __iter__(self):
		return iter(self.operands)

	def __reversed__(self):
		return reversed(self.operands)

class ConstantVector(Constant):
	'''
		ConstantVector(elements[, type]) -> ConstantVector instance

		Creates an LLVM vector from a list of element constants.
		A type of class VectorType may be given.
	'''

	def __new__(cls, elements, Type type=None):
		if elements is None:
			raise ValueError

		if len(elements) <= 0:
			raise ValueError("Vector constants must contain at least one element.")

		cdef LLVMContext context
		if type:
			context = type.context
		else:
			context = elements[0].context

		cdef LLVMValueRef* elarray = NULL
		cdef unsigned length = len(elements)
		cdef Value obj

		cdef Type item_t = type.elementType if type else elements[0].type.elementType

		elarray = <LLVMValueRef*>calloc(length, sizeof(LLVMValueRef))
		try:
			for i, item in enumerate(elements):
				if not isinstance(item, Value):
					raise TypeError('{0!r} is no valid LLVM value'.format(item))
				if item_t and item.type != item_t:
					raise ValueError('{0!r} does not match given element type'.format(item))
				elarray[i] = (<Value>item)._ref
			obj = Value.__new__(cls, context)
			obj._ref = _llvm.LLVMConstVector(elarray, length)
			if obj._ref == NULL:
				raise _exceptions.ValueCreationError('Could not create ConstantArray from {0!r}'.format(elements))
		finally:
			free(elarray)
		return obj

	def __init__(self, *args, **kwargs):
		pass

	def __len__(self):
		return len(self.operands)

	def __iter__(self):
		return iter(self.operands)

	def __reversed__(self):
		return reversed(self.operands)

class UndefValue(Value):
	'''
		UndefValue(type) -> UndefValue instance

		Creates an LLVM undefined value of given type.
	'''

	def __new__(cls, Type type, context=None):
		if type is None or cls is None:
			raise ValueError
		return _get_llvmvalue(_llvm.LLVMGetUndef(type._ref), type.context)

	def __init__(self, *args, **kwargs):
		pass

class Instruction(User):

	def __init__(self, *args, **kwargs):
		raise TypeError('Instructions cannot be created by instantiation. Please use llvm3.irbuilder.IRBuilder.')

	@property
	def opcode(Value self):
		'''an integer value representing the instruction type'''
		return _llvm.LLVMGetInstructionOpcode(self._ref)

	@property
	def parent(Value self):
		'''parent :py:class:`BasicBlock` object containing this instruction. ``None`` if not contained in a basic block.'''
		cdef LLVMBasicBlockRef bblock = _llvm.LLVMGetInstructionParent(self._ref)
		if bblock == NULL:
			return None
		return _get_llvmbblock(bblock, self.context)

	def hasMetadata(Value self):
		'''
			x.hasMetadata() -> bool

			Determine whether the instruction has any metadata attached.
		'''
		return _llvm.LLVMHasMetadata(self._ref)

	def getMetadata(Value self, id):
		'''
			x.getMetadata(id) -> MDNode instance

			Get the metadata node attached to the instruction with the given `id`.

			The id may be given as string like 'nontemporal' or as an integer.
		'''
		cdef unsigned code
		if isinstance(id, int):
			code = id
		elif isinstance(id, str):
			code = self.context.getMDKindID(id)
		else:
			raise TypeError("Invalid type for metadata id: {0!r}".format(type(id).__name__))

		cdef LLVMValueRef ref = _llvm.LLVMGetMetadata(self._ref, code)
		if ref != NULL:
			return _get_llvmvalue(ref, self.context)
		else:
			return None

	def setMetadata(Value self, id, Value node):
		'''
			x.setMetadata(id, node)

			Set the metadata node attached to the instruction with the given `id`.

			The id may be given as string like 'nontemporal' or as an integer.
		'''
		cdef unsigned code
		if isinstance(id, int):
			code = id
		elif isinstance(id, str):
			code = self.context.getMDKindID(id)
		else:
			raise TypeError("Invalid type for metadata id: {0!r}".format(type(id).__name__))

		if not isinstance(node, MDNode):
			raise TypeError("Metadata must be a MDNode instance, not {0!r}".format(type(id).__name__))

		if node is None:
			_llvm.LLVMSetMetadata(self._ref, code, NULL)
		else:
			_llvm.LLVMSetMetadata(self._ref, code, node._ref)

	def removeMetadata(Value self, id):
		'''
			x.removeMetadata(id)

			Remove any metadata node attached to the instruction with the given id.

			The id may be given as string like 'nontemporal' or as an integer.
		'''
		cdef unsigned code
		if isinstance(id, int):
			code = id
		elif isinstance(id, str):
			code = self.context.getMDKindID(id)
		else:
			raise TypeError("Invalid type for metadata id: {0!r}".format(type(id).__name__))

		_llvm.LLVMSetMetadata(self._ref, code, NULL)

class BinaryOperator(Instruction):
	@property
	def opname(Value self):
		'''The name of the operation implemented by this :class:`BinaryOperator` instance'''
		return BINOPS[self.opcode]

class CallSite:

	def _get_callingconvention(Value self):
		cdef unsigned cc = _llvm.LLVMGetInstructionCallConv(self._ref)
		return CALLINGCONVENTIONS.get(cc, 'cc {0}'.format(cc))
	def _set_callingconvention(Value self, cconv):
		cdef unsigned cc
		if isinstance(cconv, str):
			ccdict = dict((b,a) for (a,b) in CALLINGCONVENTIONS.items())

			if cconv in ccdict:
				cc = ccdict[cconv]
			elif cconv.startswith('cc ') and cconv[3:].isdigit():
				cc = <unsigned>int(cconv[3:])
			else:
				raise ValueError("unknown calling convention string {0!r}".format(cconv))
		elif isinstance(cconv, int):
			if cconv >= 0:
				cc = cconv
			else:
				raise ValueError("negative numbers not allowed as calling convention codes")
		else:
			raise TypeError("Invalid type for calling convention {0!r}".format(type(cconv).__name__))
		_llvm.LLVMSetInstructionCallConv(self._ref, cc)
	callingConvention = property(_get_callingconvention, _set_callingconvention)

class CallInst(Instruction, CallSite):

	def _get_tailcall(Value self):
		return _llvm.LLVMIsTailCall(self._ref)
	def _set_tailcall(Value self, bint v):
		_llvm.LLVMSetTailCall(self._ref, v)
	tailCall = property(_get_tailcall, _set_tailcall)

class IntrinsicInst(CallInst):
	pass

class DbgInfoIntrinsic(IntrinsicInst):
	pass

class DbgDeclareInst(DbgInfoIntrinsic):
	pass

class MemIntrinsic(IntrinsicInst):
	pass

class MemCpyInst(MemIntrinsic):
	pass

class MemMoveInst(MemIntrinsic):
	pass

class MemSetInst(MemIntrinsic):
	pass

class CmpInst(Instruction):
	pass

class FCmpInst(CmpInst):
	pass

class ICmpInst(CmpInst):

	@property
	def predicate(Value self):
		cdef _llvm.LLVMIntPredicate pred = _llvm.LLVMGetICmpPredicate(self._ref)
		if pred in IPREDICATES:
			return _named_int(pred, IPREDICATES[pred])
		else:
			return pred


class ExtractElementInst(Instruction):
	pass

class GetElementPtrInst(Instruction):
	pass

class InsertElementInst(Instruction):
	pass

class InsertValueInst(Instruction):
	pass

class LandingPadInst(Instruction):
	pass

class PHINode(Instruction):

	@property
	def incomingBlocks(Value self):
		cdef list l = []
		cdef unsigned i = 0
		for i in range(_llvm.LLVMCountIncoming(self._ref)):
			l.append(_get_llvmbblock(_llvm.LLVMGetIncomingBlock(self._ref, i), self.context))
		return l

	@property
	def incomingValues(Value self):
		cdef list l = []
		cdef unsigned i = 0
		for i in range(_llvm.LLVMCountIncoming(self._ref)):
			l.append(_get_llvmvalue(_llvm.LLVMGetIncomingValue(self._ref, i), self.context))
		return l

class SelectInst(Instruction):
	pass

class ShuffleVectorInst(Instruction):
	pass

class StoreInst(Instruction):
	pass

class TerminatorInst(Instruction):
	pass

class BranchInst(TerminatorInst):
	pass

class IndirectBrInst(TerminatorInst):
	pass

class InvokeInst(TerminatorInst, CallSite):
	pass

class ReturnInst(TerminatorInst):

	def isVoid(self):
		'''
			x.isVoid() -> bool

			Return ``True`` if no value is returned.
		'''
		return self.type == VoidType

class SwitchInst(TerminatorInst):

	@property
	def defaultDestination(Value self):
		cdef LLVMBasicBlockRef ref = _llvm.LLVMGetSwitchDefaultDest(self._ref)
		if ref != NULL:
			return _get_llvmbblock(_llvm.LLVMGetSwitchDefaultDest(self._ref), self.context)
		else:
			return None

class UnreachableInst(TerminatorInst):
	pass

class ResumeInst(TerminatorInst):
	pass

class UnaryInstruction(Instruction):
	pass

class AllocaInst(UnaryInstruction):
	pass

class CastInst(UnaryInstruction):
	pass

class BitCastInst(CastInst):
	pass

class AddrSpaceCastInst(CastInst):
	pass

class FPExtInst(CastInst):
	pass

class FPToSIInst(CastInst):
	pass

class FPToUIInst(CastInst):
	pass

class FPTruncInst(CastInst):
	pass

class IntToPtrInst(CastInst):
	pass

class PtrToIntInst(CastInst):
	pass

class SExtInst(CastInst):
	pass

class SIToFPInst(CastInst):
	pass

class TruncInst(CastInst):
	pass

class UIToFPInst(CastInst):
	pass

class ZExtInst(CastInst):
	pass

class ExtractValueInst(UnaryInstruction):
	pass

class LoadInst(UnaryInstruction):
	pass

class VAArgInst(UnaryInstruction):
	pass

'''
class AtomicRMWInst(Instruction):
	pass

class FenceInst(Instruction):
	pass
'''

Iterable.register(_Value_Users)
Sized.register(_Value_Users)
Container.register(_Value_Users)
Iterator.register(_Value_Users_Iterator)
