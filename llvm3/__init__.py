#!/usr/bin/env python3
# coding: utf-8
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

'''
	LLVM 3 bindings


'''

__all__ = [
	'exceptions', 'irbuilder', 'target', 'types', 'values', 'module',
	'context', 'LLVMException', 'globalContext'
]

from .context import LLVMContext, globalContext, VERSION_MAJOR, VERSION_MINOR
from .exceptions import LLVMException, BitcodeReaderError

if VERSION_MAJOR and VERSION_MINOR:
	__version__ = '{0}.{1}'.format(VERSION_MAJOR, VERSION_MINOR)

def load_tests(loader, standard_tests, pattern):
    from . import test
    package_tests = loader.loadTestsFromModule(test)
    standard_tests.addTests(package_tests)
    return standard_tests