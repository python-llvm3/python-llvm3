#!/usr/bin/cython
# coding: utf-8
#
# distutils: language = c++
# distutils: libraries = LLVMCore LLVMSupport tinfo
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

cimport LLVMCore as _llvm
from LLVMCore cimport LLVMTypeRef, LLVMBool, LLVMValueRef
from libc.stdlib cimport calloc, free
from cython cimport sizeof
from weakref import WeakValueDictionary as _WeakValueDictionary, ref as _weakref

from llvm3.context import globalContext as _globalContext
from llvm3 import exceptions as _exceptions

cdef class Type:
	"""
		LLVM Type Representation.

		The instances of the Type class are immutable: once they are created,
		they are never changed. Also note that only one instance of a particular
		type is ever created.
	"""

	def __cinit__(self, *args, **kwargs):
		self._ref = NULL
		self.context = None

	def __init__(self, *args, **kwargs):
		if self._ref == NULL:
			raise TypeError("Cannot be instantiated directly")
		if self.context is None:
			raise _exceptions.LLVMException("Bad initialized object!")

	def __hash__(self):
		return <long>(self._ref)

	def isSized(self):
		'''``True`` if type has size'''
		return _llvm.LLVMTypeIsSized(self._ref) != 0

	def getElementType(self, unsigned index):
		raise TypeError('Non-composite type', self)

	property typeid:
		'''Internal type ID'''
		def __get__(self):
			return _llvm.LLVMGetTypeKind(self._ref)

	def isEmptyType(self):
		'''Return ``True`` if an empty composite type'''
		if isinstance(self, ArrayType):
			return _llvm.LLVMGetArrayLength(self._ref) == 0
		elif isinstance(self, StructType):
			return _llvm.LLVMCountStructElementTypes(self._ref) == 0
		else:
			return False

	def isPrimitiveType(self):
		'''Return ``True`` if a primitive (non-derived) type'''
		return self.typeid in (
			_llvm.LLVMVoidTypeKind,
			_llvm.LLVMHalfTypeKind,
			_llvm.LLVMFloatTypeKind,
			_llvm.LLVMDoubleTypeKind,
			_llvm.LLVMX86_FP80TypeKind,
			_llvm.LLVMFP128TypeKind,
			_llvm.LLVMPPC_FP128TypeKind,
			_llvm.LLVMLabelTypeKind,
			_llvm.LLVMMetadataTypeKind,
			_llvm.LLVMX86_MMXTypeKind,
			_llvm.LLVMIntegerTypeKind,
		)

	def isDerivedType(self):
		'''Return ``True`` if a derived type'''
		return  self.typeid in (
			_llvm.LLVMArrayTypeKind,
			_llvm.LLVMFunctionTypeKind,
			_llvm.LLVMPointerTypeKind,
			_llvm.LLVMStructTypeKind,
			_llvm.LLVMVectorTypeKind,
		)

	def isAggregateType(self):
		'''Return ``True`` if an aggregate type'''
		return  self.typeid in (
			_llvm.LLVMArrayTypeKind,
			_llvm.LLVMStructTypeKind,
		)

	def isFirstClassType(self):
		'''Return ``True`` if a first-class type'''
		return not isinstance(self, (FunctionType, VoidType, X86MMXType))

	def isSingleValueType(self):
		'''Return ``True`` if a single value type (may be valid in register)'''
		return self.typeid in (
			_llvm.LLVMHalfTypeKind,
			_llvm.LLVMFloatTypeKind,
			_llvm.LLVMDoubleTypeKind,
			_llvm.LLVMX86_FP80TypeKind,
			_llvm.LLVMFP128TypeKind,
			_llvm.LLVMPPC_FP128TypeKind,
			_llvm.LLVMLabelTypeKind,
			_llvm.LLVMMetadataTypeKind,
			_llvm.LLVMX86_MMXTypeKind,
			_llvm.LLVMIntegerTypeKind,
			_llvm.LLVMPointerTypeKind,
			_llvm.LLVMVectorTypeKind,
		)

	def isPtrOrPtrVectorTy(self):
		'''Return ``True`` if a pointer type or a vector of pointer type'''
		if self.typeid == _llvm.LLVMPointerTypeKind:
			return True
		if self.typeid == _llvm.LLVMVectorTypeKind:
			return self.elementType.typeid == _llvm.LLVMPointerTypeKind

		return False

	def _get_correct_instance(self):
		return get_type_obj(self.context, self._ref)

	def __richcmp__(self, other, int op):
		if op == 2:
			if not isinstance(other, Type) or not isinstance(self, Type):
				return False
			return (<Type>self)._ref == (<Type>other)._ref
		elif op == 3:
			if not isinstance(other, Type) or not isinstance(self, Type):
				return True
			return (<Type>self)._ref != (<Type>other)._ref
		else:
			return NotImplemented

	def isomorphic(self, Type other):
		'''
			x.isomorphic(other)

			Check if the specified type is isomorphic to `x`.

			For types other than :class:`StructType` equivalent to
			``x == other``.
		'''
		return self == other

	def __str__(Type self):
		cdef char* string = _llvm.LLVMPrintTypeToString(self._ref)
		cdef bytes data
		try:
			data = string
			return data.decode('utf-8')
		finally:
			_llvm.LLVMDisposeMessage(string)

	def __repr__(self):
		data = {'clsmod': type(self).__module__, 'cls': type(self).__name__, 'context': hex(id(self.context)), 'str':str(self)}
		if self.context != _globalContext:
			return "<{clsmod}.{cls} \"{str}\" in context {context}>".format(**data)
		else:
			return "<{clsmod}.{cls} \"{str}\">".format(**data)

	def dump(self):
		_llvm.LLVMDumpType(self._ref)

cdef register_type(Type ty):
	ty.context._type_cache[<unsigned long>ty._ref] = ty

cdef Type get_void_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMVoidTypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(VoidType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_label_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMLabelTypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(LabelType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_x86mmx_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMX86MMXTypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(X86MMXType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_integer_type(unsigned bitwidth, LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMIntTypeInContext(context._ref, bitwidth)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		t = IntegerType
		if bitwidth == 1:
			t = I1Type
		elif bitwidth == 8:
			t = I8Type
		elif bitwidth == 16:
			t = I16Type
		elif bitwidth == 32:
			t = I32Type
		elif bitwidth == 64:
			t = I64Type
		result = Type.__new__(t)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_half_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMHalfTypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(HalfType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result


cdef Type get_float_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMFloatTypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(FloatType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_double_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMDoubleTypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(DoubleType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_x86_fp80_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMX86FP80TypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(X86_FP80Type)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_fp128_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMFP128TypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(FP128Type)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_ppc_fp128_type(LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMPPCFP128TypeInContext(context._ref)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(PPC_FP128Type)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_struct_type(LLVMTypeRef* elements, unsigned elementCount, LLVMBool packed, LLVMContext context):
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMStructTypeInContext(context._ref, elements, elementCount, packed)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(StructType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_array_type(LLVMTypeRef element, unsigned size, LLVMContext context):
	if context is not None and context._ref != _llvm.LLVMGetTypeContext(element):
		raise ValueError("context must match element type context")
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMArrayType(element, size)
	if ref == NULL:
		raise _exceptions.LLVMException("Could not create array type")
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(ArrayType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_vector_type(LLVMTypeRef element, unsigned size, LLVMContext context):
	if context is not None and context._ref != _llvm.LLVMGetTypeContext(element):
		raise ValueError("context must match element type context")
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMVectorType(element, size)
	if ref == NULL:
		raise _exceptions.LLVMException("Could not create vector type")
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(VectorType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result


cdef Type get_pointer_type(LLVMTypeRef element, unsigned addrspace, LLVMContext context):
	if context is not None and context._ref != _llvm.LLVMGetTypeContext(element):
		raise ValueError("context must match element type context")
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMPointerType(element, addrspace)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(PointerType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

cdef Type get_function_type(LLVMTypeRef resulttype, LLVMTypeRef* parametertypes, unsigned count, LLVMBool isvararg, LLVMContext context):
	if context._ref != _llvm.LLVMGetTypeContext(resulttype):
		raise ValueError("context must match result type context")
	cdef _llvm.LLVMTypeRef ref = _llvm.LLVMFunctionType(resulttype, parametertypes, count, isvararg)
	cdef Type result = context._type_cache.get(<unsigned long>ref, None)
	if result is None:
		result = Type.__new__(FunctionType)
		result._ref = ref
		result.context = context
		register_type(result)
		result.__init__()
	return result

class VoidType(Type):
	"""
		VoidType(context) -> :class:`VoidType` instance

		A LLVM Void Type
	"""

	typeid = _llvm.LLVMVoidTypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_void_type(context)

class LabelType(Type):
	"""
		LabelType(context) -> :class:`LabelType` instance

		A LLVM Label Type
	"""

	typeid = _llvm.LLVMLabelTypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_label_type(context)

class X86MMXType(Type):
	"""
		X86MMXType(context) -> X86MMXType instance

		A LLVM X86MMX Type
	"""

	typeid = _llvm.LLVMX86_MMXTypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_x86mmx_type(context)

class IntegerType(Type):
	"""
		IntegerType(bitwidth, context) -> :class:`IntegerType` instance

		A LLVM Integer Type
	"""

	typeid = _llvm.LLVMIntegerTypeKind

	def __new__(cls, bitwidth, context, *args, **kwargs):
		if bitwidth <= 0:
			raise ValueError
		if context is None:
			raise ValueError("Context must be specified.")
		return get_integer_type(bitwidth, context)

	def __init__(self, *args, **kwargs):
		pass

	@property
	def bitwidth(self):
		'''How many bits this integer type can hold'''
		return _llvm.LLVMGetIntTypeWidth((<Type>self)._ref)

class I1Type(IntegerType):

	def __new__(cls, context, *args, **kwargs):
		if context is None:
			raise ValueError("Context must be specified.")
		return IntegerType.__new__(cls, 1, context, *args, **kwargs)

	@property
	def bitwidth(self):
		'''How many bits this integer type can hold (1)'''
		return 1

class I8Type(IntegerType):

	def __new__(cls, context, *args, **kwargs):
		if context is None:
			raise ValueError("Context must be specified.")
		return IntegerType.__new__(cls, 8, context, *args, **kwargs)

	@property
	def bitwidth(self):
		'''How many bits this integer type can hold (8)'''
		return 8

class I16Type(IntegerType):

	def __new__(cls, context, *args, **kwargs):
		if context is None:
			raise ValueError("Context must be specified.")
		return IntegerType.__new__(cls, 16, context, *args, **kwargs)

	@property
	def bitwidth(self):
		'''How many bits this integer type can hold (16)'''
		return 16

class I32Type(IntegerType):

	def __new__(cls, context, *args, **kwargs):
		if context is None:
			raise ValueError("Context must be specified.")
		return IntegerType.__new__(cls, 32, context, *args, **kwargs)

	@property
	def bitwidth(self):
		'''How many bits this integer type can hold (32)'''
		return 32

class I64Type(IntegerType):

	def __new__(cls, context, *args, **kwargs):
		if context is None:
			raise ValueError("Context must be specified.")
		return IntegerType.__new__(cls, 64, context, *args, **kwargs)

	@property
	def bitwidth(self):
		'''How many bits this integer type can hold (64)'''
		return 64

class FloatingPointType(Type):
	"""
		LLVM Floating Point Type base class
	"""

class HalfType(FloatingPointType):
	"""
		HalfType(context) -> :class:`HalfType` instance

		A LLVM Half Type (IEEE 754 16-bit)
	"""

	typeid = _llvm.LLVMHalfTypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_half_type(context)

class FloatType(FloatingPointType):
	"""
		FloatType(context) -> :class:`FloatType` instance

		A LLVM Float Type (IEEE 754 32-bit)
	"""

	typeid = _llvm.LLVMFloatTypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_float_type(context)

class DoubleType(FloatingPointType):
	"""
		DoubleType([context]) -> :class:`DoubleType` instance

		A LLVM Double Type (IEEE 754 64-bit)
	"""

	typeid = _llvm.LLVMDoubleTypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_double_type(context)

class X86_FP80Type(FloatingPointType):
	"""
		X86_FP80Type([context]) -> X86_FP80Type instance

		A LLVM X86_FP80 Type
	"""

	typeid = _llvm.LLVMX86_FP80TypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_x86_fp80_type(context)

class FP128Type(FloatingPointType):
	"""
		FP128Type([context]) -> FP128Type instance

		A LLVM FP128 Type
	"""

	typeid = _llvm.LLVMFP128TypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_fp128_type(context)

class PPC_FP128Type(FloatingPointType):
	"""
		PPC_FP128Type([context]) -> PPC_FP128Type instance

		A LLVM PowerPC FP128 Type
	"""

	typeid = _llvm.LLVMPPC_FP128TypeKind

	def __new__(cls, LLVMContext context):
		if context is None:
			raise ValueError("Context must be specified.")
		return get_ppc_fp128_type(context)

class MetadataType(Type):
	typeid = _llvm.LLVMMetadataTypeKind

class CompositeType(Type):
	"""
		Common base class of :class:`ArrayType`, :class:`StructType`, :class:`PointerType` and :class:`VectorType`.
	"""

	def __len__(self):
		return self._getElementCount()

	def _getElementCount(self):
		raise NotImplementedError

	@property
	def elementCount(self):
		return self._getElementCount()

	def getElementType(self, unsigned index):
		raise NotImplementedError

class SequentialType(CompositeType):
	"""
		Base class of :class:`ArrayType`, :class:`PointerType` and :class:`VectorType`. All of these represent "arrays" in memory. The array type represents a specifically sized array, pointer types are unsized/unknown size arrays, vector types represent specifically sized arrays that allow for use of SIMD instructions. `SequentialType` holds the common features of all, which stem from the fact that all three lay their components out in memory identically.
	"""

	@property
	def elementType(self):
		'''Type of elements'''
		cdef Type result
		cdef LLVMTypeRef ref = _llvm.LLVMGetElementType((<Type>self)._ref)
		return get_type_obj((<Type>self).context, ref)

	def getElementType(self, index):
		return self.elementType

	def __bool__(self):
		return True

class ArrayType(SequentialType):
	"""
		ArrayType(elementtype[, size[, context]]) -> :class:`ArrayType` instance

		Class to represent array types.
	"""

	typeid = _llvm.LLVMArrayTypeKind

	def __new__(cls, Type elementtype, unsigned size, LLVMContext context=None):
		if size < 0:
			raise ValueError
		if not isinstance(elementtype, Type):
			raise TypeError
		if not cls.isValidElementType(elementtype):
			raise ValueError("Invalid element type: {0}".format(elementtype))

		if context is None:
			context = elementtype.context

		return get_array_type(elementtype._ref, size, context)

	@staticmethod
	def isValidElementType(type):
		'''
			ArrayType.isValidElementType(type) -> bool

			Check if the specified type would be a valid element type.
		'''
		return isinstance(type, Type) and not isinstance(type, (VoidType, LabelType, MetadataType, FunctionType, X86MMXType))

	def _getElementCount(self):
		return _llvm.LLVMGetArrayLength((<Type>self)._ref)

class VectorType(SequentialType):
	"""
		VectorType(elementtype[, size[, context]]) -> :class:`VectorType` instance

		Class to represent vector types.
	"""

	typeid = _llvm.LLVMVectorTypeKind

	def __new__(cls, Type elementtype, unsigned size, LLVMContext context=None):
		if size <= 0:
			raise ValueError
		if not isinstance(elementtype, Type):
			raise TypeError
		if not cls.isValidElementType(elementtype):
			raise ValueError

		if context is None:
			context = elementtype.context

		return get_vector_type(elementtype._ref, size, context)

	def __init__(self, *args, **kwargs):
		pass

	def getElementType(self, unsigned index):
		if 0 <= index < self.elementCount:
			return self.elementType
		else:
			raise ValueError('Invalid vector index', index)

	@staticmethod
	def isValidElementType(type):
		'''
			VectorType.isValidElementType(type)

			Check if the specified type would be a valid element type.
		'''
		if isinstance(type, Type):
			if isinstance(type, (IntegerType, FloatingPointType)):
				return True
			if isinstance(type, (PointerType)):
				return isinstance(type.elementType, (IntegerType, FloatingPointType))
		return False

	def _getElementCount(self):
		return _llvm.LLVMGetVectorSize((<Type>self)._ref)

class PointerType(SequentialType):
	"""
		PointerType(elementtype[, addrspace[, context]]) -> :class:`PointerType` instance

		Class to represent pointer types.
	"""

	typeid = _llvm.LLVMPointerTypeKind

	def __new__(cls, Type elementtype, unsigned addrspace=0, LLVMContext context=None):
		if not isinstance(elementtype, Type):
			raise TypeError
		if not cls.isValidElementType(elementtype):
			raise ValueError

		if context is None:
			context = elementtype.context

		return get_pointer_type((elementtype)._ref, addrspace, context)

	def __init__(self, *args, **kwargs):
		pass

	def _getElementCount(self):
		raise NotImplementedError("Pointers have no defined length")

	@property
	def addressSpace(self):
		'''The address space this pointer points to'''
		return _llvm.LLVMGetPointerAddressSpace((<Type>self)._ref)

	@staticmethod
	def isValidElementType(type):
		'''
			PointerType.isValidElementType(type)

			Check if the specified type would be a valid element type.
		'''
		return isinstance(type, Type) and not isinstance(type, (VoidType, LabelType, MetadataType, X86MMXType))

class StructType(CompositeType):
	"""
		StructType(elements[, packed[, context]]) -> :class:`StructType` instance

		Class to represent struct types.

		Literal struct types (e.g. ``{ i32, i32 }``) are uniqued structurally, and must always have a body when created.
	"""

	typeid = _llvm.LLVMStructTypeKind

	def __new__(cls, elements, bint packed=False, LLVMContext context=None):
		cdef LLVMTypeRef* elarray = NULL
		cdef unsigned long length = len(elements)

		elarray = <LLVMTypeRef*>calloc(length, sizeof(LLVMTypeRef))
		try:
			for i, item in enumerate(elements):
				if not isinstance(item, Type):
					raise TypeError('{0!r} is no valid LLVM type'.format(item))
				if not cls.isValidElementType(item):
					raise ValueError('{0!r} is not allowed as struct member'.format(item))
				elarray[i] = (<Type>item)._ref

			if context is None:
				if len(elements) == 0:
					raise ValueError('cannot instantiate empty struct without a context')
				else:
					context = elements[0].context

			return get_struct_type(elarray, length, packed, context)
		finally:
			free(elarray)

	def __init__(self, *args, **kwargs):
		pass

	def _getElementCount(self):
		return _llvm.LLVMCountStructElementTypes((<Type>self)._ref)

	@property
	def packed(self):
		'''If the struct is packed (has no implicit padding between the elements)'''
		return _llvm.LLVMIsPackedStruct((<Type>self)._ref)

	@property
	def elementTypes(self):
		'''List of element types'''
		return self._elementTypes()

	def getElementType(self, unsigned index):
		types = self.elementTypes
		try:
			return types[index]
		except IndexError:
			raise ValueError('Invalid struct index', index) from None

	def _elementTypes(Type self):
		cdef unsigned count = _llvm.LLVMCountStructElementTypes((<Type>self)._ref)

		cdef LLVMTypeRef* array = <LLVMTypeRef*>calloc(count, sizeof(LLVMTypeRef))
		try:
			_llvm.LLVMGetStructElementTypes((<Type>self)._ref, array)
			return [ get_type_obj(self.context, array[i]) for i in range(count) ]
		finally:
			free(array)

	@staticmethod
	def isValidElementType(type):
		'''
			StructType.isValidElementType(type)

			Check if the specified type would be a valid element type.
		'''
		return isinstance(type, Type) and not isinstance(type, (VoidType, LabelType, MetadataType, FunctionType, X86MMXType))

	def isomorphic(Type self, Type other):
		'''
			x.isomorphic(other)

			Check if the specified struct type is isomorphic to this.
		'''
		if not isinstance(self, StructType):
			raise TypeError

		if other is None or self is None:
			return self is other

		if other._ref == self._ref:
			return True
		if not isinstance(other, StructType):
			return False

		cdef list elements1 = self.elementTypes, elements2 = other.elementTypes

		if len(elements1) != len(elements2):
			return False

		for child1, child2 in zip(self.elementTypes, other.elementTypes):
			if (<Type>child1)._ref != (<Type>child2)._ref:
				return False

		return True

class IdentifiedStructType(StructType):
	"""
		IdentifiedStructType(name[, elements[, packed: bool[, context]]])

		Class to represent identified struct types.

		Identified structs (e.g. foo or %42) may optionally have a name and are
		NOT uniqued. The names for identified structs are managed at the
		`LLVMContext` level, so there can only be a single identified struct with
		a given name in a particular :py:class:`llvm3.context.LLVMContext`.
		Identified structs may also optionally be opaque (have no body specified).
	"""

	typeid = _llvm.LLVMStructTypeKind

	def __new__(cls, name, elements=None, bint packed=False, LLVMContext context=None):
		if context is None:
			if not elements:
				raise ValueError("Context must be specified.")
			else:
				context = elements[0].context
		else:
			if elements:
				for el in elements:
					if context != el.context:
						raise ValueError("Context must match all element contextes.")

		cdef Type obj = Type.__new__(cls)
		obj.context = context

		cdef bytes namedata
		if name is not None:
			namedata = name.encode("utf-8")
			obj._ref = _llvm.LLVMStructCreateNamed(context._ref, namedata)
		else:
			obj._ref = _llvm.LLVMStructCreateNamed(context._ref, "")
		if obj._ref == NULL:
			raise RuntimeError("LLVM struct creation failed")

		cdef LLVMTypeRef* elarray = NULL
		cdef unsigned long length = 0

		if (elements is not None):
			obj.setBody(elements, packed)

		obj.__init__()
		return obj

	def __init__(self, *args, **kwargs):
		super(IdentifiedStructType, self).__init__(*args, **kwargs)

	@property
	def name(self):
		cdef char* data = <char*>_llvm.LLVMGetStructName((<Type>self)._ref)
		if data == NULL or data == b'':
			return None
		else:
			return data.decode("UTF-8")

	@property
	def opaque(self):
		return _llvm.LLVMIsOpaqueStruct((<Type>self)._ref)

	@property
	def elementTypes(self):
		if self.opaque:
			return None
		else:
			return super(IdentifiedStructType, self)._elementTypes()

	def setBody(Type self, elements, bint packed=False):
		''' x.setBody(element[, packed]) '''
		if not self.opaque:
			raise RuntimeError("Can only set body of an opaque struct")
		length = len(elements)
		elarray = <LLVMTypeRef*>calloc(length, sizeof(LLVMTypeRef))
		try:
			for i, item in enumerate(elements):
				if not isinstance(item, Type):
					raise TypeError('{0!r} is no valid LLVM type'.format(item))
				if not self.isValidElementType(item):
					raise ValueError('{0!r} is not allowed as struct member'.format(item))
				elarray[i] = (<Type>item)._ref
			_llvm.LLVMStructSetBody(self._ref, elarray, length, packed)
		finally:
			free(elarray)

class FunctionType(Type):
	'''
		FunctionType(returntype, parametertypes[, isvararg: bool[, context]])

		Class to represent function types.
	'''
	typeid = _llvm.LLVMFunctionTypeKind

	def __new__(cls, Type returntype, parametertypes, bint isvararg=False, LLVMContext context=None):
		cdef LLVMTypeRef* paramarray = NULL
		cdef unsigned long length = len(parametertypes)

		if not isinstance(returntype, Type):
			raise TypeError('{0!r} is no valid LLVM type'.format(returntype))
		if not cls.isValidReturnType(returntype):
			raise ValueError('{0!r} is not allowed as return type'.format(returntype))

		if context is None:
			context = returntype.context
		elif context != returntype.context:
			raise ValueError('Context must match return type.')

		if not isinstance(context, LLVMContext):
			raise TypeError("Invalid context")

		paramarray = <LLVMTypeRef*>calloc(length, sizeof(LLVMTypeRef))
		try:
			for i, item in enumerate(parametertypes):
				if not isinstance(item, Type):
					raise TypeError('{0!r} is no valid LLVM type'.format(item))
				if not cls.isValidParameterType(item):
					raise ValueError('{0!r} is not allowed as parameter type'.format(item))
				if item.context != context:
					raise ValueError('Context must match argument types.')
				paramarray[i] = (<Type>item)._ref
			return get_function_type((<Type>returntype)._ref, paramarray, length, isvararg, context)
		finally:
			free(paramarray)

	@property
	def varArg(self):
		'''True if the function signature is variadic'''
		return _llvm.LLVMIsFunctionVarArg((<Type>self)._ref)

	@property
	def parameterCount(self):
		'''The count of parameter types'''
		return _llvm.LLVMCountParamTypes((<Type>self)._ref)

	@staticmethod
	def isValidReturnType(type):
		'''
			FunctionType.isValidReturnType(type)

			Check if the specified type would be a valid return type.
		'''
		return isinstance(type, Type) and (type.isFirstClassType() or isinstance(type, (VoidType, X86MMXType)))

	@staticmethod
	def isValidParameterType(type):
		'''
			FunctionType.isValidParameterType(type)

			Check if the specified type would be a valid parameter type.
		'''
		return isinstance(type, Type) and (type.isFirstClassType() or isinstance(type, (X86MMXType, MetadataType)))

	@property
	def returnType(self):
		'''The return type'''
		return get_type_obj((<Type>self).context, _llvm.LLVMGetReturnType((<Type>self)._ref))

	@property
	def parameterTypes(self):
		'''Array of parameter types'''
		cdef unsigned count = _llvm.LLVMCountParamTypes((<Type>self)._ref)
		cdef LLVMTypeRef* array = <LLVMTypeRef*>calloc(count+1, sizeof(LLVMTypeRef))
		try:
			_llvm.LLVMGetParamTypes((<Type>self)._ref, array)
			return [ get_type_obj((<Type>self).context, array[i]) for i in range(count) ]
		finally:
			free(array)

cdef Type get_type_obj(LLVMContext context, LLVMTypeRef ref):
	cdef Type result
	cdef object obj = context._type_cache.get(<unsigned long>ref, None)

	if obj is None:
		result = Type.__new__(get_correct_type_class(ref))
		result._ref = ref
		result.context = context
		register_type(result)
	else:
		result = <Type>obj
	return result

cdef type get_correct_type_class(LLVMTypeRef typeref):
	cdef _llvm.LLVMTypeKind kind = _llvm.LLVMGetTypeKind(typeref)
	cdef unsigned i
	for cls in [VoidType, LabelType, X86MMXType, HalfType, FloatType, DoubleType, X86_FP80Type, FP128Type, PPC_FP128Type, MetadataType, ArrayType, VectorType, PointerType, FunctionType]:
		if kind == cls.typeid:
			return cls
	if kind == IntegerType.typeid:
		i = _llvm.LLVMGetIntTypeWidth(typeref)
		if i == 1:
			return I1Type
		elif i == 8:
			return I8Type
		elif i == 16:
			return I16Type
		elif i == 32:
			return I32Type
		elif i == 64:
			return I64Type
		return IntegerType
	if kind == StructType.typeid:
		if _llvm.LLVMGetStructName(typeref) == NULL:
			return StructType
		else:
			return IdentifiedStructType

	return Type