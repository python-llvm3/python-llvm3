#!/usr/bin/cython
# coding: utf-8
#
# distutils: language = c++
# distutils: libraries = LLVMCore LLVMSupport tinfo
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from LLVMCore cimport LLVMContextCreate, LLVMGetMDKindIDInContext, LLVMContextDispose, LLVMGetGlobalContext, \
	LLVMPassRegistryRef, LLVMGetGlobalPassRegistry, LLVMInitializeCore, LLVMModuleRef, uintptr_t
from LLVMConfig cimport *
from weakref import WeakValueDictionary as _WeakValueDictionary, ref as _weakref
from functools import partial as _partial

cdef object _llvmcontext_cache = _WeakValueDictionary()

cdef class LLVMContext:
	"""
		This is an important class for using LLVM in a threaded context.  It
		(opaquely) owns and manages the core "global" data of LLVM's core
		infrastructure, including the type and constant uniquing tables.
		LLVMContext itself provides no locking guarantees, so you should be careful
		to have one context per thread.
	"""

	def __cinit__(self, *args, **kwargs):
		self._ref = NULL
		self._owning = 0
		self._module_cache = _WeakValueDictionary()
		self._type_cache = _WeakValueDictionary()

	def __init__(self):
		if self._ref == NULL:
			self._ref = LLVMContextCreate()
			self._owning = 1

		if self._ref == NULL:
			raise RuntimeError("Unknown error in LLVM")

		_llvmcontext_cache.setdefault(<uintptr_t>self._ref, self)

	def __hash__(self):
		return <uintptr_t>(self._ref)

	def getMDKindID(self, str name):
		"""
			x.getMDKindID(name: str) -> int

			Return a unique non-zero ID for the specified metadata kind.
			This ID is uniqued across modules in the current LLVMContext.
		"""
		cdef bytes namedata = name.encode('utf-8')
		return LLVMGetMDKindIDInContext(self._ref, namedata, len(namedata))

	def __dealloc__(self):
		if self._owning and self._ref != NULL:
			LLVMContextDispose(self._ref)

	def __getattr__(self, name):
		if name.startswith('_'):
			raise AttributeError(name)
		from llvm3 import types, values, module

		if hasattr(types, name):
			return _partial(getattr(types, name), context=self)
		if hasattr(module, module):
			return _partial(getattr(module, name), context=self)
		if hasattr(values, module):
			v = getattr(values, name)
			if isinstance(v, type):
				return _partial(v, context=self)
			else:
				return v
		raise AttributeError(name)

cdef LLVMContext _globalContext = LLVMContext.__new__(LLVMContext)
_globalContext._ref = LLVMGetGlobalContext()
_globalContext.__init__()
globalContext = _globalContext

cdef LLVMContext _getContext(LLVMContextRef ref):
	cdef LLVMContext result
	# Check if global context
	if ref == LLVMGetGlobalContext():
		return globalContext
	# Check cache
	result = _llvmcontext_cache.get(<uintptr_t>ref)
	if result is not None:
		return result
	# Create new object
	result = LLVMContext.__new__(LLVMContext)
	result._ref = LLVMGetGlobalContext()
	result.__init__()
	return result

VERSION_MAJOR = LLVM_VERSION_MAJOR if LLVM_VERSION_MAJOR else None
VERSION_MINOR = LLVM_VERSION_MINOR if LLVM_VERSION_MINOR else None
HAS_ATOMICS = LLVM_HAS_ATOMICS
ENABLE_THREADS = LLVM_ENABLE_THREADS
