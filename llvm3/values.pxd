#!/usr/bin/cython
# coding: utf-8
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
# 
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from LLVMCore cimport LLVMValueRef, LLVMBasicBlockRef
from llvm3.context cimport LLVMContext

cdef class Value:
	cdef LLVMValueRef _ref
	cdef object _users
	cdef readonly LLVMContext context

	cpdef hasOneUser(self)
	cpdef replaceAllUsesWith(self, Value replacement)
	cpdef takeName(self, Value other)

	cdef object __weakref__

cdef class BasicBlock(Value):
	cdef LLVMBasicBlockRef _bref
	cdef readonly Value parent

	cdef LLVMValueRef _firstNonPHI(self)
	cdef LLVMValueRef _get_instr(self, unsigned int index)
	cpdef empty(self)