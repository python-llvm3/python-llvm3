#!/usr/bin/cython
# coding: utf-8
#
# distutils: language = c++
# distutils: libraries = LLVMBitReader LLVMBitWriter LLVMCore LLVMSupport tinfo
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from libc.stdlib cimport calloc, free
from cython cimport final, sizeof
from cython.operator import preincrement as inc, dereference as deref
cimport LLVMCore as _llvm
from LLVMBitReader cimport LLVMGetBitcodeModuleInContext, LLVMParseBitcodeInContext
from LLVMBitWriter cimport LLVMWriteBitcodeToFile, LLVMWriteBitcodeToFD
from llvm3.types cimport Type
from llvm3.values cimport Value

from llvm3.context import globalContext as _globalContext
from llvm3.exceptions import BitcodeReaderError, _getOSError
from llvm3 import values as _values
from llvm3.types import PointerType as _PointerType
from weakref import ref as _weakref
import os as _os
from collections import Iterator, Mapping

cdef object _get_llvmvalue(_llvm.LLVMValueRef ref, Module mod):
	if ref == NULL:
		raise ValueError()
	cdef Value v = Value.__new__(Value, mod._context)
	v._ref = ref
	return v._get_correct_instance()

@final
cdef class _Module_Globals_Iterator:

	cdef Module _mod
	cdef _llvm.LLVMValueRef _first, _cur
	cdef int _done
	cdef readonly object _current
	cdef readonly int _mode

	def __cinit__(self, Module mod, mode=0):
		if mod is None:
			raise ValueError
		self._mod = mod
		self._first = _llvm.LLVMGetFirstGlobal(self._mod._ref)
		self._cur = NULL
		self._done = 0
		self._mode = mode
		self._current = None

	cdef _finish(self):
		self._done = 1
		self._cur = NULL
		self._current = None
		self._mod = None
		raise StopIteration

	cdef inline _make_value(self):
		cdef bytes data
		if self._mode == 0:
			data = _llvm.LLVMGetValueName(self._cur)
			return data.decode('utf-8')
		elif self._mode == 1:
			return _get_llvmvalue(self._cur, self._mod)
		elif self._mode == 2:
			data = _llvm.LLVMGetValueName(self._cur)
			return (data.decode('utf-8'), _get_llvmvalue(self._cur, self._mod))

	def __iter__(self):
		return self

	def __next__(self):
		if self._done:
			raise StopIteration
		if self._cur == NULL:
			self._cur = self._first
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current
		else:
			if self._cur == _llvm.LLVMGetLastGlobal(self._mod._ref):
				self._finish()

			self._cur = _llvm.LLVMGetNextGlobal(self._cur)
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current

	def __repr__(self):
		mod = self._mod.identifier
		if mod:
			mod = repr(mod)
		else:
			mod = hex(id(self._mod))
		return "<LLVM Module global variable iterator for {0} at 0x{1:x}>".format(mod, id(self))


@final
cdef class _Module_Globals:
	'''
		| Module globals table
		| (accessible via :py:obj:`Module.globals`)
	'''

	cdef Module _mod

	def __cinit__(self, Module mod):
		self._mod = mod

	def __init__(self, *args):
		raise TypeError("Cannot be manually instantiated")

	def __iter__(self):
		return _Module_Globals_Iterator(self._mod, 0)

	def iterkeys(self):
		'''Return an iterator over the names of the globals.'''
		return self.__iter__()
	keys = iterkeys

	def itervalues(self):
		'''Return an iterator over the globals.'''
		return _Module_Globals_Iterator(self._mod, 1)
	values = itervalues

	def iteritems(self):
		'''Return an iterator over ``(name, global)`` pairs.'''
		return _Module_Globals_Iterator(self._mod, 2)
	items = iteritems

	def get(self, symbol, default=None):
		'''
			x.get(symbol[, default])

			Return the global variable named `symbol` if it exists, else `default`.
		'''
		try:
			return self[symbol]
		except KeyError:
			return default

	cdef _llvm.LLVMValueRef _getitem(self, symbol) except NULL:
		cdef bytes symboldata
		if isinstance(symbol, str):
			symboldata = symbol.encode('utf-8')
		elif isinstance(symbol, bytes):
			symboldata = symbol
		else:
			raise TypeError("Globals table indices must be strings or bytes, not {0}".format(type(symbol).__name__))

		cdef _llvm.LLVMValueRef item = _llvm.LLVMGetNamedGlobal(self._mod._ref, symboldata)
		if item:
			return item
		else:
			raise KeyError(symbol)

	def __contains__(self, symbol):
		try:
			self._getitem(symbol)
			return True
		except KeyError:
			return False

	def __len__(self):
		cdef _llvm.LLVMValueRef item = _llvm.LLVMGetFirstGlobal(self._mod._ref)
		cdef size_t count = 0
		while item is not NULL:
			item = _llvm.LLVMGetNextGlobal(item)
			count += 1
		return count

	def __getitem__(self, symbol):
		cdef _llvm.LLVMValueRef item = self._getitem(symbol)
		return _get_llvmvalue(item, self._mod)

	def __delitem__(self, symbol):
		cdef _llvm.LLVMValueRef item = self._getitem(symbol)
		if _llvm.LLVMGetFirstUse(item) == NULL:
			_llvm.LLVMDeleteGlobal(item)
		else:
			raise ValueError("Only unused globals can be dropped.")

	def add(self, str name, Type type, unsigned addrspace=0):
		'''
			m.globals.add(name, type[, addrspace])

			Add a global to the global variable table.
		'''
		if type is None or name is None:
			raise ValueError
		cdef bytes namedata = name.encode("utf-8")
		return _get_llvmvalue(_llvm.LLVMAddGlobalInAddressSpace(self._mod._ref, type._ref, namedata, addrspace), self._mod)

	def __repr__(self):
		mod = self._mod.identifier
		if mod:
			mod = repr(mod)
		else:
			mod = hex(id(self._mod))
		return "<LLVM Module global variable table for {0}>".format(mod)

	cdef object __weakref__


@final
cdef class _Module_Functions_Iterator:

	cdef Module _mod
	cdef _llvm.LLVMValueRef _first, _cur
	cdef int _done
	cdef readonly object _current
	cdef readonly int _mode

	def __cinit__(self, Module mod, mode=0):
		if mod is None:
			raise ValueError
		self._mod = mod
		self._first = _llvm.LLVMGetFirstFunction(self._mod._ref)
		self._cur = NULL
		self._done = 0
		self._mode = mode
		self._current = None

	cdef _finish(self):
		self._done = 1
		self._cur = NULL
		self._current = None
		self._mod = None
		raise StopIteration

	cdef inline _make_value(self):
		cdef bytes data
		if self._mode == 0:
			data = _llvm.LLVMGetValueName(self._cur)
			return data.decode('utf-8')
		elif self._mode == 1:
			return _get_llvmvalue(self._cur, self._mod)
		elif self._mode == 2:
			data = _llvm.LLVMGetValueName(self._cur)
			return (data.decode('utf-8'), _get_llvmvalue(self._cur, self._mod))

	def __iter__(self):
		return self

	def __next__(self):
		if self._done:
			raise StopIteration
		if self._cur == NULL:
			self._cur = self._first
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current
		else:
			if self._cur == _llvm.LLVMGetLastFunction(self._mod._ref):
				self._finish()

			self._cur = _llvm.LLVMGetNextFunction(self._cur)
			if self._cur == NULL:
				self._finish()
			self._current = self._make_value()
			return self._current

	def __repr__(self):
		mod = self._mod.identifier
		if mod:
			mod = repr(mod)
		else:
			mod = hex(id(self._mod))
		return "<LLVM Module function iterator for {0} at 0x{1:x}>".format(mod, id(self))

@final
cdef class _Module_Functions:
	'''
		| Module function table
		| (accessible via :py:obj:`Module.functions`)
	'''

	cdef Module _mod

	def __cinit__(self, Module mod):
		self._mod = mod

	def __init__(self, *args):
		raise TypeError("Cannot be manually instantiated")

	def __iter__(self):
		return _Module_Functions_Iterator(self._mod, 0)

	def iterkeys(self):
		'''Return an iterator over the names of the functions.'''
		return self.__iter__()
	keys = iterkeys

	def itervalues(self):
		'''Return an iterator over the functions.'''
		return _Module_Functions_Iterator(self._mod, 1)
	values = itervalues

	def iteritems(self):
		'''Return an iterator over ``(name, function)`` pairs.'''
		return _Module_Functions_Iterator(self._mod, 2)
	items = iteritems

	cdef _llvm.LLVMValueRef _getitem(self, symbol) except NULL:
		cdef bytes symboldata
		if isinstance(symbol, str):
			symboldata = symbol.encode('utf-8')
		elif isinstance(symbol, bytes):
			symboldata = symbol
		else:
			raise TypeError("Functions table indices must be strings or bytes, not {0}".format(type(symbol).__name__))

		cdef _llvm.LLVMValueRef item = _llvm.LLVMGetNamedFunction(self._mod._ref, symboldata)
		if item:
			return item
		else:
			raise KeyError(symbol)

	def __contains__(self, symbol):
		try:
			self._getitem(symbol)
			return True
		except KeyError:
			return False

	def __len__(self):
		cdef _llvm.LLVMValueRef item = _llvm.LLVMGetFirstFunction(self._mod._ref)
		cdef size_t count = 0
		while item is not NULL:
			item = _llvm.LLVMGetNextFunction(item)
			count += 1
		return count

	def __getitem__(self, symbol):
		cdef _llvm.LLVMValueRef item = self._getitem(symbol)
		return _get_llvmvalue(item, self._mod)

	def __delitem__(self, symbol):
		cdef _llvm.LLVMValueRef item = self._getitem(symbol)
		if _llvm.LLVMGetFirstUse(item) == NULL:
			_llvm.LLVMDeleteFunction(item)
		else:
			raise ValueError("Only unused functions can be dropped.")

	def get(self, symbol, default=None):
		'''
			m.functions.get(symbol[, default])

			Return the function named `symbol` if it exists, else `default`.
		'''
		try:
			return self[symbol]
		except KeyError:
			return default

	def add(self, str name, Type type, bint force=False):
		'''
			m.functions.add(name, type[, force])

			Add a function to the function table.

			If `force` is ``True`` and a function `name` already exists,
			the already existing function will be renamed.
		'''
		if type is None or name is None:
			raise ValueError
		cdef bytes namedata = name.encode("utf-8")
		if force:
			if name in self:
				oldf = self[name]
				oldf.name = ''
				result = self.add(name, type, False)
				oldf.name = name
				return result
		return _get_llvmvalue(_llvm.LLVMAddFunction(self._mod._ref, namedata, type._ref), self._mod)

	def __repr__(self):
		mod = self._mod.identifier
		if mod:
			mod = repr(mod)
		else:
			mod = hex(id(self._mod))
		return "<LLVM Module function table for {0} at 0x{1:x}>".format(mod, id(self))

	cdef object __weakref__


@final
cdef class _Module_NamedMetadata:
	'''
	| Module named metadata table
	| (accessible via :py:obj:`Module.namedMetadata`)
	'''

	cdef Module _mod

	def __cinit__(self, Module mod):
		self._mod = mod

	def __init__(self, *args):
		raise TypeError("Cannot be manually instantiated")

	def __contains__(self, name):
		cdef bytes namedata
		if isinstance(name, str):
			namedata = name.encode('utf-8')
		elif isinstance(name, bytes):
			namedata = name
		else:
			raise TypeError("Named metadata indices must be strings or bytes, not {0}".format(type(name).__name__))

		cdef _llvm.Module* m = _llvm.unwrap_Module(self._mod._ref)

		return m.getNamedMetadata(namedata) != NULL

	def __getitem__(self, name):
		cdef bytes namedata
		if isinstance(name, str):
			namedata = name.encode('utf-8')
		elif isinstance(name, bytes):
			namedata = name
		else:
			raise TypeError("Named metadata indices must be strings or bytes, not {0}".format(type(name).__name__))

		cdef unsigned count = _llvm.LLVMGetNamedMetadataNumOperands(self._mod._ref, namedata)
		if count == 0:
			raise KeyError(name)

		cdef _llvm.LLVMValueRef* array = <_llvm.LLVMValueRef*>calloc(count, sizeof(_llvm.LLVMValueRef))
		try:
			_llvm.LLVMGetNamedMetadataOperands(self._mod._ref, namedata, array)
			return [ _get_llvmvalue(array[i], self._mod) for i in range(count) ]
		finally:
			free(array)

	def get(self, name, default=None):
		'''
			m.namedMetadata.get(name[, default])
		'''
		try:
			return self[name]
		except KeyError:
			return default

	def append(self, name, Value mdnode):
		'''
			m.namedMetadata.append(name, mdnode)

			Append a :class:`MDNode` to the named metadata list `name`
		'''
		cdef bytes namedata

		if isinstance(name, str):
			namedata = name.encode('utf-8')
		elif isinstance(name, bytes):
			namedata = name
		else:
			raise TypeError("Named metadata indices must be strings or bytes, not {0}".format(type(name).__name__))

		if not isinstance(mdnode, _values.MDNode):
			raise TypeError("Named metadata elements must be MDNodes")

		cdef _llvm.Module* m = _llvm.unwrap_Module(self._mod._ref)
		cdef _llvm.NamedMDNode* node = m.getOrInsertNamedMetadata(namedata)

		if not node:
			raise KeyError(name)

		node.addOperand(<_llvm.MDNode*>_llvm.unwrap_Value(mdnode._ref))

	def __delitem__(self, name):
		cdef bytes namedata
		if isinstance(name, str):
			namedata = name.encode('utf-8')
		elif isinstance(name, bytes):
			namedata = name
		else:
			raise TypeError("Named metadata indices must be strings or bytes, not {0}".format(type(name).__name__))

		cdef _llvm.Module* m = _llvm.unwrap_Module(self._mod._ref)
		cdef _llvm.NamedMDNode* node = m.getNamedMetadata(namedata)

		if node:
			m.eraseNamedMetadata(node)
		else:
			raise KeyError(name)


	def __repr__(self):
		mod = self._mod.identifier
		if mod:
			mod = repr(mod)
		else:
			mod = hex(id(self._mod))
		return "<LLVM Module Named Metadata table for {0} at 0x{1:x}>".format(mod, id(self))

	cdef object __weakref__

cdef Module _frombitcodefile(str path, bint lazy, LLVMContext context):
	if context is None:
		raise ValueError
	cdef bytes bpath = path.encode("utf-8")
	cdef char* errormsg = ""
	cdef _llvm.LLVMModuleRef module = NULL
	cdef _llvm.LLVMMemoryBufferRef buffer = NULL
	if _llvm.LLVMCreateMemoryBufferWithContentsOfFile(bpath, &buffer, &errormsg) != 0:
		error = errormsg.decode("utf-8")
		_llvm.LLVMDisposeMessage(errormsg)
		raise _getOSError(error, path)

	if lazy:
		if LLVMGetBitcodeModuleInContext(context._ref, buffer, &module, &errormsg) != 0:
			_llvm.LLVMDisposeMemoryBuffer(buffer)
			error = errormsg.decode("utf-8")
			_llvm.LLVMDisposeMessage(errormsg)
			raise BitcodeReaderError(error, path)
		else:
			pass # Buffer is internally destroyed by lazy bitcode reader
	else:
		if LLVMParseBitcodeInContext(context._ref, buffer, &module, &errormsg) != 0:
			_llvm.LLVMDisposeMemoryBuffer(buffer)
			error = errormsg.decode("utf-8")
			_llvm.LLVMDisposeMessage(errormsg)
			raise BitcodeReaderError(error, path)
		else:
			_llvm.LLVMDisposeMemoryBuffer(buffer)

	cdef Module m = Module.__new__(Module, path, context)
	m._ref = module
	m._name = _os.path.basename(path)
	m._lazy = lazy
	return m

def _ModuleFromBitcodeFile(str path, bint lazy=False, LLVMContext context=_globalContext):
	"""
		Module.fromBitcodeFile(path, lazy=False [, context])

		Read the specified bitcode file, returning the module.

		If lazy is ``True``, the file will lazily interpreted, which makes
		sense on large object files, but will cause Use lists, etc. to
		be incomplete until all referencing functions are materialized.
	"""
	return _frombitcodefile(path, lazy, context)

def _ModuleFromBitcode(bytes data, bint lazy=False, LLVMContext context=_globalContext):
	"""
		Module.fromBitcode(data, lazy=False [, context])

		Writes the binary data into a temporary file and reads the bitcode,
		returning the module.

		If lazy is ``True``, it will lazily interpreted, which makes
		sense on large object files, but will cause Use lists, etc. to
		be incomplete until all referencing functions are materialized.
	"""
	import tempfile
	with tempfile.NamedTemporaryFile(suffix='.bc') as f:
		f.write(data)
		f.flush()
		return _frombitcodefile(f.name, lazy, context)


cdef class Module:
	"""
		Module(identifier: :py:obj:`str`, context: :class:`llvm3.context.LLVMContext`) -> :class:`Module` instance

		The main container class for the LLVM Intermediate Representation.

		A Module instance is used to store all the information related to an
		LLVM module.Modules are the top level container of all other LLVM
		Intermediate Representation (IR) objects. Each module directly contains
		a list of globals variables, a list of functions, a list of libraries
		(or other modules) this module depends on, a symbol table, and various
		data about the target's characteristics.
	"""

	def __cinit__(self, str identifier, LLVMContext context=_globalContext, *args, **kwargs):
		if context is None:
			raise ValueError("Context cannot be None")
		self._context = context
		self._ref = NULL
		self._globals = None
		self._functions = None
		self._namedmetadata = None
		self._lazy = False

	def __init__(self, str identifier, LLVMContext context):
		cdef bytes namedata
		if self._ref == NULL:
			self._name = identifier
			namedata = self._name.encode('utf-8')
			self._ref = _llvm.LLVMModuleCreateWithNameInContext(namedata, context._ref)

		self._context._module_cache.setdefault(<unsigned long>self._ref, self)

	def __dealloc__(self):
		if(self._ref):
			_llvm.LLVMDisposeModule(self._ref)

	property dataLayout:
		"""Data layout string for the module's target platform. This encodes the type sizes and alignments expected by this module. """
		def __get__(self):
			cdef char* data = <char *>(_llvm.LLVMGetDataLayout(self._ref))
			return data.decode('UTF-8')
		def __set__(self, str value):
			cdef bytes valuedata = value.encode("UTF-8")
			_llvm.LLVMSetDataLayout(self._ref, valuedata)
		def __del__(self):
			_llvm.LLVMSetDataLayout(self._ref, "")

	property targetTriple:
		"""Target triple which is a string describing the target host."""
		def __get__(self):
			cdef char* data = <char *>(_llvm.LLVMGetTarget(self._ref))
			return data.decode('UTF-8')
		def __set__(self, str value):
			cdef bytes valuedata = value.encode("UTF-8")
			_llvm.LLVMSetTarget(self._ref, valuedata)
		def __del__(self):
			_llvm.LLVMSetTarget(self._ref, "")

	property identifier:
		"""The module identifier which is, essentially, the name of the module."""
		def __get__(self):
			return self._name

	def dump(self):
		"""
			x.dump()

			Dump the module to stderr (for debugging).
		"""
		_llvm.LLVMDumpModule(self._ref)

	def __str__(Module self):
		cdef char* data = _llvm.LLVMPrintModuleToString(self._ref)
		cdef bytes b
		try:
			b = data
			return b.decode('utf-8')
		finally:
			_llvm.LLVMDisposeMessage(data)

	def writeIRToFile(Module self, file):
		cdef bytes b
		cdef char* errormsg = ""
		if isinstance(file, str):
			file = file.encode()
		if isinstance(file, bytes):
			b = file
			if not _llvm.LLVMPrintModuleToFile(self._ref, b, &errormsg):
				error = errormsg.decode("utf-8")
				_llvm.LLVMDisposeMessage(errormsg)
				raise _getOSError(error, file)
		else:
			file.write(Module.__str__(self))

	def setModuleInlineAsm(self, str asm):
		"""
			x.setModuleInlineAsm(string)

			Set the module-scope inline assembly blocks.
		"""
		cdef bytes asmdata = asm.encode("UTF-8")
		_llvm.LLVMSetModuleInlineAsm(self._ref, asmdata)

	property context:
		"""
			:class:`llvm3.context.LLVMContext`: The global data context.
		"""
		def __get__(self):
			return self._context

	property lazy:
		"""
			``True`` if module is referencing a bitcode file lazily and might have
			unmaterialized functions.
		"""
		def __get__(self):
			return self._lazy

	property globals:
		"""
			Global Variable List (see :class:`_Module_Globals`)
		"""
		def __get__(self):
			if self._globals == None or self._globals() == None:
				obj = _Module_Globals.__new__(_Module_Globals, self)
				self._globals = _weakref(obj)
				return obj
			else:
				return self._globals()

	property functions:
		"""Function List (see :class:`_Module_Functions`)"""
		def __get__(self):
			if self._functions == None or self._functions() == None:
				obj = _Module_Functions.__new__(_Module_Functions, self)
				self._functions = _weakref(obj)
				return obj
			else:
				return self._functions()

	property namedMetadata:
		"""Named Metadata"""
		def __get__(self):
			if self._namedmetadata == None or self._namedmetadata() == None:
				obj = _Module_NamedMetadata.__new__(_Module_NamedMetadata, self)
				self._namedmetadata = _weakref(obj)
				return obj
			else:
				return self._namedmetadata()


	def addAlias(self, str name, Value aliasee, str linkage="external", str visibility="default"):
		'''
			x.addAlias(name, aliasee[, linkage[, visibility]]) -> GlobalAlias instance

			Add an alias for an global value.
		'''
		if aliasee is None:
			raise ValueError
		cdef bytes namedata = name.encode("utf-8")
		cdef Value val = Value.__new__(Value)
		val._ref = _llvm.LLVMAddAlias(self._ref, (<Type>Value.type)._ref, aliasee._ref, namedata)
		if val._ref == NULL:
			raise LookupError(name)
		val.context = self._context
		return val._get_correct_instance()

	def deleteAlias(self, str name):
		'''
			x.deleteAlias(name)

			Remove an alias for an global value.
			Raises LookupError if the alias didn't exist.
		'''
		for value in self.globals.itervalues():
			for user in value.users:
				if type(user).__name__ == 'GlobalAlias' and user.name == name:
					_llvm.LLVMDeleteGlobal((<Value>user)._ref)
					return
		for value in self.functions.itervalues():
			for user in value.users:
				if type(user).__name__ == 'GlobalAlias' and user.name == name:
					_llvm.LLVMDeleteGlobal((<Value>user)._ref)
					return
		raise LookupError(name)

	def getTypeByName(self, str name):
		'''
			x.getTypeByName(name) -> Type instance

			Returns a named type or raises lookup error if unknown.
			Currently all named types are identified structs.
		'''
		cdef bytes namedata = name.encode('utf-8')
		cdef Type ty = Type.__new__(Type)
		ty._ref = _llvm.LLVMGetTypeByName(self._ref, namedata)
		if ty._ref == NULL:
			raise LookupError(name)
		ty.context = self._context
		return ty._get_correct_instance()

	def getOrInsertFunction(self, str name, Type type, str linkage="external", str visibility="default", str cconv="ccc", bint exact=False):
		'''
			x.getOrInsertFunction(name, type[, linkage[, visibility[, [cconv[, exact]]]]])

			Returns a :py:class:`llvm3.values.Function` with given name or a cast to a function if its signature
			doesn't match the given type. The function will be created if it doesn't
			exist yet.

			If exact is ``True`` a :py:exc:`RuntimeError` is raised if type, linkage, visibility and/or
			calling convention don't match the existing function.

		'''
		if name is None or type is None:
			raise ValueError
		if name in self.functions:
			f = self.functions[name]
			if f.hasLocalLinkage():
				del f.name
				newf = self.getOrInsertFunction(name, type, linkage, visibility, exact)
				f.name = name
				return newf
			else:
				if f.type.elementType == type:
					if exact:
						if f.linkage != linkage or f.visibility != visibility:
							raise RuntimeError("Found function doesn't match given linkage or visibility")
						if f.callingConvention != cconv:
							raise RuntimeError("Found function doesn't match given calling convention")
					return f
				else:
					if exact:
						raise RuntimeError("Found function doesn't match given signature")
					return _values.ConstantExpr.BitCast(f, _PointerType(type))

		else:
			newf = self.functions.add(name, type)
			newf.linkage = linkage
			newf.visibility = visibility
			return newf

	def getNamedMetadata(self, str name):
		'''
			x.getNamedMetadata(name) -> list of MDNode instances
		'''
		cdef bytes namedata = name.encode('UTF-8')
		cdef unsigned count = _llvm.LLVMGetNamedMetadataNumOperands(self._ref, namedata)
		if count == 0:
			return None

		cdef _llvm.LLVMValueRef* array = <_llvm.LLVMValueRef*>calloc(count, sizeof(_llvm.LLVMValueRef))
		try:
			_llvm.LLVMGetNamedMetadataOperands(self._ref, namedata, array)
			return [ _get_llvmvalue(array[i], self) for i in range(count) ]
		finally:
			free(array)

	def writeBitcodeToFile(self, file):
		'''
			x.writeBitcodeToFile(file)

			Writes the module bitcode into `file`.

			`file` may be a string interpreted as file path
			or an object with a fileno() method.
		'''
		cdef int fileno

		if hasattr(file, 'fileno'):
			fileno = file.fileno()
			if hasattr(file, 'flush'):
				file.flush()
			LLVMWriteBitcodeToFD(self._ref, fileno, False, False)
			if hasattr(file, 'flush'):
				file.flush()
		elif isinstance(file, bytes):
			LLVMWriteBitcodeToFile(self._ref, <bytes>file)
		else:
			with open(file, 'wb') as f:
				self.writeBitcodeToFile(f)

	def __repr__(self):
		data = {'clsmod': type(self).__module__, 'cls': type(self).__name__, 'name': self.identifier, 'context': hex(id(self.context)), 'addr': id(self), 'flags': ""}
		if self.lazy:
			data['flags'] = ' (lazy)'
		if self._context != _globalContext:
			return "<{clsmod}.{cls} {name!r}{flags} in context {context} at 0x{addr:x}>".format(**data)
		else:
			return "<{clsmod}.{cls} {name!r}{flags} at 0x{addr:x}>".format(**data)


	fromBitcodeFile = staticmethod(_ModuleFromBitcodeFile)
	fromBitcode = staticmethod(_ModuleFromBitcode)

Iterator.register(_Module_Globals_Iterator)
Iterator.register(_Module_Functions_Iterator)
Mapping.register(_Module_Globals)
Mapping.register(_Module_Functions)

del _ModuleFromBitcodeFile
del _ModuleFromBitcode