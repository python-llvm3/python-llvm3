#!/usr/bin/cython
# coding: utf-8
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

import errno as _errno

cpdef _getOSError(str errstr, str filename=None):
	cdef int errno = {
		"Permission denied": _errno.EPERM,
		"No such file or directory": _errno.ENOENT,
		"Not a directory": _errno.ENOTDIR,
		"Is a directory": _errno.EISDIR,
	}.get(errstr, -1)

	return OSError(errno, errstr, filename)

class LLVMWarning(Warning):
	'''Warnings specific to the llvm3 module'''

class LLVMException(Exception):
	'''Base class for exceptions specific to the llvm3 module'''

	@property
	def strerror(self):
		if len(self.args) > 0 and isinstance(self.args[0], str):
			return self.args[0]
		return None

	@property
	def filename(self):
		if len(self.args) > 1 and isinstance(self.args[1], str):
			return self.args[1]
		return None

	def __str__(self):
		cdef int l = len(self.args)
		if l == 1:
			return self.strerror
		elif l == 2:
			return "{0}: {1!r}".format(self.strerror, self.filename)
		else:
			return super(LLVMException, self).__str__()

class BitcodeReaderError(LLVMException):
	pass

class ValueCreationError(LLVMException):
	pass
