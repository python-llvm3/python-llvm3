#!/usr/bin/cython
# coding: utf-8
#
# (c) 2013 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

from collections import OrderedDict
from . import types, values

LLVMDebugVersion = 12 << 16		# Current version of debug information.
LLVMDebugVersion11 = 11 << 16	# Constant for version 11.
LLVMDebugVersion10 = 10 << 16	# Constant for version 10.
LLVMDebugVersion9 = 9 << 16		# Constant for version 9.
LLVMDebugVersion8 = 8 << 16		# Constant for version 8.
LLVMDebugVersion7 = 7 << 16		# Constant for version 7.
LLVMDebugVersion6 = 6 << 16		# Constant for version 6.
LLVMDebugVersion5 = 5 << 16		# Constant for version 5.
LLVMDebugVersion4 = 4 << 16		# Constant for version 4.
LLVMDebugVersionMask = 0xffff0000 # Mask for version number.

class uint(int):

	def __init__(self, *args, **kwargs):
		if self < 0:
			raise ValueError('cannot construct unsigned int from negative values')

class DebugInfoMeta(type):

	REGISTRY = {}

	@staticmethod
	def __prepare__(name, bases):
		return OrderedDict()

	def __new__(mcs, clsname, bases, dct):

		layout = []
		for base in bases:
			if isinstance(base, DebugInfoMeta):
				layout.extend(base._layout)
		for key, value in dct.items():
			if isinstance(value, type) or isinstance(value, list) and isinstance(value[0], (type, list)):
				layout.append((key, value))
		for i, (name, cls) in enumerate(layout):
			pos = i+1
			if cls is uint:
				dct[name] = property(lambda self, _pos=pos: self.node[_pos].zextValue)
			elif cls is int:
				dct[name] = property(lambda self, _pos=pos: self.node[_pos].sextValue)
			elif cls is str:
				dct[name] = property(lambda self, _pos=pos: self.node[_pos].value)
			elif isinstance(cls, list) and isinstance(cls[0], list):
				dct[name] = property(lambda self, _pos=pos: [(DIDescriptor[x] if x else None) for x in self.node[_pos][0].operands] if self.node[_pos][0] is not None else None)
			elif isinstance(cls, list):
				dct[name] = property(lambda self, _pos=pos: [DIDescriptor[x] for x in self.node[_pos].operands] if self.node[_pos] is not None else None)
			elif issubclass(cls, DIDescriptor):
				dct[name] = property(lambda self, _pos=pos: DIDescriptor(self.node[_pos]) if self.node[_pos] is not None else None)
			elif issubclass(cls, values.Value):
				dct[name] = property(lambda self, _pos=pos: self.node[_pos] if self.node[_pos] is not None else None)

		dct['_layout'] = layout

		cls = type.__new__(mcs, clsname, bases, dict(dct))
		if 'TAG' in dct:
			mcs.REGISTRY[dct['TAG']] = cls
		return cls


class DIDescriptor(metaclass=DebugInfoMeta):

	def __new__(cls, node):
		if isinstance(node, DIDescriptor):
			node = node.node
		if not isinstance(node, values.MDNode):
			raise TypeError('invalid argument type for {0} constructor: {1!r}'.format(cls.__name__, type(node).__name__))

		if not cls.TAG:
			realcls = DebugInfoMeta.REGISTRY.get(node[0].zextValue & ~LLVMDebugVersionMask, None)
			if realcls is not None and issubclass(realcls, cls):
				return realcls(node)
			elif realcls is None:
				result = super(DIDescriptor, cls).__new__(cls)
				result.node = node
				result.__init__(node)
				return result
			else:
				raise TypeError('incompatible MDNode')
		else:
			result = super(DIDescriptor, cls).__new__(cls)
			result.node = node
			result.__init__(node)
			return result

	TAG = None

	@property
	def version(self) -> int:
		return self.node[0].zextValue & LLVMDebugVersionMask

	@property
	def tag(self) -> int:
		return self.node[0].zextValue & ~LLVMDebugVersionMask

	@classmethod
	def create(cls, context, *args, version=LLVMDebugVersion):
		args = list(args)
		if cls.TAG is None:
			raise TypeError('{0} is an abstract type'.format(cls.__name__))
		layout = cls._layout
		settable_layout = [x for x in layout if not x[0].startswith('_')]
		if len(settable_layout) != len(args):
			raise TypeError('{0}.create(...) takes {1} arguments ({2} given)'.format(cls.__name__,  len(settable_layout)+1, len(args)+1))

		# Set hidden parameters to zero/empty
		for i, (name, type_) in enumerate(layout):
			if name.startswith('_'):
				if type_ is uint or type_ is int:
					args.insert(i, 0)
				elif type_ is bool:
					args.insert(i, False)
				elif type_ is str:
					args.insert(i, '')
				else:
					args.insert(i, [])

		results = [values.ConstantInt(version|cls.TAG, 64, context)]
		for (name, type_), value in zip(layout, args):
			if type_ is uint:
				results.append(values.ConstantInt(uint(value), 64, context))
			elif type_ is int:
				results.append(values.ConstantInt(value, 64, context))
			elif type_ is bool:
				results.append(values.ConstantInt(value, 1, context))
			elif type_ is str:
				results.append(values.MDString(value, context))
			elif isinstance(type_, list) and isinstance(type_[0], list):
				results.append(values.MDNode([values.MDNode([x.node for x in value] if value is not None else [None], context)]))
			elif isinstance(type_, list):
				results.append(values.MDNode([x.node for x in value], context) if value is not None else None)
			elif issubclass(type_, DIDescriptor):
				results.append(value.node if value is not None else None)
			elif issubclass(type_, values.Value):
				results.append(value)
			else:
				raise RuntimeError('Unknown type in DebugInfo layout specification: {1!r} for {0!r}'.format(name, type_))

		return DIDescriptor(values.MDNode(results, context))


class DIScope(DIDescriptor, metaclass=DebugInfoMeta):
	TAG=None

class DICompileUnit(DIScope, metaclass=DebugInfoMeta):
	TAG = 0x11
	_unused = uint
	language = uint
	filename = str
	directory = str
	producer = str
	isMain = bool
	isOptimized = bool
	flags = str
	runtimeVersion = uint
	enumTypes = [[DIDescriptor]]
	retainedTypes = [[DIDescriptor]]
	subprograms = [[DIDescriptor]]
	globalVariables = [[DIDescriptor]]

class DIFile(DIScope, metaclass=DebugInfoMeta):
	TAG = 0x29
	filename = str
	directory = str
	_unused = uint

class DINameSpace(DIScope, metaclass=DebugInfoMeta):
	TAG = 0x39
	context = DIScope
	name = str
	sourceFile = DIFile
	lineNumber = uint

class DIGlobalVariable(DIDescriptor, metaclass=DebugInfoMeta):
	TAG = 0x34
	_unused = uint
	context = DIScope
	name = str
	displayName = str
	linkageName = str
	compileUnit = DIScope
	lineNumber = uint
	type = DIDescriptor
	isLocalToUnit = bool
	isDefinition = bool
	globalVariable = values.GlobalVariable

class DISubprogram(DIScope, metaclass=DebugInfoMeta):
	TAG = 0x2E
	_unused = uint
	context = DIScope
	name = str
	displayName = str
	linkageName = str
	compileUnit = DIScope
	lineNumber = uint
	type = DIDescriptor
	isLocalToUnit = bool
	isDefinition = bool
	virtuality = uint
	virtualIndex = uint
	containingType = DIDescriptor
	flags = uint
	isOptimized = bool
	function = values.Function
	templateParams = [DIDescriptor]
	functionDeclaration = DIDescriptor
	functionVariables = [DIDescriptor]

class DILexicalBlock(DIScope, metaclass=DebugInfoMeta):
	TAG = 0x0B
	context = DIScope
	lineNumber = uint
	columnNumber = uint
	sourceFile = DIScope
	uniqueID = uint

class DILexicalBlockFile(DIScope, metaclass=DebugInfoMeta):
	TAG = 0x0B
	scope = DIScope
	file = DIScope

class DIType(DIScope, metaclass=DebugInfoMeta):
	TAG = None
	context = DIScope
	name = str
	file = DIFile
	lineNumber = uint
	size = uint
	alignment = uint
	offset = uint
	flags = uint

class DIBaseType(DIType, metaclass=DebugInfoMeta):
	TAG = 0x24
	...
	encoding = uint

class DIDerivedType(DIType, metaclass=DebugInfoMeta):
	TAG = None
	...
	typeDerivedFrom = DIType
class DIFormalParameter(DIDerivedType, metaclass=DebugInfoMeta): TAG=5
class DIMember(DIDerivedType, metaclass=DebugInfoMeta): TAG=13
class DIPointerType(DIDerivedType, metaclass=DebugInfoMeta): TAG=15
class DIReferenceType(DIDerivedType, metaclass=DebugInfoMeta): TAG=16
class DIPtrToMemberType(DIDerivedType, metaclass=DebugInfoMeta): TAG=22
class DIConstType(DIDerivedType, metaclass=DebugInfoMeta): TAG=38
class DIVolatileType(DIDerivedType, metaclass=DebugInfoMeta): TAG=53
class DIRestrictType(DIDerivedType, metaclass=DebugInfoMeta): TAG=55

class DICompositeType(DIDerivedType, metaclass=DebugInfoMeta):
	TAG = None
	...
	memberDescriptors = [DIDescriptor]
	runtimeLanguages = uint
class DIArrayType(DICompositeType, metaclass=DebugInfoMeta): TAG=1
class DIEnumerationType(DICompositeType, metaclass=DebugInfoMeta): TAG=4
class DIStructureType(DICompositeType, metaclass=DebugInfoMeta): TAG=19
class DIUnionType(DICompositeType, metaclass=DebugInfoMeta): TAG=23
class DISubroutineType(DICompositeType, metaclass=DebugInfoMeta): TAG=21
class DIInheritance(DICompositeType, metaclass=DebugInfoMeta): TAG=28

class DISubrange(DIDescriptor, metaclass=DebugInfoMeta):
	TAG = 0x21
	lo = uint
	count = uint

class DIEnumerator(DIDescriptor, metaclass=DebugInfoMeta):
	TAG = 0x28
	name = str
	enumValue = uint

class DIVariable(DIDescriptor, metaclass=DebugInfoMeta):
	TAG = None
	name = str
	sourceFile = DIFile
	lineArgNumbers = uint
	type = uint
	flags = uint
	inlineLocation = values.MDNode

class DIAutoVariable(DIVariable, metaclass=DebugInfoMeta): TAG=256
class DIArgVariable(DIVariable, metaclass=DebugInfoMeta): TAG=257
