#!/usr/bin/cython
# coding: utf-8
#
# distutils: language = c++
## distutils: libraries =
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
#
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

cimport LLVMCore as _llvm
cimport LLVMConfig
from LLVMTarget cimport *
from LLVMTargetMachine cimport *
from llvm3.types cimport Type
from llvm3.values cimport Value
from llvm3.context cimport LLVMContext
from llvm3 import exceptions as _exceptions
from llvm3.context import globalContext
from llvm3 import types as _types
from llvm3 import values as _values

HOSTTRIPLE = LLVMConfig.LLVM_HOST_TRIPLE.decode('ascii')
DEFAULT_TARGET_TRIPLE = LLVMConfig.LLVM_DEFAULT_TARGET_TRIPLE.decode('ascii')
NATIVE_ARCH = LLVMConfig.LLVM_NATIVE_ARCH.decode('ascii')

cdef object _get_llvmtype(_llvm.LLVMTypeRef ref, LLVMContext ctx=globalContext):
	if ref == NULL:
		raise ValueError()
	cdef Type t = Type.__new__(Type, ctx)
	t._ref = ref
	return t._get_correct_instance()


cdef class TargetData:
	'''
		TargetData(dataLayout) -> TargetData instance
	'''

	def __cinit__(self, *args, **kwargs):
		self._ref = NULL
		self._free = True

	def __init__(self, str dataLayout):
		cdef bytes b = dataLayout.encode("UTF-8")
		if self._ref == NULL:
			self._ref = LLVMCreateTargetData(b)
			if self._ref == NULL:
				raise _exceptions.LLVMException("invalid target data layout string {0!r}".format(dataLayout))

	def __dealloc__(self):
		if self._ref != NULL and self._free:
			LLVMDisposeTargetData(self._ref)

	property dataLayoutString:
		def __get__(self):
			'''
				The data layout string representing this target.
			'''
			cdef char* c = LLVMCopyStringRepOfTargetData(self._ref)
			try:
				return c.decode('UTF-8')
			finally:
				_llvm.LLVMDisposeMessage(c)


	property byteOrder:
		def __get__(self):
			'''
				A string describing the byte order on this target.
				Currently either 'big-endian' or 'little-endian'.
			'''
			return {LLVMBigEndian:'big-endian', LLVMLittleEndian:'little-endian'}.get(LLVMByteOrder(self._ref), 'unknown')

	def isBigEndian(self):
		return LLVMByteOrder(self._ref) == LLVMBigEndian
	def isLittleEndian(self):
		return LLVMByteOrder(self._ref) == LLVMLittleEndian

	property nativeIntegerSizes:
			def __get__(self):
				''' Tuple of native integer sizes on this target '''
				# Extract sizes directly from data layout string
				return tuple(int(x) for x in self.dataLayoutString.split('-n')[-1].split('-')[0].split(':'))

	property pointerSize:
		def __get__(self):
			'''
				The pointer size in bytes in the main address space on this target
			'''
			return LLVMPointerSize(self._ref)

	def getPointerSizeForAS(self, unsigned addrspace):
		'''
			x.getPointerSizeForAS(addrspace)

			The pointer size in bytes in addrspace on this target.
		'''
		return LLVMPointerSizeForAS(self._ref, addrspace)

	def getPointerSizeIntType(self, LLVMContext context):
		'''
			x.getPointerSizeIntType(context)

			Returns the integer type on this target that has the same size as a pointer
		'''
		return _types.IntegerType(self.pointerSize*8, context)

	def getTypeSizeInBits(self, Type type):
		'''
			x.getTypeSizeInBits(type)

			Returns the size of type in bits on this target
		'''
		if type is None:
			raise ValueError('type cannot by None')
		return LLVMSizeOfTypeInBits(self._ref, type._ref)

	def getTypeStoreSize(self, Type type):
		'''
			x.getTypeStoreSize(type)

			Computes the storage size of a type in bytes on this target.
			This it the maximum number of bytes that may be overwritten
			by storing the specified type.
		'''
		if type is None:
			raise ValueError('type cannot by None')
		return LLVMSizeOfTypeInBits(self._ref, type._ref)

	def getTypeABISize(self, Type type):
		'''
			x.getTypeABISize(type)

			Computes the ABI size of a type in bytes on this target.
			This is the amount that alloca reserves for this type.
		'''
		if type is None:
			raise ValueError('type cannot by None')
		return LLVMABISizeOfType(self._ref, type._ref)

	def getTypeABIAlignment(self, Type type):
		'''
			x.getTypeABIAlignment(type)

			Computes the ABI alignment of a type in bytes on this target.
		'''
		if type is None:
			raise ValueError('type cannot by None')
		return LLVMABIAlignmentOfType(self._ref, type._ref)

	def getPreferredAlignment(self, type):
		'''
			x.getTypeABIAlignment(type|global)

			Computes the preferred alignment of a type or global variable
			in bytes on this target.
		'''
		if isinstance(type, Type):
			return LLVMPreferredAlignmentOfType(self._ref, (<Type>type)._ref)
		elif isinstance(type, _values.GlobalVariable):
			return LLVMPreferredAlignmentOfGlobal(self._ref, (<Value>type)._ref)
		else:
			raise TypeError('invalid parameter')

	def getElementContainingOffset(self, Type structType, unsigned long long offset):
		'''
			x.getElementContainingOffset(structType)

			Computes the index of the element of a structure that contains
			the given byte offset
		'''
		if structType is None:
			raise ValueError('structType cannot by None')
		if not isinstance(structType, _types.StructType):
			raise TypeError('structType must be of type StructType')
		return LLVMElementAtOffset(self._ref, structType._ref, offset)

	def getOffsetOfElement(self, Type structType, unsigned element):
		'''
			x.getOffsetOfElement(structType, element)

			Computes the byte offset of the element of a structure given an element index
		'''
		if structType is None:
			raise ValueError('structType cannot by None')
		if not isinstance(structType, _types.StructType):
			raise TypeError('structType must be of type StructType')
		return LLVMOffsetOfElement(self._ref, structType._ref, element)

	def __repr__(self):
		return 'llvm.target.TargetData({0!r})'.format(self.dataLayoutString)

cdef class Target:
	'''
		Wrapper for Target specific information.

		Use lookupTarget(triple), getNativeTarget(), getDefaultTarget()
		or the module-level targets list to get an instance of this class.
	'''
	def __cinit__(self, *args, **kwargs):
		self._ref = NULL

	def __init__(self):
		if self._ref == NULL:
			raise TypeError('Targets cannot be instantied manually')

	property name:
		"""Target Name"""
		def __get__(self):
			cdef char* data = <char*>LLVMGetTargetName(self._ref)
			return data.decode('UTF-8')

	property description:
		"""Target Description"""
		def __get__(self):
			cdef char* data = <char*>LLVMGetTargetDescription(self._ref)
			return data.decode('UTF-8')

	property hasJIT:
		"""True if the target has a JIT"""
		def __get__(self):
			return LLVMTargetHasJIT(self._ref)

	property hasTargetMachine:
		"""True if the target has a TargetMachine associated"""
		def __get__(self):
			return LLVMTargetHasTargetMachine(self._ref)

	property hasAsmBackend:
		"""True if the target has an ASM backend (required for emitting output)"""
		def __get__(self):
			return LLVMTargetHasAsmBackend(self._ref)

	def getTargetData(self, triple, cpu='', features=''):
		"""
			x.getTargetData(triple[, cpu[, features]])

			Return a TargetData instance describing the target.

			May return None if no target data instance can be constructed.
		"""
		cdef bytes triple_ = triple.encode('ascii')
		cdef bytes cpu_ = cpu.encode('ascii')
		cdef bytes features_ = features.encode('ascii')
		cdef LLVMTargetMachineRef tmp = LLVMCreateTargetMachine(self._ref, triple_, cpu_, features_, LLVMCodeGenLevelDefault, LLVMRelocDefault, LLVMCodeModelDefault)
		cdef LLVMTargetDataRef data = NULL
		cdef char* datastr = NULL
		if tmp == NULL:
			return None
		else:
			data = LLVMGetTargetMachineData(tmp)
			datastr = LLVMCopyStringRepOfTargetData(data)
			LLVMDisposeTargetMachine(tmp)
			tmp = NULL
			try:
				return TargetData(datastr.decode('ascii'))
			finally:
				_llvm.LLVMDisposeMessage(datastr)

	def __repr__(self):
		return '<llvm.target.Target "'+self.name+'">'

def lookupTarget(str triple):
	'''
		llvm3.target.lookupTarget(triple) -> llvm3.target.Target instance

		Get a target by triple string. Raise LookupError if Target is unknown.
	'''
	if not triple:
		raise ValueError('invalid triple')
	cdef string err
	cdef bytes triple_b = triple.encode('ascii')
	cdef string triple_ = string(triple_b)
	cdef LLVMTargetRef ref = <LLVMTargetRef>LLVMLookupTarget(triple_, err)
	if ref != NULL:
		for target in targets:
			if (<Target>target)._ref == ref:
				return target
	raise LookupError('unknown target')

cdef object gettargetlist():
	result = []
	cdef LLVMTargetRef item = LLVMGetFirstTarget()
	while item != NULL:
		target = Target.__new__(Target)
		(<Target>target)._ref = item
		target.__init__()
		result.append(target)
		item = LLVMGetNextTarget(item)
	return result

def getDefaultTarget():
	return lookupTarget(DEFAULT_TARGET_TRIPLE)

def getNativeTarget():
	return lookupTarget(HOSTTRIPLE)

LLVMInitializeAllTargetInfos()
LLVMInitializeAllTargets()
LLVMInitializeAllTargetMCs()

targets = gettargetlist()
