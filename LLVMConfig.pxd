#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
# 
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

cdef extern from "llvm_config_wrapper.h":
	cdef char* LLVM_HOST_TRIPLE
	cdef char* LLVM_DEFAULT_TARGET_TRIPLE
	cdef char* LLVM_NATIVE_ARCH "LLVM_NATIVE_ARCH_STR"
	cdef bint LLVM_HAS_ATOMICS
	cdef bint LLVM_ENABLE_THREADS
	cdef int LLVM_VERSION_MAJOR
	cdef int LLVM_VERSION_MINOR

	cdef void xtest()
