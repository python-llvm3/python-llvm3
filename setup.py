#!/usr/bin/env python3
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from subprocess import Popen, PIPE
import os

'''
	(c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
	This file is part of python-llvm3.

	python-llvm3 is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	python-llvm3 is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
'''

llvm_config = os.environ.get('LLVM_CONFIG', 'llvm-config')

LLVM_INCLUDEDIR = os.path.join(
	Popen([llvm_config, '--includedir'], stdout=PIPE).communicate()[0].strip(),
	b'llvm' ).decode()
LLVM_LIBRARYDIR = Popen([llvm_config, '--libdir'], stdout=PIPE).communicate()[0].strip().decode()

def get_llvm_target_libs():
	result = [ 'LLVMLTO' ]
	for file in os.listdir(LLVM_LIBRARYDIR):
		if file.endswith('CodeGen.a') and not file.endswith('LLVMCodeGen.a') and 'LLVM' in file:
			name = file[file.find('LLVM')+4:file.find('CodeGen')]
			result.append('LLVM'+name+'CodeGen')
			result.append('LLVM'+name+'Desc')
			result.append('LLVM'+name+'AsmPrinter')
			if os.path.exists(os.path.join(LLVM_LIBRARYDIR, 'libLLVM'+name+'Utils.a')):
				result.append('LLVM'+name+'Utils')
			result.append('LLVM'+name+'Info')
	result.extend(['LLVMSelectionDAG', 'LLVMCodeGen', 'LLVMScalarOpts', 'LLVMTransformUtils', 'LLVMAnalysis', 'LLVMTarget', 'LLVMMC', 'LLVMMCDisassembler', 'LLVMObject', 'LLVMBitReader', 'LLVMMCParser', 'LLVMCore', 'LLVMSupport'])
	#print(result)
	return result

def llvm_extension(*args, **kwargs):
	kwargs.setdefault('define_macros', []).extend([('__STDC_LIMIT_MACROS',''), ('__STDC_CONSTANT_MACROS','')])
	kwargs.setdefault('include_dirs', []).extend(['.',os.path.dirname(LLVM_INCLUDEDIR), LLVM_INCLUDEDIR])
	kwargs.setdefault('library_dirs', []).extend([LLVM_LIBRARYDIR])
	kwargs.setdefault('extra_compile_args', ['-Wno-self-assign', '-Wno-unused-variable'])
	return Extension(*args, **kwargs)

def llvm_cpp_extension(*args, **kwargs):
	kwargs.setdefault('extra_compile_args', ['-Wno-self-assign', '-Wno-unused-variable', '--std=c++11'])
	return llvm_extension(*args,**kwargs)

ext_modules = [
	llvm_cpp_extension("llvm3.context", ["llvm3/context.pyx"]),
	llvm_cpp_extension("llvm3.types", ["llvm3/types.pyx"]),
	llvm_cpp_extension("llvm3.values", ["llvm3/values.pyx"]),
	llvm_cpp_extension("llvm3.module", ["llvm3/module.pyx"]),
	llvm_extension("llvm3.exceptions", ["llvm3/exceptions.pyx"]),
	llvm_cpp_extension("llvm3.irbuilder", ["llvm3/irbuilder.pyx"]),
	llvm_cpp_extension("llvm3.target", ["llvm3/target.pyx"], libraries=get_llvm_target_libs()+['tinfo']),
	llvm_extension("llvm3.debuginfo", ["llvm3/debuginfo.pyx"]),
]

setup(
  name = 'LLVM3',
  packages = ['llvm3'],
  ext_modules = cythonize(ext_modules),
)
