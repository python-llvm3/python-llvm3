#!/usr/bin/make -f
#
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of python-llvm3.
# 
# python-llvm3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# python-llvm3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with python-llvm3.  If not, see <http://www.gnu.org/licenses/>.
#

PYTHON3=python3

build:
	@"${PYTHON3}" setup.py $@

build-dry:
	@"${PYTHON3}" setup.py -n build

install:
	@"${PYTHON3}" setup.py $@

clean:
	@"${PYTHON3}" setup.py $@
	rm llvm3/*.c || true
	rm llvm3/*.cpp || true

distclean: clean
	rm -r build

test: build
	@V=$$(${PYTHON3} -c 'import sys, platform; print("lib.{0}-{1}-{2.major}.{2.minor}".format(sys.platform, platform.machine(), sys.version_info))'); \
	cd build/"$$V"/ && "${PYTHON3}" -m unittest llvm3

doc: build
	@V="./build/$$(${PYTHON3} -c 'import sys, platform; print("lib.{0}-{1}-{2.major}.{2.minor}".format(sys.platform, platform.machine(), sys.version_info))')"; \
	export PYTHONPATH="$$(readlink -e "$$V")"; \
	$(MAKE) -C $@ html
	
.PHONY: build install test
