:mod:`llvm3.exceptions` --- LLVM Specific Exceptions
========================================

.. automodule:: llvm3.exceptions
   :members:
   :undoc-members:
   :show-inheritance:
