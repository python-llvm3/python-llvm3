:mod:`llvm3.types` --- LLVM Type Representations
================================================

Base classes
************

.. autoclass:: llvm3.types.Type
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: llvm3.types.CompositeType
   :members:
   :undoc-members:
   :show-inheritance:   

.. autoclass:: llvm3.types.SequentialType
   :members:
   :undoc-members:
   :show-inheritance:   

.. autoclass:: llvm3.types.FloatingPointType
   :members:
   :undoc-members:
   :show-inheritance:   

Concrete Type Classes
*********************

.. automodule:: llvm3.types
   :members:
   :exclude-members: Type, CompositeType, SequentialType, FloatingPointType
   :undoc-members:
   :show-inheritance:

See Also
********

* `Type class in LLVM API Documentation <http://llvm.org/docs/doxygen/html/classllvm_1_1Type.html>`_
* `LLVM IR Language Reference <http://llvm.org/docs/LangRef.html#type-system>`_
