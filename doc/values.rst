:mod:`llvm3.values` --- LLVM Value Representations
========================================

.. automodule:: llvm3.values
   :members:
   :undoc-members:
   :show-inheritance:

See Also
******************

* `Value class in LLVM API Documentation <http://llvm.org/docs/doxygen/html/classllvm_1_1Value.html>`_
