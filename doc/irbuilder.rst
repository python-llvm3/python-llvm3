:mod:`llvm3.irbuilder` --- LLVM IRBuilder
========================================

.. automodule:: llvm3.irbuilder
   :members:

See Also
******************

* `IRBuilder class in LLVM API Documentation <http://llvm.org/docs/doxygen/html/classllvm_1_1IRBuilder.html>`_
