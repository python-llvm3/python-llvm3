:mod:`llvm3.context` --- LLVM Context
========================================

.. automodule:: llvm3.context
   :members:

.. py:data:: globalContext
   
   the global context instance

.. py:data:: VERSION_MAJOR

   LLVM version number (major)
   
.. py:data:: VERSION_MINOR

   LLVM version number (minor)

.. py:data:: HAS_ATOMICS

   ``True`` if LLVM was built with atomics

.. py:data:: ENABLE_THREADS

   ``True`` if LLVM was built with thread support

See Also
******************

* `LLVMContext class in LLVM API Documentation <http://llvm.org/docs/doxygen/html/classllvm_1_1LLVMContext.html>`_
