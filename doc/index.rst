.. python-llvm3 documentation master file, created by
   sphinx-quickstart on Tue Jul 22 22:46:56 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

python-llvm3 – Python bindings for LLVM 3.x
========================================

Contents:

.. toctree::
   :maxdepth: 1
   
   context.rst
   module.rst
   types.rst
   values.rst
   irbuilder.rst
   target.rst
   debuginfo.rst
   exceptions.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

