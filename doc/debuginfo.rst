:mod:`llvm3.debuginfo` --- LLVM Debug Information Representations
========================================

.. automodule:: llvm3.debuginfo
   :members:
   :undoc-members:
   :show-inheritance:
