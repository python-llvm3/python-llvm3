:mod:`llvm3.module` --- LLVM Module
========================================

.. automodule:: llvm3.module
   :members: Module

.. autoclass:: llvm3.module._Module_Globals
   :members:
   
   .. describe:: len(m.globals)

      Return the number of globals in the module *m*.

   .. describe:: m.globals[key]

      Return the `GlobalVariable` object with name *key*. Raises a :py:exc:`KeyError`
      if *key* is not a global in the module.

   .. describe:: del m.globals[key]

      Remove the global variable with name *key* from *m*.  Raises a :py:exc:`KeyError`
      if *key* is not a global in the module.

   .. describe:: key in m.globals

      Return ``True`` if *n* has a global variable *key*, else ``False``.

   .. describe:: iter(m.globals)

      Return an iterator over the names of the globals.
      This is a shortcut for ``m.globals.iterkeys()``.

.. autoclass:: llvm3.module._Module_Functions
   :members:
   
   .. describe:: len(m.functions)

      Return the number of functions in the module *m*.

   .. describe:: m.functions[key]

      Return the `GlobalVariable` object with name *key*. Raises a :py:exc:`KeyError`
      if *key* is not a function in the module.

   .. describe:: del m.functions[key]

      Remove the global variable with name *key* from *m*.  Raises a :py:exc:`KeyError`
      if *key* is not a function in the module.

   .. describe:: key in m.functions

      Return ``True`` if *n* has a function variable *key*, else ``False``.

   .. describe:: iter(m.functions)

      Return an iterator over the names of the functions.
      This is a shortcut for ``m.functions.iterkeys()``.

.. autoclass:: llvm3.module._Module_NamedMetadata
   :members:
      
See Also
******************

* `Module class in LLVM API Documentation <http://llvm.org/docs/doxygen/html/classllvm_1_1Module.html>`_
